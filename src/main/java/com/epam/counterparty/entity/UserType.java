package com.epam.counterparty.entity;

/**
 * The enum User type.
 *
 * @author Pavel
 * @since 01.06.2019
 */
public enum UserType {
    USER("1"),
    ADMIN("2");

    String value;

    UserType(String value) {
        this.value = value;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "ClientType{" +
                "value='" + value + '\'' +
                '}';
    }
}
