package com.epam.counterparty.entity;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

/**
 * The type Payment.
 *
 * @author Pavel
 * @since 03.06.2019
 */
public class Payment extends Entity {
    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private long id;
    private long userId;
    private long tariffId;
    private Instant startDate;
    private Instant endDate;
    private BigDecimal currentBalance;

    /**
     * Instantiates a new Payment.
     */
    public Payment() {
    }

    /**
     * Instantiates a new Payment.
     *
     * @param userId         the user id
     * @param tariffId       the tariff id
     * @param startDate      the start date
     * @param endDate        the end date
     * @param currentBalance the current balance
     */
    public Payment(long userId, long tariffId, Instant startDate, Instant endDate, BigDecimal currentBalance) {
        this.userId = userId;
        this.tariffId = tariffId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.currentBalance = currentBalance;
    }

    /**
     * Instantiates a new Payment.
     *
     * @param id             the id
     * @param userId         the user id
     * @param tariffId       the tariff id
     * @param startDate      the start date
     * @param endDate        the end date
     * @param currentBalance the current balance
     */
    public Payment(long id, long userId, long tariffId, Instant startDate, Instant endDate, BigDecimal currentBalance) {
        this.id = id;
        this.userId = userId;
        this.tariffId = tariffId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.currentBalance = currentBalance;
    }

    /**
     * Gets time in simple format.
     *
     * @param inputDate the input date
     * @return the time in simple format
     */
    public String getTimeInSimpleFormat(Instant inputDate) {
        Date date = Date.from(inputDate);
        return new SimpleDateFormat(DATE_TIME_PATTERN).format(date);
    }

    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Gets tariff id.
     *
     * @return the tariff id
     */
    public long getTariffId() {
        return tariffId;
    }

    /**
     * Sets tariff id.
     *
     * @param tariffId the tariff id
     */
    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    /**
     * Gets start date.
     *
     * @return the start date
     */
    public Instant getStartDate() {
        return startDate;
    }

    /**
     * Sets start date.
     *
     * @param startDate the start date
     */
    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets end date.
     *
     * @return the end date
     */
    public Instant getEndDate() {
        return endDate;
    }

    /**
     * Sets end date.
     *
     * @param endDate the end date
     */
    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    /**
     * Gets current balance.
     *
     * @return the current balance
     */
    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    /**
     * Sets current balance.
     *
     * @param currentBalance the current balance
     */
    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Payment)) {
            return false;
        }

        Payment payment = (Payment) o;

        return getUserId() == payment.getUserId();

    }

    @Override
    public int hashCode() {
        return (int) (getUserId() ^ (getUserId() >>> 32));
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", userId=" + userId +
                ", tariffId=" + tariffId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", currentBalance=" + currentBalance +
                '}';
    }
}
