package com.epam.counterparty.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @since 16.06.2019
 * @author Pavel
 */
public class Egr {
    /**
     * TP - type of business entity (1 - legal entity, 2 - individual entrepreneur)
     */
    @JsonProperty("TP")
    private short type;

    /**
     * NM - registration number of the business entity.
     */
    @JsonProperty("NM")
    private int unp;
    /**
     * DC - the date of registration of the business entity.
     */
    @JsonProperty("DC")
    private String registrationDate;
    /**
     * DD - date of exclusion from the EGR (cessation of activity in connection with the reorganization) of a business entity.
     */
    @JsonProperty("DD")
    private String exclusionDate;
    /**
     * NU - code of the current accounting authority of the business entity.
     */
    @JsonProperty("NU")
    private short codeOrgan;
    /**
     * VU - the name of the current accounting authority of the business entity.
     */
    @JsonProperty("VU")
    private String nameOrgan;
    /**
     * NS - the code of the current state of the business entity.
     */
    @JsonProperty("NS")
    private short codeState;
    /**
     * VS - the name of the current state of the business entity.
     */
    @JsonProperty("VS")
    private String nameState;
    /**
     * VNM is the full name of the business entity / full name of the individual entrepreneur in Russian.
     */
    @JsonProperty("VNM")
    private String fullNameRu;
    /**
     * VSN - abbreviated name of the business entity in Russian.
     */
    @JsonProperty("VSN")
    private String shortNameRu;
    /**
     * VFN is the corporate name of the business entity in Russian.
     */
    @JsonProperty("VFN")
    private String firmNameRu;
    /**
     * ACT is a sign of record activity.
     */
    @JsonProperty("ACT")
    private String activity;
    /**
     * Z - the presence of a ban on the alienation of the participant's
     * share in the authorized capital of a commercial organization.
     */
    @JsonProperty("Z")
    private String zapret;
    /**
     * VNMB is the full name of the business entity in the Belarusian language.
     */
    @JsonProperty("VNMB")
    private String fullNameBy;
    /**
     * VSNB - abbreviated name of the business entity in the Belarusian language.
     */
    @JsonProperty("VSNB")
    private String shortNameBy;
    /**
     * VFNB is the corporate name of the business entity in the Belarusian language.
     */
    @JsonProperty("VFNB")
    private String firmNameBy;

    @Override
    public String toString() {
        return "Egr{" +
                "type=" + type +
                ", unp=" + unp +
                ", registrationDate=" + registrationDate +
                ", exclusionDate=" + exclusionDate +
                ", codeOrgan=" + codeOrgan +
                ", nameOrgan='" + nameOrgan + '\'' +
                ", codeState=" + codeState +
                ", nameState='" + nameState + '\'' +
                ", fullNameRu='" + fullNameRu + '\'' +
                ", shortNameRu='" + shortNameRu + '\'' +
                ", firmNameRu='" + firmNameRu + '\'' +
                ", activity='" + activity + '\'' +
                ", zapret='" + zapret + '\'' +
                ", fullNameBy='" + fullNameBy + '\'' +
                ", shortNameBy='" + shortNameBy + '\'' +
                ", firmNameBy='" + firmNameBy + '\'' +
                '}';
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public short getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(short type) {
        this.type = type;
    }

    /**
     * Gets unp.
     *
     * @return the unp
     */
    public int getUnp() {
        return unp;
    }

    /**
     * Sets unp.
     *
     * @param unp the unp
     */
    public void setUnp(int unp) {
        this.unp = unp;
    }

    /**
     * Gets registration date.
     *
     * @return the registration date
     */
    public String getRegistrationDate() {
        return registrationDate;
    }

    /**
     * Sets registration date.
     *
     * @param registrationDate the registration date
     */
    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    /**
     * Gets exclusion date.
     *
     * @return the exclusion date
     */
    public String getExclusionDate() {
        return exclusionDate;
    }

    /**
     * Sets exclusion date.
     *
     * @param exclusionDate the exclusion date
     */
    public void setExclusionDate(String exclusionDate) {
        this.exclusionDate = exclusionDate;
    }

    /**
     * Gets code organ.
     *
     * @return the code organ
     */
    public short getCodeOrgan() {
        return codeOrgan;
    }

    /**
     * Sets code organ.
     *
     * @param codeOrgan the code organ
     */
    public void setCodeOrgan(short codeOrgan) {
        this.codeOrgan = codeOrgan;
    }

    /**
     * Gets name organ.
     *
     * @return the name organ
     */
    public String getNameOrgan() {
        return nameOrgan;
    }

    /**
     * Sets name organ.
     *
     * @param nameOrgan the name organ
     */
    public void setNameOrgan(String nameOrgan) {
        this.nameOrgan = nameOrgan;
    }

    /**
     * Gets code state.
     *
     * @return the code state
     */
    public short getCodeState() {
        return codeState;
    }

    /**
     * Sets code state.
     *
     * @param codeState the code state
     */
    public void setCodeState(short codeState) {
        this.codeState = codeState;
    }

    /**
     * Gets name state.
     *
     * @return the name state
     */
    public String getNameState() {
        return nameState;
    }

    /**
     * Sets name state.
     *
     * @param nameState the name state
     */
    public void setNameState(String nameState) {
        this.nameState = nameState;
    }

    /**
     * Gets full name ru.
     *
     * @return the full name ru
     */
    public String getFullNameRu() {
        return fullNameRu;
    }

    /**
     * Sets full name ru.
     *
     * @param fullNameRu the full name ru
     */
    public void setFullNameRu(String fullNameRu) {
        this.fullNameRu = fullNameRu;
    }

    /**
     * Gets short name ru.
     *
     * @return the short name ru
     */
    public String getShortNameRu() {
        return shortNameRu;
    }

    /**
     * Sets short name ru.
     *
     * @param shortNameRu the short name ru
     */
    public void setShortNameRu(String shortNameRu) {
        this.shortNameRu = shortNameRu;
    }

    /**
     * Gets firm name ru.
     *
     * @return the firm name ru
     */
    public String getFirmNameRu() {
        return firmNameRu;
    }

    /**
     * Sets firm name ru.
     *
     * @param firmNameRu the firm name ru
     */
    public void setFirmNameRu(String firmNameRu) {
        this.firmNameRu = firmNameRu;
    }

    /**
     * Gets activity.
     *
     * @return the activity
     */
    public String getActivity() {
        return activity;
    }

    /**
     * Sets activity.
     *
     * @param activity the activity
     */
    public void setActivity(String activity) {
        this.activity = activity;
    }

    /**
     * Gets zapret.
     *
     * @return the zapret
     */
    public String getZapret() {
        return zapret;
    }

    /**
     * Sets zapret.
     *
     * @param zapret the zapret
     */
    public void setZapret(String zapret) {
        this.zapret = zapret;
    }

    /**
     * Gets full name by.
     *
     * @return the full name in Belorussian language.
     */
    public String getFullNameBy() {
        return fullNameBy;
    }

    /**
     * Sets full name in Belorussian language.
     *
     * @param fullNameBy the full name by
     */
    public void setFullNameBy(String fullNameBy) {
        this.fullNameBy = fullNameBy;
    }

    /**
     * Gets short name in Belorussian language.
     *
     * @return the short name by
     */
    public String getShortNameBy() {
        return shortNameBy;
    }

    /**
     * Sets short name in Belorussian language.
     *
     * @param shortNameBy the short name by
     */
    public void setShortNameBy(String shortNameBy) {
        this.shortNameBy = shortNameBy;
    }

    /**
     * Gets firm name in Belorussian language.
     *
     * @return the firm name by
     */
    public String getFirmNameBy() {
        return firmNameBy;
    }

    /**
     * Sets firm name in Belorussian language.
     *
     * @param firmNameBy the firm name by
     */
    public void setFirmNameBy(String firmNameBy) {
        this.firmNameBy = firmNameBy;
    }

}
