package com.epam.counterparty.entity;

/**
 * The type Ability tariff.
 *
 * @author Pavel
 * @since 16.06.2019
 */
public class AbilityTariff extends Entity {
    private long abilityId;
    private long tariffId;

    /**
     * Instantiates a new Ability tariff.
     */
    public AbilityTariff() {
    }

    /**
     * Gets ability id.
     *
     * @return the ability id
     */
    public long getAbilityId() {
        return abilityId;
    }

    /**
     * Sets ability id.
     *
     * @param abilityId the ability id
     */
    public void setAbilityId(long abilityId) {
        this.abilityId = abilityId;
    }

    /**
     * Gets tariff id.
     *
     * @return the tariff id
     */
    public long getTariffId() {
        return tariffId;
    }

    /**
     * Sets tariff id.
     *
     * @param tariffId the tariff id
     */
    public void setTariffId(long tariffId) {
        this.tariffId = tariffId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbilityTariff)) {
            return false;
        }

        AbilityTariff that = (AbilityTariff) o;

        if (getAbilityId() != that.getAbilityId()) {
            return false;
        }
        return getTariffId() == that.getTariffId();

    }

    @Override
    public int hashCode() {
        int result = (int) (getAbilityId() ^ (getAbilityId() >>> 32));
        result = 31 * result + (int) (getTariffId() ^ (getTariffId() >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "AbilityTariff{" +
                "abilityId=" + abilityId +
                ", tariffId=" + tariffId +
                '}';
    }


}
