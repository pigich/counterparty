package com.epam.counterparty.entity;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

/**
 * The type Fee.
 *
 * @author Pavel
 * @since 04.06.2019
 */
public class Fee extends Entity {
    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private long id;
    private long userId;
    private BigDecimal amount;
    private String feesDate;

    /**
     * Gets fees date.
     *
     * @return the fees date
     */
    public String getFeesDate() {
        return feesDate;
    }

    /**
     * Sets fees date.
     *
     * @param instantDate the instant date
     */
    public void setFeesDate(Instant instantDate) {
        Date date = Date.from(instantDate);
        this.feesDate = new SimpleDateFormat(DATE_TIME_PATTERN).format(date);
    }

    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets amount.
     *
     * @param amount the amount
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Fee)) {
            return false;
        }

        Fee fee = (Fee) o;

        return getId() == fee.getId();

    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", userId=" + userId +
                ", amount=" + amount +
                ", feesDate=" + feesDate;
    }
}
