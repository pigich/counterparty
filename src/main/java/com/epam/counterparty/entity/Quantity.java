package com.epam.counterparty.entity;

import lombok.*;

/**
 * The type Quantity.
 *
 * @author Pavel
 * @since 28.06.2019
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Quantity extends Entity {
    /**
     * rowsCount - it's a total number of searched data
     */
    private long rowsCount;
    /**
     * pagesCount - the number of pages
     */
    private long pagesCount;

    @Override
    public String toString() {
        return "Quantity{" +
                "rowsCount=" + rowsCount +
                ", pageCount=" + pagesCount +
                '}';
    }
}
