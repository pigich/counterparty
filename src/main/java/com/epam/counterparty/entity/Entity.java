package com.epam.counterparty.entity;

/**
 * The {@code Entity} is a base class for all entities which represents tables from Database.
 *
 * @author Pavel
 * @since 26.05.2019
 */
public abstract class Entity {
    /**
     * The Id.
     */
    long id;

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }
}
