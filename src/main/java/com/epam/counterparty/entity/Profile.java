package com.epam.counterparty.entity;

import lombok.*;

/**
 * The type Profile.
 *
 * @author Pavel
 * @since 03.06.2019
 */
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode(callSuper = false)
public class Profile extends Entity {
    private long userId;
    private String fullName;
    private String phone;
    private int orgUnp;
    private String orgName;
    private long tariffId;

}
