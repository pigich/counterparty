package com.epam.counterparty.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Tariff.
 *
 * @author Pavel
 * @since 03.06.2019
 */
public class Tariff extends Entity {
    private long id;
    private String tariffNameRu;
    private String tariffNameEn;
    private BigDecimal price;
    private short period;
    private List<Ability> abilityList = new ArrayList<>();
    private boolean archival;

    /**
     * Instantiates a new Tariff.
     */
    public Tariff() {
    }

    /**
     * Instantiates a new Tariff.
     *
     * @param tariffNameRu the tariff name ru
     * @param tariffNameEn the tariff name en
     * @param price        the price
     * @param period       the period
     * @param abilityList  the ability list
     */
    public Tariff(String tariffNameRu, String tariffNameEn, BigDecimal price, short period, List<Ability> abilityList) {
        this.tariffNameRu = tariffNameRu;
        this.tariffNameEn = tariffNameEn;
        this.price = price;
        this.period = period;
        this.abilityList = abilityList;
    }

    /**
     * Instantiates a new Tariff.
     *
     * @param id           the id
     * @param tariffNameRu the tariff name ru
     * @param tariffNameEn the tariff name en
     * @param price        the price
     * @param period       the period
     * @param abilityList  the ability list
     * @param archival     the archival
     */
    public Tariff(long id, String tariffNameRu, String tariffNameEn, BigDecimal price, short period, List<Ability> abilityList, boolean archival ) {
        this.id = id;
        this.tariffNameRu = tariffNameRu;
        this.tariffNameEn = tariffNameEn;
        this.price = price;
        this.period = period;
        this.abilityList = abilityList;
        this.archival = archival;
    }

    /**
     * Instantiates a new Tariff.
     *
     * @param tariffNameRu the tariff name ru
     * @param tariffNameEn the tariff name en
     * @param price        the price
     * @param period       the period
     * @param archival     the archival
     */
    public Tariff(String tariffNameRu, String tariffNameEn, BigDecimal price, short period, boolean archival) {
        this.tariffNameRu = tariffNameRu;
        this.tariffNameEn = tariffNameEn;
        this.price = price;
        this.period = period;
        this.archival = archival;
    }

    /**
     * Instantiates a new Tariff.
     *
     * @param tariffNameRu the tariff name ru
     * @param tariffNameEn the tariff name en
     * @param price        the price
     * @param period       the period
     */
    public Tariff(String tariffNameRu, String tariffNameEn, BigDecimal price, short period) {
        this.tariffNameRu = tariffNameRu;
        this.tariffNameEn = tariffNameEn;
        this.price = price;
        this.period = period;
    }

    /**
     * Gets ability list.
     *
     * @return the ability list
     */
    public List<Ability> getAbilityList() {
        return abilityList;
    }

    /**
     * Is archival boolean.
     *
     * @return the boolean
     */
    public boolean isArchival() {
        return archival;
    }

    /**
     * Sets archival.
     *
     * @param archival the archival
     */
    public void setArchival(boolean archival) {
        this.archival = archival;
    }

    /**
     * Sets ability list.
     *
     * @param abilityList the ability list
     */
    public void setAbilityList(List<Ability> abilityList) {
        this.abilityList = abilityList;
    }

    @Override
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Gets period.
     *
     * @return the period
     */
    public short getPeriod() {
        return period;
    }

    /**
     * Sets period.
     *
     * @param period the period
     */
    public void setPeriod(short period) {
        this.period = period;
    }

    /**
     * Gets tariff name ru.
     *
     * @return the tariff name ru
     */
    public String getTariffNameRu() {
        return tariffNameRu;
    }

    /**
     * Sets tariff name ru.
     *
     * @param tariffNameRu the tariff name ru
     */
    public void setTariffNameRu(String tariffNameRu) {
        this.tariffNameRu = tariffNameRu;
    }

    /**
     * Gets tariff name en.
     *
     * @return the tariff name en
     */
    public String getTariffNameEn() {
        return tariffNameEn;
    }

    /**
     * Sets tariff name en.
     *
     * @param tariffNameEn the tariff name en
     */
    public void setTariffNameEn(String tariffNameEn) {
        this.tariffNameEn = tariffNameEn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tariff)) {
            return false;
        }

        Tariff tariff = (Tariff) o;

        if (getId() != tariff.getId()) {
            return false;
        }
        if (!getTariffNameRu().equals(tariff.getTariffNameRu())) {
            return false;
        }
        return getTariffNameEn().equals(tariff.getTariffNameEn());

    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + getTariffNameRu().hashCode();
        result = 31 * result + getTariffNameEn().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "id=" + id +
                ", tariffNameRu='" + tariffNameRu + '\'' +
                ", tariffNameEn='" + tariffNameEn + '\'' +
                ", price=" + price +
                ", period=" + period +
                ", abilityList=" + abilityList.toString() +
                ", archival=" + archival +
                '}';
    }
}
