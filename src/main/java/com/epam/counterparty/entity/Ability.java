package com.epam.counterparty.entity;

/**
 * The type Ability.
 *
 * @author Pavel
 * @since 16.06.2019
 */
public class Ability extends Entity {
    private long id;
    private String abilityNameEn;
    private String abilityNameRu;
    private long tariffAbilityId;

    /**
     * Instantiates a new Ability.
     */
    public Ability() {
    }

    /**
     * Instantiates a new Ability.
     *
     * @param id the id
     */
    public Ability(long id) {
        this.id = id;
    }

    /**
     * Instantiates a new Ability.
     *
     * @param abilityNameEn the ability name en
     * @param abilityNameRu the ability name ru
     */
    public Ability(String abilityNameEn, String abilityNameRu) {
        this.abilityNameEn = abilityNameEn;
        this.abilityNameRu = abilityNameRu;
    }

    /**
     * Instantiates a new Ability.
     *
     * @param abilityId     the ability id
     * @param abilityNameEn the ability name en
     * @param abilityNameRu the ability name ru
     */
    public Ability(long abilityId, String abilityNameEn, String abilityNameRu) {
        this.id = abilityId;
        this.abilityNameEn = abilityNameEn;
        this.abilityNameRu = abilityNameRu;
    }

    /**
     * Instantiates a new Ability.
     *
     * @param abilityId       the ability id
     * @param abilityNameEn   the ability name en
     * @param abilityNameRu   the ability name ru
     * @param tariffAbilityId the tariff ability id
     */
    public Ability(long abilityId, String abilityNameEn, String abilityNameRu, long tariffAbilityId) {
        this.id = abilityId;
        this.abilityNameEn = abilityNameEn;
        this.abilityNameRu = abilityNameRu;
        this.tariffAbilityId = tariffAbilityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ability)) {
            return false;
        }

        Ability ability = (Ability) o;

        if (getId() != ability.getId()) {
            return false;
        }
        if (!getAbilityNameEn().equals(ability.getAbilityNameEn())) {
            return false;
        }
        return getAbilityNameRu().equals(ability.getAbilityNameRu());

    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + getAbilityNameEn().hashCode();
        result = 31 * result + getAbilityNameRu().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Abilities{" +
                "id=" + id +
                ", abilityNameEn='" + abilityNameEn + '\'' +
                ", abilityNameRu='" + abilityNameRu + '\'' +
                ", tariffAbilityId='" + tariffAbilityId + '\'' +
                '}';
    }


    /**
     * Gets tariff ability id.
     *
     * @return the tariff ability id
     */
    public long getTariffAbilityId() {
        return tariffAbilityId;
    }

    /**
     * Sets tariff ability id.
     *
     * @param tariffAbilityId the tariff ability id
     */
    public void setTariffAbilityId(long tariffAbilityId) {
        this.tariffAbilityId = tariffAbilityId;
    }

    @Override
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets ability name en.
     *
     * @return the ability name en
     */
    public String getAbilityNameEn() {
        return abilityNameEn;
    }

    /**
     * Sets ability name en.
     *
     * @param abilityNameEn the ability name en
     */
    public void setAbilityNameEn(String abilityNameEn) {
        this.abilityNameEn = abilityNameEn;
    }

    /**
     * Gets ability name ru.
     *
     * @return the ability name ru
     */
    public String getAbilityNameRu() {
        return abilityNameRu;
    }

    /**
     * Sets ability name ru.
     *
     * @param abilityNameRu the ability name ru
     */
    public void setAbilityNameRu(String abilityNameRu) {
        this.abilityNameRu = abilityNameRu;
    }
}
