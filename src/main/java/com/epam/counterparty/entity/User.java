package com.epam.counterparty.entity;

import lombok.*;

/**
 * The type User.
 *
 * @author Pavel
 * @since 19.05.2019
 */
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode(callSuper = false)
public class User extends Entity {
    private long id;
    private String password;
    private String email;
    @EqualsAndHashCode.Exclude private byte role;
    @EqualsAndHashCode.Exclude private boolean deleted;
    @EqualsAndHashCode.Exclude private Profile profile;
}
