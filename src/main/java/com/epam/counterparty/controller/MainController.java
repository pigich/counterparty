package com.epam.counterparty.controller;

import com.epam.counterparty.command.*;
import com.epam.counterparty.command.impl.forwad.ToMainPageCommand;
import com.epam.counterparty.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static com.epam.counterparty.command.Attribute.DO_COMMAND;
import static com.epam.counterparty.command.Attribute.TO_ERROR_PAGE;

/**
 * The {@code MainController} class represents main Servlet logic.
 *
 * @author Pavel
 * @since 25.05.2019
 */
@WebServlet("/do")
public class MainController extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String ERROR_IN_PROCESS_REQUEST = "Error in processRequest method of MainController";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        processRequest(req, res, new RequestContent());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        processRequest(req, res, new RequestContent());
    }

    /**
     * Extracts HttpServletRequest to RequestContent object, hen defines command, performs logic and performs transfer
     * to next command or path
     *
     * @param req     the HttpServletRequest object
     * @param res     the HttpServletResponse object
     * @param content the RequestContent object
     * @throws ServletException the servlet exception
     * @throws IOException      the io exception
     */
    private void processRequest(HttpServletRequest req, HttpServletResponse res, RequestContent content) throws ServletException, IOException {
        try {
            content.extractValues(req);
            Optional<Command> optional = CommandFactory.defineCommand(content);
            Command command = optional.orElseGet(ToMainPageCommand::new);
            Router router;
            router = command.execute(content);
            if (!command.getClass().isInstance(ToMainPageCommand.class)) {
                content.insertAttributes(req);
            }
            if (router.getSendingType().equals(SendingType.FORWARD)) {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(router.getPath());
                dispatcher.forward(req, res);
            } else {
                res.sendRedirect(req.getContextPath() + router.getPath());
            }
        } catch (CommandException e) {
            LOGGER.error(ERROR_IN_PROCESS_REQUEST, e);
            res.sendRedirect(DO_COMMAND + TO_ERROR_PAGE);
        }
    }
}
