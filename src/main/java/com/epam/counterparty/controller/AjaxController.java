package com.epam.counterparty.controller;

import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.ajax.AjaxCommand;
import com.epam.counterparty.command.ajax.AjaxCommandFactory;
import com.epam.counterparty.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static com.epam.counterparty.command.Attribute.*;

/**
 * The {@code AjaxController} is a Servlet for Ajax commands.
 *
 * @author Pavel
 * @since 21.06.2019
 */
@WebServlet("/ado")
public class AjaxController extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String AND_SYMBOL = "&";
    private static final String EQUAL_SYMBOL = "=";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    /**
     * Extracts HttpServletRequest to RequestContent object, then defines command, performs logic and returns response
     * in String format
     *
     * @param request  the request
     * @param response the response
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) {
        RequestContent content = new RequestContent();
        try {
            content.extractValues(request);
            AjaxCommand ajaxCommand = AjaxCommandFactory.defineCommand(content);
            PrintWriter out = response.getWriter();
            String responseString = ajaxCommand.execute(content);
            out.print(responseString);
        } catch (CommandException | IOException e) {
            try {
                response.sendRedirect(DO_COMMAND + TO_ERROR_PAGE + AND_SYMBOL + ERROR_MESSAGE + EQUAL_SYMBOL + e.getMessage());
            } catch (IOException ex) {
                LOGGER.error(ex.getMessage());
            }
        }
    }
}
