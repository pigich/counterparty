package com.epam.counterparty.dao;

import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.exception.DaoException;

import java.util.List;

/**
 * The {@code AbilityDao} interface represents methods to work with {@code Ability} entity.
 * @author Pavel
 * @since 07.07.2019
 */
public interface AbilityDao extends BaseDao<Ability> {

    /**
     * Finds {@code Ability} in database by name id and puts them to the list.
     *
     * @param nameEn the name en
     * @param nameRu the name ru
     * @return the ability
     * @throws DaoException the dao exception
     */
    Ability findByName(String nameEn, String nameRu) throws DaoException;

    /**
     * Finds {@code Ability} in database by tariff id and puts them to the list.
     *
     * @param id the id
     * @return the list of Abilities
     * @throws DaoException the dao exception
     */
    List<Ability> findByTariffId(long id) throws DaoException;

    /**
     * Finds {@code Ability} in database by tariff state and puts them to the list.
     *
     * @param tariffState the tariff state (archival or not)
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Ability> findByTariffState(boolean tariffState) throws DaoException;

    /**
     * Finds all {@code Ability} in database and puts them to the list.
     *
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Ability> findAllAbilities() throws DaoException;
}
