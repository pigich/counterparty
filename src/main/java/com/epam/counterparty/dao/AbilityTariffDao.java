package com.epam.counterparty.dao;

import com.epam.counterparty.entity.AbilityTariff;
import com.epam.counterparty.exception.DaoException;

/**
 * The interface {@code AbilityTariffDao} represents methods to work with {@code AbilityTariff} entity.
 *
 * @author Pavel
 * @see AbilityTariff
 * @since 07.07.2019
 */
public interface AbilityTariffDao extends BaseDao<AbilityTariff> {
    /**
     * Finds {@code AbilityTariff} in database by id and attribute name.
     *
     * @param id            the id
     * @param attributeName the attribute name
     * @return the ability tariff
     * @throws DaoException the dao exception
     */
    AbilityTariff findById(String id, String attributeName) throws DaoException;

    /**
     * Deletes {@code AbilityTariff} in database by id and attribute name.
     *
     * @param id            the id
     * @param attributeName the attribute name
     * @throws DaoException the dao exception
     */
    void delete(long id, String attributeName) throws DaoException;
}
