package com.epam.counterparty.dao;

import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.exception.DaoException;

import java.util.List;

/**
 * The {@code ProfileDao} interface represents methods to work with {@code Profile} entity.
 *
 * @author Pavel
 * @see Profile
 * @since 07.07.2019
 */
public interface ProfileDao extends BaseDao<Profile> {

    /**
     * Updates tariff in database.
     *
     * @param profile the profile
     * @throws DaoException the dao exception
     */
    void updateTariff(Profile profile) throws DaoException;

    /**
     * Finds all profiles list.
     *
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Profile> findAllProfiles() throws DaoException;
}
