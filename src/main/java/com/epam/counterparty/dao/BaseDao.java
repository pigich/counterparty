package com.epam.counterparty.dao;

import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.exception.DaoException;

/**
 * the {@code BaseDao} interface defines CRUD operations for working with database.
 *
 * @param <T> the type parameter is a generic type which extends {@link Entity}
 * @author Pavel
 * @since 26.05.2019
 */
public interface BaseDao<T extends Entity> {
    /**
     * Creates data in database.
     *
     * @param t the generic type which extends {@link Entity}
     * @throws DaoException the dao exception
     */
    void create(T t) throws DaoException;

    /**
     * Finds data in database by id entity.
     *
     * @param id the id
     * @return the entity
     * @throws DaoException the dao exception
     */
    Entity findById(long id) throws DaoException;

    /**
     * Updates data in database.
     *
     * @param t the generic type which extends {@link Entity}
     * @throws DaoException the dao exception
     */
    void update(T t) throws DaoException;

    /**
     * Deletes data in database by id.
     *
     * @param id the id
     * @throws DaoException the dao exception
     */
    void delete(long id) throws DaoException;
}
