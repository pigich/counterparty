package com.epam.counterparty.dao;

import com.epam.counterparty.dao.impl.*;
import com.epam.counterparty.entity.*;
import com.epam.counterparty.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.MESSAGE_ENTITY_CREATING_ERROR;
import static com.epam.counterparty.command.Message.MESSAGE_NOT_RECOGNIZED_ENTITY_TYPE;

/**
 * The {@code EntityCreator} class creates entity from {@code ResultSet} object data.
 *
 * @param <T> the type of elements returned by the EntityCreator
 * @author Pavel
 * @since 26.05.2019
 */
public class EntityCreator<T> {
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Creates generic type.
     *
     * @param table the table name in database
     * @param rs    the ResultSet object
     * @return the generic type
     * @throws DaoException the dao exception
     */
    @SuppressWarnings("unchecked")
    public T create(String table, ResultSet rs) throws DaoException {
        try {
            Entity entity;
            switch (table) {
                case QuantityDaoImpl.ROWS_COUNT:
                    entity = Quantity.builder()
                            .rowsCount(rs.getLong(ROWS_COUNT))
                            .build();
                    break;
                case UserDaoImpl.TABLE_NAME:
                    entity = User.builder()
                            .id(rs.getLong(USER_ID))
                            .password(rs.getString(PASSWORD))
                            .email(rs.getString(EMAIL))
                            .role(rs.getByte(ROLE_ID))
                            .deleted(rs.getBoolean(DELETED))
                            .build();
                    break;
                case UserDaoImpl.USERS_AND_PROFILE:
                    entity = User.builder()
                            .id(rs.getLong(USER_ID))
                            .email(rs.getString(EMAIL))
                            .role(rs.getByte(ROLE_ID))
                            .deleted(rs.getBoolean(DELETED))
                            .profile(Profile.builder()
                                    .fullName(rs.getString(FULL_NAME))
                                    .orgName(rs.getString(ORGANIZATION_NAME))
                                    .orgUnp(rs.getInt(ORGANIZATION_UNP))
                                    .phone(rs.getString(PHONE))
                                    .tariffId(rs.getLong(TARIFF_ID))
                                    .build())
                            .build();
                    break;
                case PaymentDaoImpl.TABLE_NAME:
                    Payment payment = new Payment();
                    payment.setId(rs.getLong(ID));
                    payment.setUserId(rs.getLong(USER_ID));
                    payment.setTariffId(rs.getLong(TARIFF_ID));
                    payment.setCurrentBalance(rs.getBigDecimal(CURRENT_BALANCE));
                    payment.setStartDate(rs.getTimestamp(PAYMENTS_START_DATE).toInstant());
                    payment.setEndDate(rs.getTimestamp(PAYMENTS_END_DATE).toInstant());
                    entity = payment;
                    break;
                case ProfileDaoImpl.TABLE_NAME:
                    entity = Profile.builder()
                            .fullName(rs.getString(FULL_NAME))
                            .userId(rs.getLong(USER_ID))
                            .orgName(rs.getString(ORGANIZATION_NAME))
                            .orgUnp(rs.getInt(ORGANIZATION_UNP))
                            .phone(rs.getString(PHONE))
                            .tariffId(rs.getInt(TARIFF_ID))
                            .build();
                    break;
                case TariffDaoImpl.TABLE_NAME:
                    Tariff tariff = new Tariff();
                    tariff.setId(rs.getLong(TARIFF_ID));
                    tariff.setTariffNameEn(rs.getString(TARIFF_NAME_EN));
                    tariff.setTariffNameRu(rs.getString(TARIFF_NAME_RU));
                    tariff.setPeriod(rs.getShort(PERIOD));
                    tariff.setPrice(rs.getBigDecimal(PRICE));
                    tariff.setArchival(rs.getBoolean(ARCHIVAL));
                    entity = tariff;
                    break;
                case AbilityDaoImpl.TABLE_NAME:
                    Ability ability = new Ability();
                    ability.setId(rs.getLong(ABILITY_ID));
                    ability.setAbilityNameEn(rs.getString(ABILITY_NAME_EN));
                    ability.setAbilityNameRu(rs.getString(ABILITY_NAME_RU));
                    entity = ability;
                    break;
                case AbilityDaoImpl.TABLE_WITH_TARIFF_ID:
                    ability = new Ability();
                    ability.setId(rs.getLong(ABILITY_ID));
                    ability.setTariffAbilityId(rs.getLong(TARIFF_ID));
                    ability.setAbilityNameEn(rs.getString(ABILITY_NAME_EN));
                    ability.setAbilityNameRu(rs.getString(ABILITY_NAME_RU));
                    entity = ability;
                    break;
                case AbilityTariffDaoImpl.TABLE_NAME:
                    AbilityTariff abilityTariff = new AbilityTariff();
                    abilityTariff.setAbilityId(rs.getLong(ABILITY_ID));
                    abilityTariff.setTariffId(rs.getLong(TARIFF_ID));
                    entity = abilityTariff;
                    break;
                case FeesDaoImpl.TABLE_NAME:
                    Fee fee = new Fee();
                    fee.setId(rs.getLong(ID));
                    fee.setUserId(rs.getLong(USER_ID));
                    fee.setAmount(rs.getBigDecimal(AMOUNT));
                    fee.setFeesDate(rs.getTimestamp(FEE_DATE).toInstant());
                    entity = fee;
                    break;
                default:
                    throw new DaoException(MESSAGE_NOT_RECOGNIZED_ENTITY_TYPE);
            }
            return (T) entity;
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_ENTITY_CREATING_ERROR, e);
            throw new DaoException(MESSAGE_ENTITY_CREATING_ERROR, e);
        }
    }
}
