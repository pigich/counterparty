package com.epam.counterparty.dao;

import com.epam.counterparty.entity.Fee;
import com.epam.counterparty.exception.DaoException;

import java.math.BigDecimal;
import java.util.List;

/**
 * The {@code FeesDao} interface represents methods to work with {@code Fee} entity.
 *
 * @author Pavel
 * @see Fee
 * @since 07.07.2019
 */
public interface FeesDao extends BaseDao<Fee> {
    /**
     * Creates fee in database.
     *
     * @param userId the user id
     * @param amount the amount
     * @throws DaoException the dao exception
     */
    void createFee(long userId, BigDecimal amount) throws DaoException;

    /**
     * Finds all fees in database and returns a list of them.
     *
     * @param currentPage the current page
     * @param rowsPerPage the rows per page
     * @param orderColumn the order column
     * @param orderType   the order type
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Fee> findAllFees(long currentPage, int rowsPerPage, int orderColumn , String orderType) throws DaoException;

    /**
     * Finds fees in database by searching value and returns a list of them.
     *
     * @param searchingValue the searching value
     * @param page           the page
     * @param rowsPerPage    the rows per page
     * @param orderColumn    the order column
     * @param orderType      the order type
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Fee> findFeesBySearchingValue(String searchingValue, long page, int rowsPerPage, int orderColumn , String orderType) throws DaoException;
}
