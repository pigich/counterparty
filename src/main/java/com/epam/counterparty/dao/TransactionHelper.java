package com.epam.counterparty.dao;

import com.epam.counterparty.connection.ConnectionPool;
import com.epam.counterparty.connection.ProxyConnection;
import com.epam.counterparty.dao.impl.BaseDaoImpl;
import com.epam.counterparty.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The {@code TransactionHelper} class represents logic for creating transactions to database.
 *
 * @author Pavel
 * @since 18.05.2019
 */
public class TransactionHelper {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String ROLLBACK_TRANSACTION_EXCEPTION = "SQL rollback exception";
    private static final String COMMIT_TRANSACTION_EXCEPTION = "Commit transaction exception";
    private static final String END_TRANSACTION_EXCEPTION = "End transaction exception";
    private static final String BEGIN_TRANSACTION_EXCEPTION = "Begin transaction exception";
    private Connection connection = ConnectionPool.getInstance().getConnection();
    private BaseDaoImpl[] daoArray;

    /**
     * Begins transaction.
     *
     * @param dao the dao
     * @throws DaoException the dao exception
     */
    public void beginTransaction(BaseDaoImpl... dao) throws DaoException {
        try {
            connection.setAutoCommit(false);
            daoArray = dao;
            for (BaseDaoImpl d : dao) {
                d.setConnection(connection);
                d.setInAction(true);
            }
        } catch (SQLException e) {
            LOGGER.error(BEGIN_TRANSACTION_EXCEPTION, e);
            throw new DaoException(BEGIN_TRANSACTION_EXCEPTION, e);
        }
    }

    /**
     * Ends transaction.
     *
     * @throws DaoException the dao exception
     */
    public void endTransaction() throws DaoException {
        try {
            connection.setAutoCommit(true);
            ConnectionPool.getInstance().releaseConnection((ProxyConnection) connection);
            for (BaseDaoImpl d : daoArray) {
                d.setInAction(false);
            }
            daoArray = null;
        } catch (SQLException e) {
            LOGGER.error(END_TRANSACTION_EXCEPTION, e);
            throw new DaoException(END_TRANSACTION_EXCEPTION, e);
        }
    }

    /**
     * Commits finished transaction.
     *
     * @throws DaoException the dao exception
     */
    public void commit() throws DaoException {
        try {
            connection.commit();
        } catch (SQLException e) {
            LOGGER.error(COMMIT_TRANSACTION_EXCEPTION, e);
            throw new DaoException(COMMIT_TRANSACTION_EXCEPTION, e);
        }
    }

    /**
     * Rollbacks transaction.
     *
     * @throws DaoException the dao exception
     */
    public void rollback() throws DaoException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            LOGGER.error(ROLLBACK_TRANSACTION_EXCEPTION, e);
            throw new DaoException(ROLLBACK_TRANSACTION_EXCEPTION, e);
        }
    }
}
