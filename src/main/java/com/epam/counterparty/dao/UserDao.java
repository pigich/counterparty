package com.epam.counterparty.dao;

import com.epam.counterparty.entity.User;
import com.epam.counterparty.exception.DaoException;

import java.util.List;

/**
 * The {@code UserDao} interface represents methods to work with {@code User} entity.
 *
 * @author Pavel
 * @see User
 * @since 07.07.2019
 */
public interface UserDao extends BaseDao<User> {
    /**
     * Finds users by role list.
     *
     * @param role the role
     * @return the list
     * @throws DaoException the dao exception
     */
    List<User> findUsersByRole(String role) throws DaoException;

    /**
     * Finds email user.
     *
     * @param email the email
     * @return the user
     * @throws DaoException the dao exception
     */
    User findEmail(String email) throws DaoException;

    /**
     * Finds users list.
     *
     * @param currentPage the current page
     * @param rowsPerPage the rows per page
     * @return the list
     * @throws DaoException the dao exception
     */
    List<User> findUsers(int currentPage, int rowsPerPage) throws DaoException;

    /**
     * Blocks and unlocks user in database.
     *
     * @param userId     the user id
     * @param userStatus the user status
     * @throws DaoException the dao exception
     */
    void blockUser(long userId, boolean userStatus) throws DaoException;

    /**
     * Finds by tariff id list.
     *
     * @param tariffId    the tariff id
     * @param currentPage the current page
     * @param rowsPerPage the rows per page
     * @return the list
     * @throws DaoException the dao exception
     */
    List<User> findByTariffId(long tariffId, int currentPage, int rowsPerPage) throws DaoException;

    /**
     * Finds users by value list.
     *
     * @param searchingValue the searching value
     * @param currentPage    the current page
     * @param rowsPerPage    the rows per page
     * @return the list
     * @throws DaoException the dao exception
     */
    List<User> findUsersByValue(String searchingValue, int currentPage, int rowsPerPage) throws DaoException;

    /**
     * Finds users by value and tariff list.
     *
     * @param searchingValue the searching value
     * @param tariffId       the tariff id
     * @param currentPage    the current page
     * @param rowsPerPage    the rows per page
     * @return the list
     * @throws DaoException the dao exception
     */
    List<User> findUsersByValueAndTariff(String searchingValue, long tariffId, int currentPage, int rowsPerPage) throws DaoException;
}
