package com.epam.counterparty.dao;

import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.exception.DaoException;

import java.math.BigDecimal;
import java.util.List;

/**
 * The {@code PaymentDao} interface represents methods to work with {@code Payment} entity.
 *
 * @author Pavel
 * @see Payment
 * @since 07.07.2019
 */
public interface PaymentDao extends BaseDao<Payment> {
    /**
     * Creates payment in database.
     *
     * @param payment the payment
     * @throws DaoException the dao exception
     */
    void createPayment(Payment payment) throws DaoException;

    /**
     * Updates payment in database by id.
     *
     * @param userId the user id
     * @param amount the amount
     * @throws DaoException the dao exception
     */
    void updatePayment(long userId, BigDecimal amount) throws DaoException;

    /**
     * Updates payment by userId for tariff and period.
     *
     * @param period   the period
     * @param amount   the amount
     * @param tariffId the tariff id
     * @param userId   the user id
     * @throws DaoException the dao exception
     */
    void updatePayment(String period, BigDecimal amount, long tariffId, long userId) throws DaoException;

    /**
     * Finds all payments list.
     *
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Payment> findAllPayments() throws DaoException;

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#findById(long)
     */
    Payment findById(long id) throws DaoException;
}
