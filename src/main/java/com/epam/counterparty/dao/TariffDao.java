package com.epam.counterparty.dao;

import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.DaoException;

import java.util.List;

/**
 * The {@code TariffDao} interface represents methods to work with {@code Tariff} entity.
 *
 * @author Pavel
 * @see Tariff
 * @since 07.07.2019
 */
public interface TariffDao extends BaseDao<Tariff> {
    /**
     * {@inheritDoc}
     *
     * @see BaseDao#findById(long)
     */
    Tariff findById(long id) throws DaoException;

    /**
     * Finds all tariffs in database by tariff name.
     *
     * @param nameEn the tariff name in english
     * @param nameRu the tariff name in russian
     * @return the tariff
     * @throws DaoException the dao exception
     */
    Tariff findByName(String nameEn, String nameRu) throws DaoException;

    /**
     * Finds all tariffs in database and returns a list of them.
     *
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Tariff> findAllTariffs() throws DaoException;

    /**
     * Adds AbilityTariff to database.
     *
     * @param tariff the Tariff object
     * @throws DaoException the dao exception
     */
    void addAbilityTariff(Tariff tariff) throws DaoException;

    /**
     * Finds working tariffs in database and returns a list of them.
     *
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Tariff> findWorkingTariffs() throws DaoException;
}
