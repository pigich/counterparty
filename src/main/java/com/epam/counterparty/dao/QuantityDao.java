package com.epam.counterparty.dao;

import com.epam.counterparty.entity.Quantity;
import com.epam.counterparty.exception.DaoException;

/**
 * The {@code QuantityDao} interface represents methods to work with {@code Quantity} entity.
 *
 * @author Pavel
 * @see Quantity
 * @since 07.07.2019
 */
public interface QuantityDao extends BaseDao<Quantity> {
    /**
     * Finds quantity of users by tariff id quantity.
     *
     * @param tariffId the tariff id
     * @return the quantity
     * @throws DaoException the dao exception
     */
    Quantity findQuantityOfUsersByTariffId(long tariffId) throws DaoException;

    /**
     * Finds total amount of users.
     *
     * @return the quantity
     * @throws DaoException the dao exception
     */
    Quantity findAllQuantityOfUsers() throws DaoException;

    /**
     * Finds quantity by value and tariff.
     *
     * @param searchingValue the searching value
     * @param tariffId       the tariff id
     * @return the quantity
     * @throws DaoException the dao exception
     */
    Quantity findQuantityByValueAndTariff(String searchingValue, long tariffId) throws DaoException;

    /**
     * Finds quantity by value.
     *
     * @param searchingValue the searching value
     * @return the quantity
     * @throws DaoException the dao exception
     */
    Quantity findQuantityByValue(String searchingValue) throws DaoException;

    /**
     * Finds quantity of fee by value.
     *
     * @param searchingValue the searching value
     * @return the quantity
     * @throws DaoException the dao exception
     */
    Quantity findQuantityOfFeeByValue(String searchingValue) throws DaoException;

    /**
     * Finds quantity of fees.
     *
     * @return the quantity
     * @throws DaoException the dao exception
     */
    Quantity findQuantityOfFees() throws DaoException;
}
