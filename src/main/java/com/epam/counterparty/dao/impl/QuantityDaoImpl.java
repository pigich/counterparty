package com.epam.counterparty.dao.impl;

import com.epam.counterparty.dao.AbilityDao;
import com.epam.counterparty.dao.BaseDao;
import com.epam.counterparty.dao.QuantityDao;
import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.entity.Quantity;
import com.epam.counterparty.exception.DaoException;
import com.epam.counterparty.util.CheckRoleUtil;
import com.epam.counterparty.util.DateTimeConverterUtil;
import org.intellij.lang.annotations.Language;

import java.util.List;

/**
 * The {@code AbilityDaoImpl} class is the implementation of AbilityDao
 *
 * @author Pavel
 * @see AbilityDao
 * @see BaseDaoImpl
 * @see BaseDao
 * @since 28.06.2019
 */
public class QuantityDaoImpl extends BaseDaoImpl<Quantity> implements QuantityDao {
    public static final String ROWS_COUNT = "rowsCount";
    private static final String OR_SYMBOL = "%";
    @Language("SQL")
    private static final String SQL_SELECT_QUANTITY_USERS_BY_TARIFF_ID =
            "select count(*) as rows_count from profile WHERE tariff_id=?";

    @Language("SQL")
    private static final String SQL_SELECT_QUANTITY_USERS = "select count(*) as rows_count from profile";

    @Language("SQL")
    private static final String SQL_SELECT_QUANTITY_USERS_BY_VALUE =
            "SELECT count(*) as rows_count FROM users u join profile p ON u.user_id=p.user_id " +
                    "WHERE u.user_id LIKE ? or u.email LIKE ? or u.role_id = ? " +
                    "or p.organization_name LIKE ? or p.phone LIKE ? or p.full_name=?";
    @Language("SQL")
    private static final String SQL_SELECT_QUANTITY_FEES_BY_VALUE =
            "SELECT count(*) as rows_count FROM fees f WHERE f.user_id LIKE ? or f.amount LIKE ? or f.id LIKE ?";
    @Language("SQL")
    private static final String SQL_SELECT_QUANTITY_FEES_BY_VALUE_DATE =
            "SELECT count(*) as rows_count FROM fees f WHERE f.user_id LIKE ? or f.amount LIKE ? or f.id LIKE ? or DATE_FORMAT(f.date, ? ) = ?";
    @Language("SQL")
    private static final String SQL_SELECT_QUANTITY_FEES = "SELECT count(*) as rows_count FROM fees";

    @Language("SQL")
    private static final String SQL_SELECT_QUANTITY_USERS_BY_VALUE_AND_TARIFF =
            "SELECT count(*) as rows_count FROM users u join profile p ON u.user_id=p.user_id " +
                    "WHERE u.user_id LIKE ? or u.email LIKE ? or u.role_id= ? " +
                    "or p.organization_name LIKE ? or p.phone LIKE ? or p.full_name=? AND p.tariff_id=?";

    /**
     * {@inheritDoc}
     *
     * @see QuantityDao#findQuantityOfUsersByTariffId(long)
     */
    @Override
    public Quantity findQuantityOfUsersByTariffId(long tariffId) throws DaoException {
        List<Quantity> tempList = findBy(SQL_SELECT_QUANTITY_USERS_BY_TARIFF_ID, ROWS_COUNT, tariffId);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see QuantityDao#findAllQuantityOfUsers()
     */
    @Override
    public Quantity findAllQuantityOfUsers() throws DaoException {
        List<Quantity> tempList = findBy(SQL_SELECT_QUANTITY_USERS, ROWS_COUNT);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see QuantityDao#findQuantityByValueAndTariff(String, long)
     */
    @Override
    public Quantity findQuantityByValueAndTariff(String searchingValue, long tariffId) throws DaoException {
        String value = OR_SYMBOL + searchingValue + OR_SYMBOL;
        CheckRoleUtil checkRoleUtil = new CheckRoleUtil();
        String role = checkRoleUtil.checkRole(searchingValue);
        List<Quantity> tempList = findBy(SQL_SELECT_QUANTITY_USERS_BY_VALUE_AND_TARIFF, ROWS_COUNT, value, value, role, value, value, value, tariffId);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see QuantityDao#findQuantityByValue(String)
     */
    @Override
    public Quantity findQuantityByValue(String searchingValue) throws DaoException {
        String value = OR_SYMBOL + searchingValue + OR_SYMBOL;
        CheckRoleUtil checkRoleUtil = new CheckRoleUtil();
        String role = checkRoleUtil.checkRole(searchingValue);
        List<Quantity> tempList = findBy(SQL_SELECT_QUANTITY_USERS_BY_VALUE, ROWS_COUNT, value, value, role, value, value, value);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see QuantityDao#findQuantityOfFeeByValue(String)
     */
    @Override
    public Quantity findQuantityOfFeeByValue(String searchingValue) throws DaoException {
        String value = OR_SYMBOL + searchingValue + OR_SYMBOL;
        List<Quantity> tempList;
        String[] date = DateTimeConverterUtil.convertSearchStringToDate(searchingValue);
        if (date.length != 0) {
            tempList = findBy(SQL_SELECT_QUANTITY_FEES_BY_VALUE_DATE, ROWS_COUNT, value, value, searchingValue, date[0], date[1]);
        } else {
            tempList = findBy(SQL_SELECT_QUANTITY_FEES_BY_VALUE, ROWS_COUNT, value, value, searchingValue);
        }
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see QuantityDao#findQuantityOfFees()
     */
    @Override
    public Quantity findQuantityOfFees() throws DaoException {
        List<Quantity> tempList = findAll(SQL_SELECT_QUANTITY_FEES, ROWS_COUNT);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#create(Entity)
     */
    @Override
    public void create(Quantity quantity) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#findById(long)
     */
    @Override
    public Entity findById(long id) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#update(Entity)
     */
    @Override
    public void update(Quantity quantity) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#delete(long)
     */
    @Override
    public void delete(long feeId) {
        throw new UnsupportedOperationException();
    }
}
