package com.epam.counterparty.dao.impl;

import com.epam.counterparty.dao.BaseDao;
import com.epam.counterparty.dao.EntityCreator;
import com.epam.counterparty.dao.FeesDao;
import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.entity.Fee;
import com.epam.counterparty.exception.DaoException;
import com.epam.counterparty.util.DateTimeConverterUtil;
import org.intellij.lang.annotations.Language;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * The {@code FeesDaoImpl} class is the implementation of FeesDao
 *
 * @author Pavel
 * @see FeesDao
 * @see BaseDaoImpl
 * @see BaseDao
 * @since 04.06.2019
 */
public class FeesDaoImpl extends BaseDaoImpl<Fee> implements FeesDao {
    /**
     * The TABLE_NAME constant represents the name of {@code Fee} entity in database
     * is used in EntityCreator class {@link EntityCreator}.
     */
    public static final String TABLE_NAME = "fees";
    private static final String OR_SYMBOL = "%";
    @Language("SQL")
    private static final String SQL_INSERT_NEW_FEE =
            "INSERT INTO `counterparty`.`fees` ( `user_id`, `amount` ) VALUES ( ? , ?)";
    @Language("SQL")
    private static final String SQL_FIND_FEES_SORT_ASC = "SELECT * FROM fees ORDER BY ? LIMIT ?, ?";
    @Language("SQL")
    private static final String SQL_FIND_FEES_SORT_DESC = "SELECT * FROM fees ORDER BY ? DESC LIMIT ?, ?";
    @Language("SQL")
    private static final String SQL_FIND_FEES_BY_VALUE_SORT_ASC =
            "SELECT * FROM fees f WHERE f.user_id LIKE ? or f.amount LIKE ? or f.id = ? ORDER BY ? LIMIT ?, ?";
    @Language("SQL")
    private static final String SQL_FIND_FEES_BY_VALUE_SORT_DESC =
            "SELECT * FROM fees f WHERE f.user_id LIKE ? or f.amount LIKE ? or f.id = ? ORDER BY ? DESC LIMIT ?, ?";
    @Language("SQL")
    private static final String SQL_FIND_FEES_BY_VALUE_WITH_DATE_SORT_ASC =
            "SELECT * FROM fees f WHERE f.user_id LIKE ? or f.amount LIKE ? or f.id = ? or DATE_FORMAT(f.date, ? ) = ? ORDER BY ? LIMIT ?, ?";
    @Language("SQL")
    private static final String SQL_FIND_FEES_BY_VALUE_WITH_DATE_SORT_DESC =
            "SELECT * FROM fees f WHERE f.user_id LIKE ? or f.amount LIKE ? or f.id = ? or DATE_FORMAT(f.date, ? ) = ? ORDER BY ? DESC  LIMIT ?, ?";
    private static final String ASC_ORDER = "asc";

    /**
     * {@inheritDoc}
     *
     * @see FeesDao#createFee(long, BigDecimal)
     */
    @Override
    public void createFee(long userId, BigDecimal amount) throws DaoException {
        PreparedStatement ps = null;
        try {
            ps = getConnection().prepareStatement(SQL_INSERT_NEW_FEE);
            ps.setLong(1, userId);
            ps.setBigDecimal(2, amount);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        } finally {
            if (isInAction()) {
                closeResources(ps);
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see FeesDao#findAllFees(long, int, int, String)
     */
    @Override
    public List<Fee> findAllFees(long currentPage, int rowsPerPage, int orderColumn, String orderType) throws DaoException {
        List<Fee> fees;
        if (orderType.equalsIgnoreCase(ASC_ORDER)) {
            fees = findBy(SQL_FIND_FEES_SORT_ASC, TABLE_NAME, orderColumn, currentPage, rowsPerPage);
        } else {
            fees = findBy(SQL_FIND_FEES_SORT_DESC, TABLE_NAME, orderColumn, currentPage, rowsPerPage);
        }
        return fees;
    }

    /**
     * {@inheritDoc}
     *
     * @see FeesDao#findFeesBySearchingValue(String, long, int, int, String)
     */
    @Override
    public List<Fee> findFeesBySearchingValue(String searchingValue, long currentPage, int rowsPerPage, int orderColumn, String orderType) throws DaoException {
        List<Fee> fees;
        String value = OR_SYMBOL + searchingValue + OR_SYMBOL;
        String[] date = DateTimeConverterUtil.convertSearchStringToDate(searchingValue);
        if (orderType.equalsIgnoreCase(ASC_ORDER)) {
            if (date.length != 0) {
                fees = findBy(SQL_FIND_FEES_BY_VALUE_WITH_DATE_SORT_ASC, TABLE_NAME, value, value, searchingValue, date[0], date[1], orderColumn, currentPage, rowsPerPage);
            } else {
                fees = findBy(SQL_FIND_FEES_BY_VALUE_SORT_ASC, TABLE_NAME, value, value, searchingValue, orderColumn, currentPage, rowsPerPage);
            }
        } else {
            if (date.length != 0) {
                fees = findBy(SQL_FIND_FEES_BY_VALUE_WITH_DATE_SORT_DESC, TABLE_NAME, value, value, searchingValue, date[0], date[1], orderColumn, currentPage, rowsPerPage);
            } else {
                fees = findBy(SQL_FIND_FEES_BY_VALUE_SORT_DESC, TABLE_NAME, value, value, searchingValue, orderColumn, currentPage, rowsPerPage);
            }
        }
        return fees;
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#create(Entity)
     */
    @Override
    public void create(Fee fee) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#findById(long)
     */
    @Override
    public Entity findById(long id) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#update(Entity)
     */
    @Override
    public void update(Fee fee) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#delete(long)
     */
    @Override
    public void delete(long feeId) {
        throw new UnsupportedOperationException();
    }
}
