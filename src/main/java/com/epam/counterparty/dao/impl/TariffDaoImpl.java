package com.epam.counterparty.dao.impl;

import com.epam.counterparty.dao.BaseDao;
import com.epam.counterparty.dao.EntityCreator;
import com.epam.counterparty.dao.TariffDao;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.intellij.lang.annotations.Language;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * The {@code TariffDaoImpl} class is the implementation of TariffDao
 *
 * @author Pavel
 * @see TariffDao
 * @see BaseDaoImpl
 * @see BaseDao
 * @since 11.06.2019
 */
public class TariffDaoImpl extends BaseDaoImpl<Tariff> implements TariffDao {
    /**
     * The TABLE_NAME constant represents the name of {@code Tariff} entity in database is used in EntityCreator class
     * {@link EntityCreator}.
     */
    public static final String TABLE_NAME = "tariff";
    private static final Logger LOGGER = LogManager.getLogger();
    @Language("SQL")
    private static final String SQL_FIND_ALL_TARIFFS = "SELECT * FROM tariff";
    @Language("SQL")
    private static final String
            FIND_TARIFF_BY_NAME = "SELECT * FROM tariff where tariff_name_en=? or tariff_name_ru=?";
    @Language("SQL")
    private static final String SQL_SET_TARIFF_ARCHIVAL = "UPDATE tariff SET archival=true WHERE tariff_id=?";
    @Language("SQL")
    private static final String FIND_TARIFF_BY_ID = "SELECT * FROM tariff WHERE tariff_id=?";
    @Language("SQL")
    private static final String SQL_CREATE_NEW_ABILITY_TARIFF = "INSERT INTO ability_tariff (ability_id, tariff_id) VALUES (?,?)";
    @Language("SQL")
    private static final String SQL_CREATE_NEW_TARIFF = "INSERT INTO tariff (tariff_name_en, tariff_name_ru, period, price) VALUES (?,?,?,?)";
    @Language("SQL")
    private static final String SQL_UPDATE_TARIFF = "UPDATE tariff SET tariff_name_en=?, tariff_name_ru=?, period=?, price=? WHERE tariff_id=?";
    @Language("SQL")
    private static final String SQL_FIND_WORKING_TARIFFS = "SELECT * FROM tariff WHERE archival=FALSE";

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#create(Entity)
     */
    @Override
    public void create(Tariff tariff) throws DaoException {
        String nameEn = tariff.getTariffNameEn();
        String nameRu = tariff.getTariffNameRu();
        String price = String.valueOf(tariff.getPrice());
        String period = String.valueOf(tariff.getPeriod());
        create(SQL_CREATE_NEW_TARIFF, nameEn, nameRu, period, price);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#findById(long)
     */
    @Override
    public Tariff findById(long id) throws DaoException {
        List<Tariff> tempList = findBy(FIND_TARIFF_BY_ID, TABLE_NAME, id);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffDao#findByName(String, String)
     */
    @Override
    public Tariff findByName(String nameEn, String nameRu) throws DaoException {
        List<Tariff> tempList = findBy(FIND_TARIFF_BY_NAME, TABLE_NAME, nameEn, nameRu);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#update(Entity)
     */
    @Override
    public void update(Tariff tariff) throws DaoException {
        String nameEn = tariff.getTariffNameEn();
        String nameRu = tariff.getTariffNameRu();
        String price = String.valueOf(tariff.getPrice());
        String period = String.valueOf(tariff.getPeriod());
        String tariffId = String.valueOf(tariff.getId());
        create(SQL_UPDATE_TARIFF, nameEn, nameRu, period, price, tariffId);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#delete(long)
     */
    @Override
    public void delete(long tariffId) throws DaoException {
        delete(SQL_SET_TARIFF_ARCHIVAL, tariffId);
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffDao#findAllTariffs()
     */
    @Override
    public List<Tariff> findAllTariffs() throws DaoException {
        List<Tariff> tariffs;
        tariffs = findAll(SQL_FIND_ALL_TARIFFS, TABLE_NAME);
        return tariffs;
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffDao#addAbilityTariff(Tariff)
     */
    @Override
    public void addAbilityTariff(Tariff tariff) throws DaoException {
        PreparedStatement preparedStatement;
        List<Ability> abilities = tariff.getAbilityList();
        try {
            preparedStatement = getConnection().prepareStatement(SQL_CREATE_NEW_ABILITY_TARIFF);
            for (Ability ability : abilities) {
                preparedStatement.setLong(1, ability.getId());
                preparedStatement.setLong(2, tariff.getId());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffDao#findWorkingTariffs()
     */
    @Override
    public List<Tariff> findWorkingTariffs() throws DaoException {
        List<Tariff> tariffs;
        tariffs = findAll(SQL_FIND_WORKING_TARIFFS, TABLE_NAME);
        return tariffs;
    }
}
