package com.epam.counterparty.dao.impl;

import com.epam.counterparty.dao.BaseDao;
import com.epam.counterparty.dao.EntityCreator;
import com.epam.counterparty.dao.PaymentDao;
import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.intellij.lang.annotations.Language;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * The {@code PaymentDaoImpl} class is the implementation of PaymentDao
 *
 * @author Pavel
 * @see PaymentDao
 * @see BaseDaoImpl
 * @see BaseDao
 * @since 03.06.2019
 */
public class PaymentDaoImpl extends BaseDaoImpl<Payment> implements PaymentDao {
    /**
     * The TABLE_NAME constant represents the name of {@code Payment} entity in database
     * is used in EntityCreator class {@link EntityCreator}.
     */
    public static final String TABLE_NAME = "payments";
    @Language("SQL")
    private static final String SQL_INSERT_NEW_PAYMENT =
            "INSERT INTO `counterparty`.`payments` (`user_id`,`current_balance`) VALUES ( ? , ?)";
    @Language("SQL")
    private static final String SQL_SELECT_PAYMENT_BY_USER_ID = "SELECT * FROM payments WHERE user_id=?";
    private static final String SQL_UPDATE_PAYMENT =
            "UPDATE payments SET payments.current_balance = payments.current_balance + ? WHERE payments.user_id=?";
    @Language("SQL")
    private static final String SQL_UPDATE_PAYMENT_WITH_TARIFF =
            "UPDATE payments SET payments.start_date=current_timestamp(), " +
                    "payments.end_date=current_timestamp()+ INTERVAL ? DAY, " +
                    "payments.current_balance = payments.current_balance - ?," +
                    "payments.tariff_id=? WHERE payments.user_id=?";

    @Language("SQL")
    private static final String SQL_FIND_ALL_PAYMENTS = "SELECT * FROM payments";

    /**
     * {@inheritDoc}
     *
     * @see PaymentDao#createPayment(Payment)
     */
    @Override
    public void createPayment(Payment payment) throws DaoException {
        String userId = String.valueOf(payment.getUserId());
        String balance = String.valueOf(payment.getCurrentBalance());
        create(SQL_INSERT_NEW_PAYMENT, userId, balance);
    }

    /**
     * {@inheritDoc}
     *
     * @see PaymentDao#findById(long)
     */
    @Override
    public Payment findById(long userId) throws DaoException {
        List<Payment> tempList = findBy(SQL_SELECT_PAYMENT_BY_USER_ID, TABLE_NAME, userId);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see PaymentDao#updatePayment(long, BigDecimal)
     */
    @Override
    public void updatePayment(long userId, BigDecimal amount) throws DaoException {
        PreparedStatement ps = null;
        try {
            ps = getConnection().prepareStatement(SQL_UPDATE_PAYMENT);
            ps.setBigDecimal(1, amount);
            ps.setLong(2, userId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        } finally {
            if (isInAction()) {
                closeResources(ps);
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see PaymentDao#updatePayment(String, BigDecimal, long, long)
     */
    @Override
    public void updatePayment(String period, BigDecimal amount, long tariffId, long userId) throws DaoException {
        PreparedStatement ps = null;
        try {
            ps = getConnection().prepareStatement(SQL_UPDATE_PAYMENT_WITH_TARIFF);
            ps.setShort(1, Short.parseShort(period));
            ps.setBigDecimal(2, amount);
            ps.setLong(3, tariffId);
            ps.setLong(4, userId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        } finally {
            if (isInAction()) {
                closeResources(ps);
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see PaymentDao#findAllPayments()
     */
    @Override
    public List<Payment> findAllPayments() throws DaoException {
        List<Payment> payments;
        payments = findAll(SQL_FIND_ALL_PAYMENTS, TABLE_NAME);
        return payments;
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#create(Entity)
     */
    @Override
    public void update(Payment payment) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#create(Entity)
     */
    @Override
    public void create(Payment payment) {

    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#delete(long)
     */
    @Override
    public void delete(long paymentId) {

    }
}
