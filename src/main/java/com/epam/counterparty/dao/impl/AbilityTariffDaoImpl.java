package com.epam.counterparty.dao.impl;

import com.epam.counterparty.dao.AbilityTariffDao;
import com.epam.counterparty.dao.BaseDao;
import com.epam.counterparty.dao.EntityCreator;
import com.epam.counterparty.entity.AbilityTariff;
import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.exception.DaoException;
import org.intellij.lang.annotations.Language;

import java.util.List;

import static com.epam.counterparty.command.Attribute.TARIFF_ID;

/**
 * The {@code AbilityTariffDaoImpl} class is the implementation of AbilityTariffDao
 *
 * @author Pavel
 * @see AbilityTariffDao
 * @see BaseDaoImpl
 * @see BaseDao
 * @since 21.06.2019
 */
public class AbilityTariffDaoImpl extends BaseDaoImpl<AbilityTariff> implements AbilityTariffDao {
    /**
     * The TABLE_NAME constant represents the name of {@code AbilityTariff} entity in database is used in EntityCreator
     * class {@link EntityCreator}.
     */
    public static final String TABLE_NAME = "ability_tariff";
    @Language("SQL")
    private static final String
            FIND_ABILITY_BY_TARIFF_ID = "SELECT * FROM ability_tariff where tariff_id=?";
    @Language("SQL")
    private static final String
            FIND_ABILITY_BY_ABILITY_ID = "SELECT * FROM ability_tariff where ability_id=?";
    @Language("SQL")
    private static final String DELETE_ABILITY_TARIFF_BY_TARIFF_ID = "DELETE FROM ability_tariff WHERE ability_tariff.tariff_id=?";
    @Language("SQL")
    private static final String DELETE_ABILITY_TARIFF_BY_ABILITY_ID = "DELETE FROM ability_tariff WHERE ability_tariff.ability_id=?";
    @Language("SQL")
    private static final String SQL_UPDATE_ABILITY = "UPDATE abilities SET ability_name_en=?, ability_name_ru=? WHERE ability_id=?";

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#create(Entity)
     */
    @Override
    public void create(AbilityTariff abilityTariff) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#findById(long)
     */
    @Override
    public Entity findById(long id) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see AbilityTariffDao#findById(String, String)
     */
    @Override
    public AbilityTariff findById(String id, String idName) throws DaoException {
        List<AbilityTariff> tempList;
        if (idName.equals(TARIFF_ID)) {
            tempList = findBy(FIND_ABILITY_BY_TARIFF_ID, TABLE_NAME, id);
        } else {
            tempList = findBy(FIND_ABILITY_BY_ABILITY_ID, TABLE_NAME, id);
        }
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#update(Entity)
     */
    @Override
    public void update(AbilityTariff abilityTariff) throws DaoException {
        String abilityId = String.valueOf(abilityTariff.getAbilityId());
        String tariffId = String.valueOf(abilityTariff.getTariffId());
        create(SQL_UPDATE_ABILITY, abilityId, tariffId);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#delete(long)
     */
    @Override
    public void delete(long t) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see AbilityTariffDao#delete(long, String)
     */
    @Override
    public void delete(long id, String attributeName) throws DaoException {
        if (attributeName.equals(TARIFF_ID)) {
            super.delete(DELETE_ABILITY_TARIFF_BY_TARIFF_ID, id);
        } else {
            super.delete(DELETE_ABILITY_TARIFF_BY_ABILITY_ID, id);
        }
    }

}
