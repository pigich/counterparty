package com.epam.counterparty.dao.impl;

import com.epam.counterparty.connection.ConnectionPool;
import com.epam.counterparty.connection.ProxyConnection;
import com.epam.counterparty.dao.BaseDao;
import com.epam.counterparty.dao.EntityCreator;
import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.counterparty.command.Message.MESSAGE_EXCEPTION_IN_CLOSING_PREPARED_STATEMENT;
import static com.epam.counterparty.command.Message.MESSAGE_EXCEPTION_IN_CLOSING_RESULT_SET;

/**
 * The Base dao class represents basic CRUD methods to work with database.
 *
 * @param <T> the type parameter
 * @author Pavel
 * @since 26.05.2019
 */
public abstract class BaseDaoImpl<T extends Entity> implements BaseDao<T> {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String INTEGER_TYPE = "Integer";
    private static final String LONG_TYPE = "Long";
    private static final String BOOLEAN_TYPE = "Boolean";
    private static final String INSTANT_TYPE = "Instant";
    private Connection connection;
    private boolean inAction;

    /**
     * Finds data in database.
     *
     * @param sqlQuery the sql query
     * @param table    the table where to search data
     * @param values   the values by which data will searched
     * @return the list of data with specific type {@link EntityCreator}
     * @throws DaoException the dao exception
     * @see EntityCreator
     */
    List<T> findBy(String sqlQuery, String table, Object... values) throws DaoException {
        List<T> entities = new ArrayList<>();
        if (!inAction) {
            connection = ConnectionPool.getInstance().getConnection();
        }
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            for (int i = 0; i < values.length; i++) {
                switch (values[i].getClass().getSimpleName()) {
                    case INTEGER_TYPE:
                        preparedStatement.setInt(i + 1, (Integer) values[i]);
                        break;
                    case LONG_TYPE:
                        preparedStatement.setLong(i + 1, (Long) values[i]);
                        break;
                    case BOOLEAN_TYPE:
                        preparedStatement.setBoolean(i + 1, (Boolean) values[i]);
                        break;
                    case INSTANT_TYPE:
                        preparedStatement.setTimestamp(i + 1, (Timestamp) values[i]);
                        break;
                    default:
                        preparedStatement.setString(i + 1, (String) values[i]);
                }
            }
            resultSet = preparedStatement.executeQuery();
            EntityCreator<T> creator = new EntityCreator<>();
            while (resultSet.next()) {
                T entity = creator.create(table, resultSet);
                entities.add(entity);
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        } finally {
            if (!inAction) {
                closeResources(preparedStatement, connection, resultSet);
            }
        }
        return entities;
    }

    /**
     * Finds data in database without any condition. Then puts founded data to list.
     *
     * @param sqlQuery the sql query
     * @param table    the table where to search data
     * @return the list of founded data
     * @throws DaoException the dao exception
     * @see EntityCreator
     */
    List<T> findAll(String sqlQuery, String table) throws DaoException {
        List<T> entities = new ArrayList<>();
        if (!inAction) {
            connection = ConnectionPool.getInstance().getConnection();
        }
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();
            EntityCreator<T> creator = new EntityCreator<>();
            while (resultSet.next()) {
                T entity = creator.create(table, resultSet);
                entities.add(entity);
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        } finally {
            if (!inAction) {
                closeResources(preparedStatement, connection, resultSet);
            }
        }
        return entities;
    }

    /**
     * Creates(puts) data in database.
     *
     * @param sqlQuery the sql query
     * @param values   the condition values
     * @throws DaoException the dao exception
     */
    public void create(String sqlQuery, String... values) throws DaoException {
        if (!inAction) {
            setConnection(ConnectionPool.getInstance().getConnection());
        }
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            for (int i = 0; i < values.length; i++) {
                preparedStatement.setString(i + 1, values[i]);
            }
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        } finally {
            if (!inAction) {
                closeResources(preparedStatement, connection);
            }
        }
    }

    /**
     * Deletes data in database by id.
     *
     * @param sqlQuery the sql query
     * @param id       the id
     * @throws DaoException the dao exception
     */
    public void delete(String sqlQuery, long id) throws DaoException {
        if (!inAction) {
            connection = ConnectionPool.getInstance().getConnection();
        }
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sqlQuery);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        } finally {
            if (!inAction) {
                closeResources(preparedStatement, connection);
            }
        }
    }

    /**
     * Closes PreparedStatement and connection resources.
     *
     * @param preparedStatement the prepared statement
     * @param connection        the connection
     */
    void closeResources(PreparedStatement preparedStatement, Connection connection) {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_EXCEPTION_IN_CLOSING_PREPARED_STATEMENT, e);
        }
        if (connection != null) {
            ConnectionPool.getInstance().releaseConnection((ProxyConnection) connection);
        }
    }

    /**
     * Closes PreparedStatement resources.
     *
     * @param preparedStatement the prepared statement
     */
    void closeResources(PreparedStatement preparedStatement) {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_EXCEPTION_IN_CLOSING_PREPARED_STATEMENT, e);
        }
    }

    /**
     * Closes PreparedStatement, connection and ResultSet resources.
     *
     * @param preparedStatement the PreparedStatement object
     * @param connection        the Connection object
     * @param rs                the ResultSet object
     */
    private void closeResources(PreparedStatement preparedStatement, Connection connection, ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_EXCEPTION_IN_CLOSING_RESULT_SET, e);
        }
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_EXCEPTION_IN_CLOSING_PREPARED_STATEMENT, e);
        }

        if (connection != null) {
            ConnectionPool.getInstance().releaseConnection((ProxyConnection) connection);
        }
    }

    /**
     * Gets connection.
     *
     * @return the connection
     */
    Connection getConnection() {
        return connection;
    }

    /**
     * Sets connection.
     *
     * @param connection the connection
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * Checks inAction boolean field.
     *
     * @return the boolean
     */
    boolean isInAction() {
        return inAction;
    }

    /**
     * Sets inAction boolean field.
     *
     * @param inAction the in action
     */
    public void setInAction(boolean inAction) {
        this.inAction = inAction;
    }
}
