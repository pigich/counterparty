package com.epam.counterparty.dao.impl;

import com.epam.counterparty.dao.AbilityDao;
import com.epam.counterparty.dao.BaseDao;
import com.epam.counterparty.dao.EntityCreator;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.intellij.lang.annotations.Language;

import java.util.List;

/**
 * The {@code AbilityDaoImpl} class is the implementation of AbilityDao
 *
 * @author Pavel
 * @see AbilityDao
 * @see BaseDaoImpl
 * @see BaseDao
 * @since 17.06.2019
 */
public class AbilityDaoImpl extends BaseDaoImpl<Ability> implements AbilityDao {
    /**
     * The constant TABLE_NAME represents the name of {@code Ability} entity in database is used in EntityCreator class
     * {@link EntityCreator}.
     */
    public static final String TABLE_NAME = "abilities";
    /**
     * The constant TABLE_WITH_TARIFF_ID represents the name of {@code Ability} entity with TariffId in database is used
     * in EntityCreator class{@link EntityCreator}.
     */
    public static final String TABLE_WITH_TARIFF_ID = "abilitiesWithTariffId";
    private static final Logger LOGGER = LogManager.getLogger();
    @Language("SQL")
    private static final String SQL_FIND_ALL_ABILITIES = "SELECT * FROM abilities";
    @Language("SQL")
    private static final String
            SQL_FIND_ABILITY_BY_NAME = "SELECT * FROM abilities where ability_name_en=? or ability_name_ru=?";
    @Language("SQL")
    private static final String SQL_DELETE_ABILITY_BY_ID = "DELETE FROM abilities WHERE ability_id=?";
    @Language("SQL")
    private static final String SQL_FIND_ABILITY_BY_ID = "SELECT t.tariff_id, a.ability_id, a.ability_name_en, a.ability_name_ru " +
            "FROM tariff t  JOIN ability_tariff at ON t.tariff_id=at.tariff_id  JOIN abilities a " +
            "ON at.ability_id=a.ability_id AND t.tariff_id=?";
    @Language("SQL")
    private static final String SQL_FIND_ABILITIES_TARIFF_STATE = "SELECT t.tariff_id as tariff_id, a.ability_id, a.ability_name_en, a.ability_name_ru " +
            "FROM tariff t  JOIN ability_tariff at ON t.tariff_id=at.tariff_id  JOIN abilities a " +
            "ON at.ability_id=a.ability_id AND t.archival=?";
    @Language("SQL")
    private static final String SQL_CREATE_NEW_ABILITY = "INSERT INTO abilities (ability_name_en, ability_name_ru) VALUES (?,?)";
    @Language("SQL")
    private static final String SQL_UPDATE_ABILITY = "UPDATE abilities SET ability_name_en=?, ability_name_ru=? WHERE ability_id=?";

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#create(Entity)
     */
    @Override
    public void create(Ability ability) throws DaoException {
        String nameEn = ability.getAbilityNameEn();
        String nameRu = ability.getAbilityNameRu();
        create(SQL_CREATE_NEW_ABILITY, nameEn, nameRu);
        LOGGER.info("ability created = " + ability.getAbilityNameEn());
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#findById(long)
     */
    @Override
    public Ability findById(long id) throws DaoException {
        List<Ability> tempList = findBy(SQL_FIND_ABILITY_BY_ID, TABLE_NAME, id);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see AbilityDao#findByName(String, String)
     */
    @Override
    public Ability findByName(String nameEn, String nameRu) throws DaoException {
        List<Ability> tempList = findBy(SQL_FIND_ABILITY_BY_NAME, TABLE_NAME, nameEn, nameRu);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see AbilityDao#findByTariffId(long)
     */
    @Override
    public List<Ability> findByTariffId(long id) throws DaoException {
        return findBy(SQL_FIND_ABILITY_BY_ID, TABLE_NAME, id);
    }

    /**
     * {@inheritDoc}
     *
     * @see AbilityDao#findByTariffState(boolean)
     */
    @Override
    public List<Ability> findByTariffState(boolean tariffState) throws DaoException {
        return findBy(SQL_FIND_ABILITIES_TARIFF_STATE, TABLE_WITH_TARIFF_ID, tariffState);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#update(Entity)
     */
    @Override
    public void update(Ability ability) throws DaoException {
        String nameEn = ability.getAbilityNameEn();
        String nameRu = ability.getAbilityNameRu();
        String abilityId = String.valueOf(ability.getId());
        create(SQL_UPDATE_ABILITY, nameEn, nameRu, abilityId);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#delete(long)
     */
    @Override
    public void delete(long abilityId) throws DaoException {
        delete(SQL_DELETE_ABILITY_BY_ID, abilityId);
    }

    /**
     * {@inheritDoc}
     *
     * @see AbilityDao#findAllAbilities()
     */
    @Override
    public List<Ability> findAllAbilities() throws DaoException {
        List<Ability> abilities;
        abilities = findAll(SQL_FIND_ALL_ABILITIES, TABLE_NAME);
        return abilities;
    }
}