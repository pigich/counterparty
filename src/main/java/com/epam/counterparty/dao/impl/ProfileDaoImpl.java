package com.epam.counterparty.dao.impl;

import com.epam.counterparty.dao.BaseDao;
import com.epam.counterparty.dao.EntityCreator;
import com.epam.counterparty.dao.ProfileDao;
import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.exception.DaoException;
import org.intellij.lang.annotations.Language;

import java.util.List;

/**
 * The {@code ProfileDaoImpl} class is the implementation of ProfileDao
 *
 * @author Pavel
 * @see ProfileDao
 * @see BaseDaoImpl
 * @see BaseDao
 * @since 07.06.2019
 */
public class ProfileDaoImpl extends BaseDaoImpl<Profile> implements ProfileDao {
    /**
     * The TABLE_NAME constant represents the name of {@code Profile} entity in database is used in EntityCreator class
     * {@link EntityCreator}.
     */
    public static final String TABLE_NAME = "profile";
    @Language("SQL")
    private static final String SQL_FIND_ALL_PROFILES = "SELECT * FROM profile";
    @Language("SQL")
    private static final String SQL_CREATE_NEW_PROFILE = "INSERT INTO profile " +
            "( `user_id`) VALUES ( ? )";
    @Language("SQL")
    private static final String SQL_FIND_PROFILE_BY_USER_ID = "SELECT * FROM profile where user_id=?";
    @Language("SQL")
    private static final String SQL_UPDATE_PROFILE = "UPDATE profile " +
            "SET profile.full_name=?, profile.phone=?, profile.organization_unp=?, " +
            "profile.organization_name=? WHERE  profile.user_id= ?";
    @Language("SQL")
    private static final String SQL_UPDATE_PROFILE_TARIFF = "UPDATE profile " +
            "SET profile.tariff_id=? WHERE  profile.user_id= ?";

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#create(Entity)
     */
    @Override
    public void create(Profile profile) throws DaoException {
        String userId = String.valueOf(profile.getUserId());
        create(SQL_CREATE_NEW_PROFILE, userId);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#findById(long)
     */
    @Override
    public Entity findById(long userId) throws DaoException {
        List<Profile> tempList = findBy(SQL_FIND_PROFILE_BY_USER_ID, TABLE_NAME, userId);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#update(Entity)
     */
    @Override
    public void update(Profile profile) throws DaoException {
        String userId = String.valueOf(profile.getUserId());
        String fullName = profile.getFullName();
        String phone = profile.getPhone();
        String orgUnp = String.valueOf(profile.getOrgUnp());
        String orgName = profile.getOrgName();
        create(SQL_UPDATE_PROFILE, fullName, phone, orgUnp, orgName, userId);
    }

    /**
     * {@inheritDoc}
     *
     * @see ProfileDao#updateTariff(Profile)
     */
    @Override
    public void updateTariff(Profile profile) throws DaoException {
        String tariffId = String.valueOf(profile.getTariffId());
        String userId = String.valueOf(profile.getUserId());
        create(SQL_UPDATE_PROFILE_TARIFF, tariffId, userId);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#delete(long)
     */
    @Override
    public void delete(long profileId) throws DaoException {

    }

    /**
     * {@inheritDoc}
     *
     * @see ProfileDao#findAllProfiles()
     */
    @Override
    public List<Profile> findAllProfiles() throws DaoException {
        List<Profile> profileList;
        profileList = findAll(SQL_FIND_ALL_PROFILES, TABLE_NAME);
        return profileList;
    }
}
