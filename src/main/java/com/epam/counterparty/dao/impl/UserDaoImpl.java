package com.epam.counterparty.dao.impl;

import com.epam.counterparty.connection.ConnectionPool;
import com.epam.counterparty.dao.BaseDao;
import com.epam.counterparty.dao.EntityCreator;
import com.epam.counterparty.dao.UserDao;
import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.entity.User;
import com.epam.counterparty.exception.DaoException;
import com.epam.counterparty.util.CheckRoleUtil;
import org.intellij.lang.annotations.Language;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * The {@code UserDaoImpl} class is the implementation of UserDao
 *
 * @author Pavel
 * @see UserDao
 * @see BaseDaoImpl
 * @see BaseDao
 * @since 19.05.2019
 */
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {
    /**
     * The TABLE_NAME constant represents the name of User entity in database is used in EntityCreator class {@link
     * EntityCreator}.
     */
    public static final String TABLE_NAME = "users";
    /**
     * The USERS_AND_PROFILE constant represents the name of User with Profile entity in database is used in
     * EntityCreator class {@link EntityCreator}.
     */
    public static final String USERS_AND_PROFILE = "usersAndProfile";
    private static final String OR_SYMBOL = "%";

    @Language("SQL")
    private static final String SQL_SELECT_USERS_BY_ROLE = "SELECT * FROM users WHERE role_id=?";
    @Language("SQL")
    private static final String SQL_FIND_ALL_USERS = "SELECT u.user_id, u.email, u.role_id, u.deleted, " +
            "p.tariff_id, p.organization_name, p.phone, p.full_name, " +
            "p.organization_unp FROM users u join profile p ON u.user_id=p.user_id LIMIT ?, ?";
    @Language("SQL")
    private static final String SQL_SELECT_USERS_BY_ID = "SELECT * FROM users WHERE user_id=?";
    @Language("SQL")
    private static final String SQL_SELECT_USER_BY_EMAIL = "SELECT * FROM users WHERE email=?";
    @Language("SQL")
    private static final String SQL_UPDATE_USER =
            "UPDATE users SET users.password =? WHERE users.user_id=?";

    @Language("SQL")
    private static final String SQL_CREATE_NEW_ACCOUNT = "INSERT INTO `counterparty`.`users` ( `email`, `password`, role_id)" +
            " VALUES ( ? , ?, ?)";
    @Language("SQL")
    private static final String SQL_BLOCK_USER_BY_ID = "UPDATE users SET deleted=? WHERE user_id=?";
    @Language("SQL")
    private static final String SQL_FIND_USERS_BY_TARIFF = "SELECT u.user_id, u.email, u.role_id, u.deleted, " +
            "p.tariff_id, p.organization_name, p.phone, p.full_name, " +
            "p.organization_unp FROM users u join profile p ON u.user_id=p.user_id AND p.tariff_id=? LIMIT ?, ?";
    @Language("SQL")
    private static final String SQL_FIND_USERS_BY_VALUE = "SELECT u.user_id, u.email, u.role_id, u.deleted, " +
            "p.tariff_id, p.organization_name, p.phone, p.full_name, " +
            "p.organization_unp FROM users u join profile p ON u.user_id=p.user_id " +
            "WHERE u.user_id LIKE ? or u.email LIKE ? or u.role_id=? or p.organization_name LIKE ?" +
            "or p.phone LIKE ? or p.full_name=? LIMIT ?, ?";
    @Language("SQL")
    private static final String SQL_FIND_USERS_BY_VALUE_AND_TARIFF = "SELECT u.user_id, u.email, u.role_id, u.deleted, " +
            "p.tariff_id, p.organization_name, p.phone, p.full_name, " +
            "p.organization_unp FROM users u join profile p ON u.user_id=p.user_id " +
            "WHERE u.user_id LIKE ? or u.email LIKE ? or u.role_id=? or p.organization_name LIKE ?" +
            "or p.phone LIKE ? or p.full_name=? AND p.tariff_id=? LIMIT ?, ?";

    /**
     * {@inheritDoc}
     *
     * @see UserDao#findUsersByRole(String)
     */
    @Override
    public List<User> findUsersByRole(String role) throws DaoException {
        return findBy(SQL_SELECT_USERS_BY_ROLE, TABLE_NAME, role);
    }

    /**
     * {@inheritDoc}
     *
     * @see UserDao#findEmail(String)
     */
    @Override
    public User findEmail(String email) throws DaoException {
        List<User> tempList = findBy(SQL_SELECT_USER_BY_EMAIL, TABLE_NAME, email);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#create(Entity)
     */
    @Override
    public void create(User user) throws DaoException {
        String email = user.getEmail();
        String password = user.getPassword();
        String role = String.valueOf(user.getRole());
        create(SQL_CREATE_NEW_ACCOUNT, email, password, role);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#findById(long)
     */
    @Override
    public Entity findById(long userId) throws DaoException {
        List<User> tempList = findBy(SQL_SELECT_USERS_BY_ID, TABLE_NAME, userId);
        return tempList.isEmpty() ? null : tempList.get(0);
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#delete(long)
     */
    @Override
    public void delete(long userId) {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     *
     * @see BaseDao#update(Entity)
     */
    @Override
    public void update(User user) throws DaoException {
        create(SQL_UPDATE_USER, user.getPassword(), String.valueOf(user.getId()));
    }

    /**
     * {@inheritDoc}
     *
     * @see UserDao#findUsers(int, int)
     */
    @Override
    public List<User> findUsers(int currentPage, int rowsPerPage) throws DaoException {
        List<User> users;
        users = findBy(SQL_FIND_ALL_USERS, USERS_AND_PROFILE, currentPage, rowsPerPage);
        return users;
    }

    /**
     * {@inheritDoc}
     *
     * @see UserDao#blockUser(long, boolean)
     */
    @Override
    public void blockUser(long userId, boolean userStatus) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_BLOCK_USER_BY_ID);
            preparedStatement.setBoolean(1, userStatus);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e.getMessage());
        } finally {
            closeResources(preparedStatement, connection);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see UserDao#findByTariffId(long, int, int)
     */
    @Override
    public List<User> findByTariffId(long tariffId, int currentPage, int rowsPerPage) throws DaoException {
        List<User> users;
        users = findBy(SQL_FIND_USERS_BY_TARIFF, USERS_AND_PROFILE, tariffId, currentPage, rowsPerPage);
        return users;
    }

    /**
     * {@inheritDoc}
     *
     * @see UserDao#findUsersByValue(String, int, int)
     */
    @Override
    public List<User> findUsersByValue(String searchingValue, int currentPage, int rowsPerPage) throws DaoException {
        List<User> users;
        CheckRoleUtil checkRoleUtil = new CheckRoleUtil();
        String role = checkRoleUtil.checkRole(searchingValue);
        String value = OR_SYMBOL + searchingValue + OR_SYMBOL;
        users = findBy(SQL_FIND_USERS_BY_VALUE, USERS_AND_PROFILE, value, value, role, value, value, value, currentPage, rowsPerPage);
        return users;
    }

    /**
     * {@inheritDoc}
     *
     * @see UserDao#findUsersByValueAndTariff(String, long, int, int)
     */
    @Override
    public List<User> findUsersByValueAndTariff(String searchingValue, long tariffId, int currentPage, int rowsPerPage) throws DaoException {
        List<User> users;
        CheckRoleUtil checkRoleUtil = new CheckRoleUtil();
        String role = checkRoleUtil.checkRole(searchingValue);
        String value = OR_SYMBOL + searchingValue + OR_SYMBOL;
        users = findBy(SQL_FIND_USERS_BY_VALUE_AND_TARIFF, USERS_AND_PROFILE, value, value, role, value, value, value, tariffId, currentPage, rowsPerPage);
        return users;
    }

}
