package com.epam.counterparty.util;

import com.epam.counterparty.entity.UserType;

/**
 * The {@code CheckRoleUtil} is a class for one aim to check if searching value matches user role.
 *
 * @author Pavel
 * @see UserType
 * @since 01.07.2019
 */
public class CheckRoleUtil {
    /**
     * Checks if searching value matches {@code UserType}.
     *
     * @param searchingValue the searching value
     * @return the UserType string
     */
    public String checkRole(String searchingValue) {
        if (searchingValue.equalsIgnoreCase(UserType.USER.name())) {
            return UserType.USER.getValue();
        } else if (searchingValue.equalsIgnoreCase(UserType.ADMIN.name())) {
            return UserType.ADMIN.getValue();
        } else {
            return searchingValue;
        }
    }
}
