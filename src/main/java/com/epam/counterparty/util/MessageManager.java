package com.epam.counterparty.util;

import java.util.Locale;
import java.util.ResourceBundle;

import static com.epam.counterparty.util.LangType.RU;

/**
 * The {@code MessageManager} class manages message {@code ResourceBundle} properties.
 *
 * @author Pavel
 * @since 26.05.2019
 */
public class MessageManager {
    private static final String RESOURCE_NAME = "properties/pageContent";
    private static final ResourceBundle rb = ResourceBundle.getBundle(RESOURCE_NAME);

    private MessageManager() {
    }

    /**
     * Gets resource bundle.
     *
     * @param lang the lang
     * @return the resource bundle
     */
    private static ResourceBundle getResourceBundle(Locale lang) {
        return ResourceBundle.getBundle(RESOURCE_NAME, lang);

    }

    /**
     * Gets property.
     *
     * @param key the key
     * @return the property
     */
    public static String getProperty(String key) {
        return rb.getString(key);
    }

    /**
     * Gets the property.
     *
     * @param key          the key
     * @param localeString the locale string
     * @return the property
     */
    public static String getProperty(String key, String localeString) {
        Locale locale;
        ResourceBundle rb;
        String value;
        if (localeString == null) {
            localeString = LangType.EN.getLocale();
        }
        if (LangType.getNameByValue(localeString) == RU) {
            locale = new Locale(RU.getLocale());
            rb = MessageManager.getResourceBundle(locale);
            value = rb.getString(key);
        } else {
            locale = Locale.ROOT;
            rb = MessageManager.getResourceBundle(locale);
            value = rb.getString(key);
        }
        return value;
    }
}
