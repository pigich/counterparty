package com.epam.counterparty.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * The {@code DateTimeConverterUtil} class uses for converting different data/time formats to
 * array of strings which can be used in SQL query.
 *
 * @author Pavel
 * @since 26.07.2019
 */
public class DateTimeConverterUtil {
    /**
     * Converts search string to date string.
     *
     * @param inputString the input string
     * @return the string [ ]
     */
    public static String[] convertSearchStringToDate(String inputString) {
        SimpleDateFormat simpleDateFormat;
        String[] result = new String[]{};
        for (DateTimeFormat dateTimeFormat : DateTimeFormat.values()) {
            try {
                switch (dateTimeFormat) {
                    case HOURS_MINUTES_SECONDS_PATTERN:
                        simpleDateFormat = new SimpleDateFormat(DateTimeFormat.HOURS_MINUTES_SECONDS_PATTERN.getFormat());
                        simpleDateFormat.parse(inputString);
                        result = new String[]{"%H:%i:%s", inputString};
                        break;
                    case YEAR_PATTERN:
                        simpleDateFormat = new SimpleDateFormat(DateTimeFormat.YEAR_PATTERN.getFormat());
                        simpleDateFormat.parse(inputString);
                        result = new String[]{"%Y", inputString};
                        break;
                    case YEAR_MONTH_PATTERN:
                        simpleDateFormat = new SimpleDateFormat(DateTimeFormat.YEAR_MONTH_PATTERN.getFormat());
                        simpleDateFormat.parse(inputString);
                        result = new String[]{"%Y-%m", inputString};
                        break;
                    case YEAR_MONTH_DAY_PATTERN:
                        simpleDateFormat = new SimpleDateFormat(DateTimeFormat.YEAR_MONTH_DAY_PATTERN.getFormat());
                        simpleDateFormat.parse(inputString);
                        result = new String[]{"%Y-%m-%d", inputString};
                        break;
                    case FULL_DATE_PATTERN:
                        simpleDateFormat = new SimpleDateFormat(DateTimeFormat.FULL_DATE_PATTERN.getFormat());
                        simpleDateFormat.parse(inputString);
                        result = new String[]{"%Y-%m-%d %H:%i:%s", inputString};
                        break;
                    default:
                        return new String[]{};
                }
            } catch (ParseException ignored) {
            }
        }
        return result;
    }

    private enum DateTimeFormat {
        /**
         * Year pattern.
         */
        YEAR_PATTERN("yyyy"),
        /**
         * Year month pattern.
         */
        YEAR_MONTH_PATTERN("yyyy-MM"),
        /**
         * Year month day pattern.
         */
        YEAR_MONTH_DAY_PATTERN("yyyy-MM-dd"),
        /**
         * The Full date pattern.
         */
        FULL_DATE_PATTERN("yyyy-MM-dd HH:mm:ss"),
        /**
         * Hours minutes seconds pattern.
         */
        HOURS_MINUTES_SECONDS_PATTERN("HH:mm:ss");

        private String format;

        DateTimeFormat(String format) {
            this.format = format;
        }

        /**
         * Gets format.
         *
         * @return the format
         */
        public String getFormat() {
            return format;
        }
    }

}
