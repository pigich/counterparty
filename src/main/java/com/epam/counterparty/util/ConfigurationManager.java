package com.epam.counterparty.util;

import java.util.ResourceBundle;

/**
 * The {@code ConfigurationManager} class manages page paths properties for commands.
 *
 * @author Pavel
 * @since 27.05.2019
 */
public class ConfigurationManager {
    private static final String RESOURCE_NAME = "config/pagePath";
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle(RESOURCE_NAME);

    private ConfigurationManager() {
    }

    /**
     * Gets property.
     *
     * @param key the key of property
     * @return the property
     */
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
