package com.epam.counterparty.util;

import com.epam.counterparty.exception.ServiceException;

import java.math.BigDecimal;
import java.util.regex.Pattern;

import static com.epam.counterparty.command.Message.*;

/**
 * The {@code DataValidator} class is a validator of input data.
 *
 * @author Pavel
 * @since 15.07.2019
 */
public class DataValidator {
    private static final String FULL_NAME_REGEX = "(^[А-я\\s]{3,30})|(^[A-z\\s]{3,30})$";
    private static final String PASSWORD_REGEX = "([\\w]){3,15}";
    private static final String UNP_REGEX = "([\\d]){9}";
    private static final String PHONE_REGEX = "(^[\\d\\s-+]{10,18})$";
    private static final String ORG_NAME_REGEX = "(^[А-я\\w\\s\"'-]{3,80})$";
    private static final String SEARCH_VALUE_REGEX = "(^[А-я\\w\\s\"+\\:@'-]{1,30})$";
    private static final String EMAIL_REGEX = "^([\\w_.-]{1,45})@([\\w_.-]{1,45})\\.([A-z]{2,6})$";
    private static final String TARIFF_ABILITY_REGEX = "(^[А-я\\w\\s\"'!-]{3,100})$";
    private static final String TARIFF_PERIOD = "([1-9]){1,3}";
    private static final int MINIMUM_AMOUNT = 1;
    private static final int MAXIMUM_AMOUNT = 1_000_000;
    private static final int MINIMUM_TARIFF_AMOUNT = 4;
    private static final int MAXIMUM_TARIFF_AMOUNT = 10_000;

    /**
     * Validates amount.
     *
     * @param amount the amount
     * @throws ServiceException the service exception
     */
    public void validateAmount(BigDecimal amount) throws ServiceException {
        if ((amount.compareTo(BigDecimal.valueOf(MINIMUM_AMOUNT)) <= 0) ||
                (amount.compareTo(BigDecimal.valueOf(MAXIMUM_AMOUNT)) > 0)) {
            throw new ServiceException(MESSAGE_AMOUNT_WRONG_FORMAT);
        }
    }

    /**
     * Validates password.
     *
     * @param password the password
     * @throws ServiceException the service exception
     */
    public void validatePassword(String password) throws ServiceException {
        if (!Pattern.matches(PASSWORD_REGEX, password)) {
            throw new ServiceException(MESSAGE_PASSWORD_WRONG_FORMAT);
        }
    }

    /**
     * Validates name.
     *
     * @param name the name
     * @throws ServiceException the service exception
     */
    public void validateName(String name) throws ServiceException {
        if (!Pattern.matches(FULL_NAME_REGEX, name)) {
            throw new ServiceException(MESSAGE_WRONG_USER_NAME_FORMAT);
        }
    }

    /**
     * Validates unp.
     *
     * @param unp the unp
     * @throws ServiceException the service exception
     */
    public void validateUnp(String unp) throws ServiceException {
        if (!Pattern.matches(UNP_REGEX, unp)) {
            throw new ServiceException(MESSAGE_WRONG_ORG_UNP_FORMAT);
        }
    }

    /**
     * Validates phone.
     *
     * @param phone the phone
     * @throws ServiceException the service exception
     */
    public void validatePhone(String phone) throws ServiceException {
        if (!Pattern.matches(PHONE_REGEX, phone)) {
            throw new ServiceException(MESSAGE_WRONG_PHONE_FORMAT);
        }
    }

    /**
     * Validates org name.
     *
     * @param orgName the org name
     * @throws ServiceException the service exception
     */
    public void validateOrgName(String orgName) throws ServiceException {
        if (!Pattern.matches(ORG_NAME_REGEX, orgName)) {
            throw new ServiceException(MESSAGE_WRONG_ORG_NAME_FORMAT);
        }
    }

    /**
     * Validates search value.
     *
     * @param value the value
     * @throws ServiceException the service exception
     */
    public void validateSearchValue(String value) throws ServiceException {
        if (value != null  && !value.isEmpty()) {
            if (!Pattern.matches(SEARCH_VALUE_REGEX, value)) {
                throw new ServiceException(MESSAGE_WRONG_SEARCH_VALUE_FORMAT);
            }
        }
    }

    /**
     * Validates email.
     *
     * @param emailString the email string
     * @throws ServiceException the service exception
     */
    public void validateEmail(String emailString) throws ServiceException {
        if (!Pattern.matches(EMAIL_REGEX, emailString)) {
            throw new ServiceException(MESSAGE_WRONG_EMAIL_FORMAT);
        }
    }

    /**
     * Validates tariff ability name.
     *
     * @param emailString the email string
     * @throws ServiceException the service exception
     */
    public void validateTariffAbilityName(String emailString) throws ServiceException {
        if (!Pattern.matches(TARIFF_ABILITY_REGEX, emailString)) {
            throw new ServiceException(MESSAGE_WRONG_NAME_FORMAT);
        }
    }

    /**
     * Validates tariff period.
     *
     * @param emailString the email string
     * @throws ServiceException the service exception
     */
    public void validateTariffPeriod(String emailString) throws ServiceException {
        if (!Pattern.matches(TARIFF_PERIOD, emailString)) {
            throw new ServiceException(MESSAGE_WRONG_PERIOD_FORMAT);
        }
    }

    /**
     * Validates tariff price.
     *
     * @param amount the amount
     * @throws ServiceException the service exception
     */
    public void validateTariffPrice(BigDecimal amount) throws ServiceException {
        if ((amount.compareTo(BigDecimal.valueOf(MINIMUM_TARIFF_AMOUNT)) <= 0) ||
                (amount.compareTo(BigDecimal.valueOf(MAXIMUM_TARIFF_AMOUNT)) >= 0)) {
            throw new ServiceException(MESSAGE_PRICE_WRONG_FORMAT);
        }
    }
}

