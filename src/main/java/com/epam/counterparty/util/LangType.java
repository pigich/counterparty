package com.epam.counterparty.util;

/**
 * The enum Lang type.
 *
 * @author Pavel
 * @since 17.07.2019
 */
public enum LangType {
    EN("en_US"),
    RU("ru_RU");

    private String locale;
    LangType(String locale) {
        this.locale = locale;
    }

    /**
     * Gets locale.
     *
     * @return the locale
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Gets name by value.
     *
     * @param value the value
     * @return the name by value
     */
    public static LangType getNameByValue(String value) {
        LangType langType = LangType.EN;
        for (int i = 0; i < LangType.values().length; i++) {
            if (value.equals(LangType.values()[i].locale))
                langType= LangType.values()[i];
        }
        return langType;
    }
}
