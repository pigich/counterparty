package com.epam.counterparty.exception;

/**
 * Thrown to indicate that a problem occurred with a Dao.
 *
 * @author Pavel
 * @since 21.05.2019
 */
public class DaoException extends Exception {
    /**
     * Instantiates a new Dao exception.
     *
     * @param message the message
     */
    public DaoException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Dao exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
