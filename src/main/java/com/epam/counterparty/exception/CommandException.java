package com.epam.counterparty.exception;

/**
 * Thrown to indicate that a problem occurred with a Command.
 *
 * @author Pavel
 * @since 25.05.2019
 */
public class CommandException extends Exception {

    /**
     * Instantiates a new Command exception.
     *
     * @param message the message
     */
    public CommandException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Command exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

}
