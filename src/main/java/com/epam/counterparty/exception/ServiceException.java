package com.epam.counterparty.exception;

/**
 * Thrown to indicate that a problem occurred with a Service (business logic).
 *
 * @author Pavel
 * @since 05.06.2019
 */
public class ServiceException extends Exception {
    /**
     * Instantiates a new Service exception.
     *
     * @param message the message
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Service exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
