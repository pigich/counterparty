package com.epam.counterparty.exception;

import java.sql.SQLException;

/**
 * Thrown to indicate that a problem occurred with a ConnectionPool.
 *
 * @author Pavel
 * @since 19.05.2019
 */
public class ConnectionPoolException extends SQLException {
    /**
     * Instantiates a new Connection pool exception.
     *
     * @param reason the reason
     * @param cause  the cause
     */
    public ConnectionPoolException(String reason, Throwable cause) {
        super(reason, cause);
    }

}
