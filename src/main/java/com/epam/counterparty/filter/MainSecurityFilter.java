package com.epam.counterparty.filter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.epam.counterparty.command.Attribute.*;

/**
 * This filter shares commands rights for allowed roles.
 *
 * @author Pavel
 * @since 31.05.2019
 */
@WebFilter(urlPatterns = {"/*"})
public class MainSecurityFilter extends GenericFilter {
    private static final String INDEX_SLASH = "/";
    private static final Set<String> ALLOWED_GUEST_PATHS = Collections.unmodifiableSet(new HashSet<>(
            Arrays.asList(RECOVER_PASSWORD, TO_ERROR_PAGE, TO_REGISTERED_PAGE, SIGN_UP, TO_SIGN_UP, LOGIN, LANG)));
    /**
     * Allowed commands for USER
     */
    private static final Set<String> ALLOWED_USER_PATHS = new HashSet<>(
            Arrays.asList(CHANGE_TARIFF, FIND_ORG_INFO, EXTEND_TARIFF, CHANGE_PROFILE, CHANGE_PWD, MAKE_FEE,
                    TO_PROFILE, LOGOUT, TO_MAIN_PAGE, TO_USER_PAGE));
    /**
     * Allowed commands for ADMIN
     */
    private static final Set<String> ALLOWED_ADMIN_PATHS = new HashSet<>(
            Arrays.asList(FIND_FEES_AJAX, BLOCK_USER_AJAX, FIND_USERS_AJAX, BLOCK_USER, FIND_ABILITIES, FIND_ABILITY_AJAX,
                    UPDATE_ABILITY, DELETE_ABILITY, ADD_ABILITY, DELETE_TARIFF, UPDATE_TARIFF, ADD_TARIFF,
                    TO_PAYMENTS_INFO, TO_USER_INFO, TO_TARIFF_INFO, LOGOUT, CHANGE_PWD, CHANGE_PROFILE,
                    TO_ADMIN_PROFILE, TO_ADMIN_PAGE, TO_MAIN_PAGE));

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        HttpSession session = httpServletRequest.getSession();
        String role = (String) session.getAttribute(ROLE);
        if (role == null) {
            role = GUEST_ROLE;
        }
        ALLOWED_USER_PATHS.addAll(ALLOWED_GUEST_PATHS);
        ALLOWED_ADMIN_PATHS.addAll(ALLOWED_GUEST_PATHS);
        try {
            if (httpServletRequest.getParameter(COMMAND) != null) {
                switch (role) {
                    case ADMIN:
                        if (!ALLOWED_ADMIN_PATHS.contains(httpServletRequest.getParameter(COMMAND))) {
                            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + INDEX_SLASH);
                            return;
                        }
                        break;
                    case USER: {
                        if (!ALLOWED_USER_PATHS.contains(httpServletRequest.getParameter(COMMAND))) {
                            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + INDEX_SLASH);
                            return;
                        }
                        break;
                    }
                    default:
                        if (!ALLOWED_GUEST_PATHS.contains(httpServletRequest.getParameter(COMMAND))) {
                            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + INDEX_SLASH);
                            return;
                        }
                        break;
                }
            }
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (Exception e) {
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + INDEX_SLASH);
        }
    }
}
