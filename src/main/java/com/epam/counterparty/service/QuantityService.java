package com.epam.counterparty.service;

import com.epam.counterparty.entity.Quantity;
import com.epam.counterparty.exception.ServiceException;

/**
 * The {@code QuantityService} interface represents methods for working with {@code Quantity} entity.
 *
 * @author Pavel
 * @since 28.06.2019
 */
public interface QuantityService {
    /**
     * Finds quantity of users.
     *
     * @param tariffId the tariff id
     * @return the quantity
     * @throws ServiceException the service exception
     */
    Quantity findQuantityOfUsers(String tariffId) throws ServiceException;

    /**
     * Sets page count.
     *
     * @param quantity  the quantity
     * @param rowsCount the rows count
     * @return the page count
     */
    long setPageCount(Quantity quantity, long rowsCount);

    /**
     * Finds quantity of users by searching value.
     *
     * @param searchingValue the searching value
     * @param tariffId       the tariff id
     * @return the quantity
     * @throws ServiceException the service exception
     */
    Quantity findQuantityOfUsersByValue(String searchingValue, String tariffId)throws ServiceException;

    /**
     * Finds quantity of fees by searching value.
     *
     * @param searchingValue the searching value
     * @return the quantity
     * @throws ServiceException the service exception
     */
    Quantity findQuantityOfFeesByValue(String searchingValue) throws ServiceException;

    /**
     * Finds fees quantity.
     *
     * @return the quantity
     * @throws ServiceException the service exception
     */
    Quantity findFeesCount()  throws ServiceException;
}
