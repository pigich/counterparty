package com.epam.counterparty.service;

import com.epam.counterparty.entity.Fee;
import com.epam.counterparty.exception.ServiceException;

import java.util.List;

/**
 * The interface Fee service.
 *
 * @author Pavel
 * @since 26.07.2019
 */
public interface FeeService {

    /**
     * Finds fees list.
     *
     * @param currentPage the current page
     * @param rowsPerPage the rows per page
     * @param orderColumn the order column
     * @param orderType   the order type
     * @return the  list of Fee objects
     * @throws ServiceException the service exception
     */
    List<Fee> findFees(long currentPage, int rowsPerPage, int orderColumn , String orderType) throws ServiceException;

    /**
     * Finds fees by searching value.
     *
     * @param currentPage    the current page
     * @param rowsPerPage    the rows per page
     * @param searchingValue the searching value
     * @param orderColumn    the order column
     * @param orderType      the order type
     * @return the list of Fee objects
     * @throws ServiceException the service exception
     */
    List<Fee> findFeesBySearchingValue(long currentPage, int rowsPerPage, String searchingValue, int orderColumn , String orderType) throws ServiceException;

}
