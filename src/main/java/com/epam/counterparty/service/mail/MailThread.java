package com.epam.counterparty.service.mail;

import com.epam.counterparty.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

import static com.epam.counterparty.command.Message.*;

/**
 * The {@code MailThread} class.
 *
 * @author Pavel
 * @since 29.05.2019
 */
public class MailThread extends Thread {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String REGISTRATION = "REGISTRATION";
    private static final String RECOVER_PASSWORD = "RECOVER_PASSWORD";
    private static final String EMAIL_SUBJECT_CHARSET = "UTF-8";
    private static final String EMAIL_CONTENT_TYPE = "text/html";
    private static final String SPACE_SYMBOL = " ";
    private MimeMessage message;
    private String mailSubject;
    private User user;
    private String type;
    private Properties properties;

    /**
     * Instantiates a new Mail thread.
     *
     * @param mailType    the mail type
     * @param mailSubject the mail subject
     * @param user        the user
     * @param properties  the properties
     */
    MailThread(String mailType, String mailSubject, User user, Properties properties) {
        this.type = mailType;
        this.mailSubject = mailSubject;
        this.user = user;
        this.properties = properties;
    }

    /**
     * Initiates MailThread.
     */
    private void init() {
        Session mailSession = (new SessionCreator(properties)).createSession();
        message = new MimeMessage(mailSession);
        String mailText;
        String sendToEmail = user.getEmail();
        try {
            switch (type) {
                case REGISTRATION:
                    mailText = createRegistrationMessage(user);
                    break;
                case RECOVER_PASSWORD:
                    mailText = createRecoverMessage(user);
                    break;
                default:
                    LOGGER.error(MESSAGE_UNKNOWN_EMAIL_TYPE);
                    throw new MessagingException(MESSAGE_UNKNOWN_EMAIL_TYPE);
            }
            message.setSubject(mailSubject, EMAIL_SUBJECT_CHARSET);
            message.setContent(mailText, EMAIL_CONTENT_TYPE);
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(sendToEmail));
        } catch (AddressException e) {
            LOGGER.error(MESSAGE_INVALID_EMAIL_ADDRESS + sendToEmail + SPACE_SYMBOL, e);
        } catch (MessagingException e) {
            LOGGER.error(MESSAGE_INVALID_EMAIL_BODY + sendToEmail + SPACE_SYMBOL, e);
        }
    }

    @Override
    public void run() {
        init();
        try {
            Transport.send(message);
        } catch (MessagingException e) {
            LOGGER.error(MESSAGE_SEND_EMAIL_EXCEPTION, e);
        }
    }

    /**
     * Creates registration message string.
     *
     * @param user the user
     * @return the string
     */
    private String createRegistrationMessage(User user) {
        return "<html>\n" +
                "<head></head>\n" +
                "<body>\n" +
                "<h2>Thank you for signing up in Counterparty Web service</h2> \n" +
                "<h3>Your login: " + user.getEmail() + "</h3> \n" +
                "<h3>Your password: " + user.getPassword() + "</h3> \n" +
                "<p>You can change password after authorization in your profile </p> \n" +
                "</body></html>";
    }

    /**
     * Creates recover message string.
     *
     * @param user the user
     * @return the string
     */
    private String createRecoverMessage(User user) {
        return "<html>\n" +
                "<head></head>\n" +
                "<body>\n" +
                "<h2>This mail was send to recover access to your account at Counterparty Web service </h2> \n" +
                "<h3>Your login: " + user.getEmail() + "</h3> \n" +
                "<h3>Your password: " + user.getPassword() + "</h3> \n" +
                "</body></html>";
    }

}
