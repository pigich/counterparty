package com.epam.counterparty.service.mail;

import com.epam.counterparty.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.epam.counterparty.command.Message.MESSAGE_EXCEPTION_READ_PROPERTIES_FILE;

/**
 * The type Mail service.
 *
 * @author Pavel
 * @since 29.05.2019
 */
public class MailService {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String MAIL_PROPERTIES = "/config/mail.properties";
    private static final String SUBJECT = "From Counterparty";

    /**
     * Reads properties and sends Email in new thread.
     *
     * @param user     the user
     * @param mailType the mail type
     */
    public void sendMail(User user, String mailType) {
        Properties properties = new Properties();
        try (InputStream in = MailService.class.getResourceAsStream(MAIL_PROPERTIES)) {
            properties.load(in);
            MailThread mailOperator =
                    new MailThread(mailType, SUBJECT, user, properties);
            mailOperator.start();
        } catch (IOException e) {
            LOGGER.fatal(MESSAGE_EXCEPTION_READ_PROPERTIES_FILE, e);
            throw new RuntimeException(MESSAGE_EXCEPTION_READ_PROPERTIES_FILE, e);
        }
    }
}
