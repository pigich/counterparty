package com.epam.counterparty.service.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;

/**
 * The Session creator.
 *
 * @author Pavel
 * @since 29.05.2019
 */
class SessionCreator {
    private static final String MAIL_USER_NAME = "mail.user.name";
    private static final String MAIL_USER_PASSWORD = "mail.user.password";
    private Properties sessionProperties;

    /**
     * Instantiates a new Session creator.
     *
     * @param sessionProperties the session properties
     */
    SessionCreator(Properties sessionProperties) {
        this.sessionProperties = sessionProperties;
    }

    /**
     * Create session.
     *
     * @return the session
     */
    Session createSession() {
        return Session.getDefaultInstance(sessionProperties,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                sessionProperties.getProperty(MAIL_USER_NAME),
                                sessionProperties.getProperty(MAIL_USER_PASSWORD));
                    }
                });
    }
}
