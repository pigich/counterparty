package com.epam.counterparty.service.impl;

import com.epam.counterparty.dao.ProfileDao;
import com.epam.counterparty.dao.impl.ProfileDaoImpl;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.exception.DaoException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.ProfileService;
import com.epam.counterparty.util.DataValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static com.epam.counterparty.command.Message.*;

/**
 * The {@code ProfileServiceImpl} class is the implementation of {@code ProfileService}
 *
 * @author Pavel
 * @see ProfileService
 * @since 07.06.2019
 */
public class ProfileServiceImpl implements ProfileService {
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * {@inheritDoc}
     *
     * @see ProfileService#findUserProfileById(long)
     */
    @Override
    public Profile findUserProfileById(long userId) throws ServiceException {
        ProfileDao profileDaoImpl = new ProfileDaoImpl();
        Profile profile;
        try {
            profile = (Profile) profileDaoImpl.findById(userId);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_EXCEPTION_UPDATE_USER_PROFILE, e);
        }
        return profile;
    }

    /**
     * {@inheritDoc}
     *
     * @see ProfileService#changeUserProfile(String, String, String, String, long)
     */
    @Override
    public Profile changeUserProfile(String name, String phone, String orgUnp, String orgName, long userId) throws ServiceException {
        ProfileDao profileDaoImpl = new ProfileDaoImpl();
        DataValidator dataValidator = new DataValidator();
        int organizationUnp;
        Profile profile;
        try {
            organizationUnp = Integer.parseInt(orgUnp);
        } catch (NumberFormatException e) {
            throw new ServiceException(MESSAGE_WRONG_ORG_UNP_FORMAT);
        }
        dataValidator.validateName(name);
        dataValidator.validatePhone(phone);
        dataValidator.validateUnp(orgUnp);
        dataValidator.validateOrgName(orgName);
        profile = Profile.builder()
                .orgName(orgName)
                .fullName(name)
                .phone(phone)
                .orgUnp(organizationUnp)
                .userId(userId).build();
        try {
            profileDaoImpl.update(profile);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_TRANSACTION_ERROR);
        }
        return profile;
    }

    /**
     * {@inheritDoc}
     *
     * @see ProfileService#findProfiles()
     */
    @Override
    public List<Profile> findProfiles() throws ServiceException {
        List<Profile> profileList;
        ProfileDao profileDaoImpl = new ProfileDaoImpl();
        try {
            profileList = profileDaoImpl.findAllProfiles();
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_EXCEPTION_FIND_PROFILES, e);
        }
        return profileList;
    }
}
