package com.epam.counterparty.service.impl;

import com.epam.counterparty.dao.TariffDao;
import com.epam.counterparty.dao.TransactionHelper;
import com.epam.counterparty.dao.impl.AbilityTariffDaoImpl;
import com.epam.counterparty.dao.impl.PaymentDaoImpl;
import com.epam.counterparty.dao.impl.ProfileDaoImpl;
import com.epam.counterparty.dao.impl.TariffDaoImpl;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.DaoException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.AbilityService;
import com.epam.counterparty.service.TariffService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static com.epam.counterparty.command.Attribute.TARIFF_ID;
import static com.epam.counterparty.command.Message.*;

/**
 * The {@code TariffServiceImpl} class is the implementation of {@code TariffService}
 *
 * @author Pavel
 * @see TariffService
 * @since 11.06.2019
 */
public class TariffServiceImpl implements TariffService {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String TRANSACTION_SUCCESSFUL_PART_ONE = "Transaction successful. User with id= ";
    private static final String TRANSACTION_SUCCESSFUL_PART_TWO = "changed tariff to tariff with id=";
    private final boolean TARIFF_STATE_IS_WORKING = false;

    /**
     * {@inheritDoc}
     *
     * @see TariffService#findTariffByName(Tariff)
     */
    @Override
    public Tariff findTariffByName(Tariff tariff) throws ServiceException {
        Tariff newTariff;
        String nameEn = tariff.getTariffNameEn();
        String nameRu = tariff.getTariffNameRu();
        TariffDao tariffDao = new TariffDaoImpl();
        try {
            newTariff = tariffDao.findByName(nameEn, nameRu);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_EXCEPTION_IN_FIND_TARIFF_BY_NAME_METHOD, e);
        }
        return newTariff;
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffService#findWorkingTariffs()
     */
    @Override
    public List<Tariff> findWorkingTariffs() throws ServiceException {
        List<Tariff> tariffs;
        TariffDao tariffDao = new TariffDaoImpl();
        try {
            tariffs = tariffDao.findWorkingTariffs();
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_EXCEPTION_IN_FIND_WORKING_TARIFFS_METHOD, e);
        }
        return tariffs;
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffService#findTariffs()
     */
    @Override
    public List<Tariff> findTariffs() throws ServiceException {
        List<Tariff> tariffs;
        TariffDao tariffDao = new TariffDaoImpl();
        try {
            tariffs = tariffDao.findAllTariffs();
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_EXCEPTION_IN_FIND_TARIFFS_METHOD, e);
        }
        return tariffs;
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffService#changeTariff(Tariff, long)
     */
    @Override
    public void changeTariff(Tariff tariff, long userId) throws ServiceException {
        long tariffId = tariff.getId();
        Profile profile = Profile.builder()
                .tariffId(tariffId)
                .userId(userId)
                .build();
        BigDecimal amount = tariff.getPrice();
        String period = String.valueOf(tariff.getPeriod());
        TransactionHelper transaction = new TransactionHelper();
        ProfileDaoImpl profileDaoImpl = new ProfileDaoImpl();
        PaymentDaoImpl paymentDaoImpl = new PaymentDaoImpl();
        try {
            transaction.beginTransaction(profileDaoImpl, paymentDaoImpl);
            profileDaoImpl.updateTariff(profile);
            paymentDaoImpl.updatePayment(period, amount, tariffId, userId);
            transaction.commit();
            LOGGER.info(TRANSACTION_SUCCESSFUL_PART_ONE + profile.getUserId()
                    + TRANSACTION_SUCCESSFUL_PART_TWO + profile.getTariffId());
        } catch (DaoException e) {
            try {
                transaction.rollback();
            } catch (DaoException ex) {
                LOGGER.error(e.getMessage());
                throw new ServiceException(MESSAGE_EXCEPTION_CHANGE_TARIFF, e);
            }
        } finally {
            try {
                transaction.endTransaction();
            } catch (DaoException e) {
                throw new ServiceException(e.getMessage());
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffService#extendTariff(Tariff, long)
     */
    @Override
    public void extendTariff(Tariff tariff, long userId) throws ServiceException {
        long tariffId = tariff.getId();
        BigDecimal amount = tariff.getPrice();
        String period = String.valueOf(tariff.getPeriod());
        TransactionHelper transaction = new TransactionHelper();
        PaymentDaoImpl paymentDaoImpl = new PaymentDaoImpl();
        try {
            transaction.beginTransaction(paymentDaoImpl);
            paymentDaoImpl.updatePayment(period, amount, tariffId, userId);
            transaction.commit();
        } catch (DaoException e) {
            try {
                transaction.rollback();
            } catch (DaoException ex) {
                LOGGER.error(e.getMessage());
                throw new ServiceException(MESSAGE_EXCEPTION_EXTEND_TARIFF, e);
            }
        } finally {
            try {
                transaction.endTransaction();
            } catch (DaoException e) {
                throw new ServiceException(e.getMessage());
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffService#setTariffArchival(long)
     */
    @Override
    public void setTariffArchival(long tariffId) throws ServiceException {
        TariffDao tariffDao = new TariffDaoImpl();
        try {
            tariffDao.delete(tariffId);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_EXCEPTION_DELETE_TARIFF_FROM_DATABASE, e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffService#findTariffById(long)
     */
    @Override
    public Tariff findTariffById(long tariffId) throws ServiceException {
        Tariff tariff;
        TariffDao tariffDao = new TariffDaoImpl();
        try {
            tariff = tariffDao.findById(tariffId);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_EXCEPTION_IN_FIND_TARIFF_BY_ID_METHOD, e);
        }
        return tariff;
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffService#addTariff(Tariff)
     */
    @Override
    public Tariff addTariff(Tariff tariff) throws ServiceException {
        Tariff newTariff;
        newTariff = findTariffByName(tariff);
        if (newTariff == null) {
            newTariff = createTariff(tariff);
        }
        return newTariff;
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffService#updateTariff(Tariff)
     */
    @Override
    public void updateTariff(Tariff tariff) throws ServiceException {
        TransactionHelper transaction = new TransactionHelper();
        TariffDaoImpl tariffDaoImpl = new TariffDaoImpl();
        AbilityTariffDaoImpl abilityTariffDaoImpl = new AbilityTariffDaoImpl();
        try {
            transaction.beginTransaction(tariffDaoImpl, abilityTariffDaoImpl);
            long tariffId = tariff.getId();
            tariffDaoImpl.update(tariff);
            abilityTariffDaoImpl.delete(tariffId, TARIFF_ID);
            if (!tariff.getAbilityList().isEmpty()) {
                tariffDaoImpl.addAbilityTariff(tariff);
            }
            transaction.commit();
        } catch (DaoException e) {
            try {
                transaction.rollback();
            } catch (DaoException ex) {
                throw new ServiceException(e.getMessage());
            }
            throw new ServiceException(e.getMessage());
        } finally {
            try {
                transaction.endTransaction();
            } catch (DaoException e) {
                throw new ServiceException(e.getMessage());
            }
        }
    }

    /**
     * Creates tariff.
     *
     * @param tariff the Tariff object
     * @return the Tariff object
     * @throws ServiceException the service exception
     */
    private Tariff createTariff(Tariff tariff) throws ServiceException {
        Tariff newTariff;
        TransactionHelper transaction = new TransactionHelper();
        TariffDaoImpl tariffDaoImpl = new TariffDaoImpl();
        try {
            transaction.beginTransaction(tariffDaoImpl);
            tariffDaoImpl.create(tariff);
            String nameEn = String.valueOf(tariff.getTariffNameEn());
            String nameRu = String.valueOf(tariff.getTariffNameRu());
            newTariff = tariffDaoImpl.findByName(nameEn, nameRu);
            tariff.setId(newTariff.getId());
            tariffDaoImpl.addAbilityTariff(tariff);
            transaction.commit();
        } catch (DaoException e) {
            try {
                transaction.rollback();
            } catch (DaoException ex) {
                throw new ServiceException(ex.getMessage());
            }
            throw new ServiceException(e.getMessage());
        } finally {
            try {
                transaction.endTransaction();
            } catch (DaoException e) {
                throw new ServiceException(e.getMessage());
            }
        }
        return newTariff;
    }

    /**
     * {@inheritDoc}
     *
     * @see TariffService#createWorkingTariffsList()
     */
    public List<Tariff> createWorkingTariffsList() {
        AbilityService abilityService = new AbilityServiceImpl();
        List<Tariff> tariffs;
        List<Ability> abilities;
        try {
            tariffs = findWorkingTariffs();
            abilities = abilityService.findAbilitiesByTariffState(TARIFF_STATE_IS_WORKING);
            for (Tariff tariff : tariffs) {
                List<Ability> abilityList = new LinkedList<>();
                for (Ability ability : abilities) {
                    if (tariff.getId() == ability.getTariffAbilityId()) {
                        abilityList.add(ability);
                    }
                }
                tariff.setAbilityList(abilityList);
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            tariffs = null;
        }
        return tariffs;
    }
}
