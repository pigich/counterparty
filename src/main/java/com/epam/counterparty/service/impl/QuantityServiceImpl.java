package com.epam.counterparty.service.impl;

import com.epam.counterparty.dao.QuantityDao;
import com.epam.counterparty.dao.impl.QuantityDaoImpl;
import com.epam.counterparty.entity.Quantity;
import com.epam.counterparty.exception.DaoException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.QuantityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The {@code QuantityServiceImpl} class is the implementation of {@code QuantityService}
 *
 * @author Pavel
 * @see QuantityService
 * @since 28.06.2019
 */
public class QuantityServiceImpl implements QuantityService {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final int DEFAULT_ROWS_PER_PAGE = 2;
    private static final String ALL_TARIFFS_EN = "All";
    private static final String ALL_TARIFFS_RU = "Все";

    /**
     * {@inheritDoc}
     *
     * @see QuantityService#findQuantityOfUsers(String)
     */
    @Override
    public Quantity findQuantityOfUsers(String tariffId) throws ServiceException {
        QuantityDao quantityDao = new QuantityDaoImpl();
        Quantity quantity;
        try {
            if (tariffId.equals(ALL_TARIFFS_EN) || tariffId.equals(ALL_TARIFFS_RU)) {
                quantity = quantityDao.findAllQuantityOfUsers();
            } else {
                long tariff = Long.parseLong(tariffId);
                quantity = quantityDao.findQuantityOfUsersByTariffId(tariff);
            }
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return quantity;
    }

    /**
     * {@inheritDoc}
     *
     * @see QuantityService#setPageCount(Quantity, long)
     */
    @Override
    public long setPageCount(Quantity quantity, long rowsCount) {
        if (rowsCount != 0) {
            return ((quantity.getRowsCount() - 1) / rowsCount + 1);
        } else {
            return ((quantity.getRowsCount() - 1) / DEFAULT_ROWS_PER_PAGE + 1);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see QuantityService#findQuantityOfUsersByValue(String, String)
     */
    @Override
    public Quantity findQuantityOfUsersByValue(String searchingValue, String tariffId) throws ServiceException {
        QuantityDao quantityDao = new QuantityDaoImpl();
        Quantity quantity;
        try {
            if (tariffId.equals(ALL_TARIFFS_EN) || tariffId.equals(ALL_TARIFFS_RU)) {
                quantity = quantityDao.findQuantityByValue(searchingValue);
            } else {
                long tariff = Long.parseLong(tariffId);
                quantity = quantityDao.findQuantityByValueAndTariff(searchingValue, tariff);
            }
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return quantity;
    }

    /**
     * {@inheritDoc}
     *
     * @see QuantityService#findQuantityOfFeesByValue(String)
     */
    @Override
    public Quantity findQuantityOfFeesByValue(String searchingValue) throws ServiceException {
        QuantityDao quantityDao = new QuantityDaoImpl();
        Quantity quantity;
        try {
            quantity = quantityDao.findQuantityOfFeeByValue(searchingValue);

        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return quantity;
    }

    /**
     * {@inheritDoc}
     *
     * @see QuantityService#findFeesCount()
     */
    @Override
    public Quantity findFeesCount() throws ServiceException {
        QuantityDao quantityDao = new QuantityDaoImpl();
        Quantity quantity;
        try {
            quantity = quantityDao.findQuantityOfFees();
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return quantity;
    }
}