package com.epam.counterparty.service.impl;

import com.epam.counterparty.dao.FeesDao;
import com.epam.counterparty.dao.PaymentDao;
import com.epam.counterparty.dao.TransactionHelper;
import com.epam.counterparty.dao.impl.FeesDaoImpl;
import com.epam.counterparty.dao.impl.PaymentDaoImpl;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.exception.DaoException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.PaymentService;
import com.epam.counterparty.util.DataValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.List;

import static com.epam.counterparty.command.Message.*;

/**
 * The {@code PaymentServiceImpl} class is the implementation of {@code PaymentService}
 *
 * @author Pavel
 * @see PaymentService
 * @since 04.06.2019
 */
public class PaymentServiceImpl implements PaymentService {
    private static final Logger LOGGER = LogManager.getLogger();
    /**
     * {@inheritDoc}
     *
     * @see PaymentService#doPayment(long, String)
     */
    @Override
    public void doPayment(long userId, String stringAmount) throws ServiceException {
        BigDecimal amount;
        DataValidator dataValidator = new DataValidator();
        try {
            double amountFromString = Double.parseDouble(stringAmount);
            amount = BigDecimal.valueOf(amountFromString);
        } catch (NumberFormatException e) {
            throw new ServiceException(MESSAGE_AMOUNT_WRONG_FORMAT);
        }
        dataValidator.validateAmount(amount);
        doPaymentTransaction(userId, amount);
    }

    /**
     * {@inheritDoc}
     *
     * @see PaymentService#doPaymentTransaction(long, BigDecimal)
     */
    @Override
    public void doPaymentTransaction(long userId, BigDecimal amount) throws ServiceException {
        TransactionHelper transaction = new TransactionHelper();
        PaymentDaoImpl paymentDao = new PaymentDaoImpl();
        FeesDaoImpl feesDao = new FeesDaoImpl();
        try {
            transaction.beginTransaction(feesDao, paymentDao);
            makeFee(feesDao, userId, amount);
            updatePayment(paymentDao, userId, amount);
            transaction.commit();
            LOGGER.info("transaction from " + userId + "with amount " + amount + " was successful");
        } catch (DaoException e) {
            try {
                transaction.rollback();
            } catch (DaoException ex) {
                throw new ServiceException(MESSAGE_TRANSACTION_ERROR);
            }
        } finally {
            try {
                transaction.endTransaction();
            } catch (DaoException e) {
                throw new ServiceException(e.getMessage());
            }
        }
    }

    /**
     * Updates payment.
     *
     * @param paymentDao the payment dao
     * @param userId     the user id
     * @param amount     the amount
     * @throws ServiceException the service exception
     */
    private void updatePayment(PaymentDao paymentDao, long userId, BigDecimal amount) throws ServiceException {
        try {
            paymentDao.updatePayment(userId, amount);
        } catch (DaoException e) {
            LOGGER.error(MESSAGE_EXCEPTION_IN_UPDATE_METHOD, e);
            throw new ServiceException(MESSAGE_EXCEPTION_IN_UPDATE_METHOD, e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see PaymentService#findPayment(long)
     */
    @Override
    public Payment findPayment(long userId) throws ServiceException {
        Payment payment;
        PaymentDao paymentDao = new PaymentDaoImpl();
        try {
            payment = paymentDao.findById(userId);
        } catch (DaoException e) {
            LOGGER.error(MESSAGE_EXCEPTION_IN_FIND_PAYMENT_METHOD, e);
            throw new ServiceException(MESSAGE_EXCEPTION_IN_FIND_PAYMENT_METHOD, e);
        }
        return payment;
    }

    /**
     * Makes fee.
     *
     * @param feesDao the fees dao
     * @param userId  the user id
     * @param amount  the amount
     * @throws ServiceException the service exception
     */
    private void makeFee(FeesDao feesDao, long userId, BigDecimal amount) throws ServiceException {
        try {
            feesDao.createFee(userId, amount);
        } catch (DaoException e) {
            LOGGER.error(MESSAGE_EXCEPTION_IN_MAKE_FEE_METHOD, e);
            throw new ServiceException(MESSAGE_EXCEPTION_IN_MAKE_FEE_METHOD, e);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see PaymentService#findPayments()
     */
    @Override
    public List<Payment> findPayments() throws ServiceException {
        List<Payment> payments;
        PaymentDao paymentDao = new PaymentDaoImpl();
        try {
            payments = paymentDao.findAllPayments();
        } catch (DaoException e) {
            LOGGER.error(MESSAGE_EXCEPTION_IN_FIND_PAYMENTS_METHOD, e);
            throw new ServiceException(MESSAGE_EXCEPTION_IN_FIND_PAYMENTS_METHOD, e);
        }
        return payments;
    }
}

