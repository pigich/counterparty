package com.epam.counterparty.service.impl;

import com.epam.counterparty.dao.TransactionHelper;
import com.epam.counterparty.dao.UserDao;
import com.epam.counterparty.dao.impl.PaymentDaoImpl;
import com.epam.counterparty.dao.impl.ProfileDaoImpl;
import com.epam.counterparty.dao.impl.UserDaoImpl;
import com.epam.counterparty.entity.Egr;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.entity.User;
import com.epam.counterparty.exception.DaoException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.PasswordService;
import com.epam.counterparty.service.RestClient;
import com.epam.counterparty.service.UserService;
import com.epam.counterparty.service.mail.MailService;
import com.epam.counterparty.util.DataValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.List;

import static com.epam.counterparty.command.Attribute.ALL_TARIFFS;
import static com.epam.counterparty.command.Attribute.ALL_TARIFFS_RU;
import static com.epam.counterparty.command.Message.*;

/**
 * The {@code UserServiceImpl} class is the implementation of {@code UserService}
 *
 * @author Pavel
 * @see UserService
 * @since 27.05.2019
 */
public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final BigDecimal NEW_USER_BALANCE = new BigDecimal(0);
    private static final String USER_ROLE_ID = "1";
    private static final String REGISTRATION = "REGISTRATION";
    private static final String RECOVER_PASSWORD = "RECOVER_PASSWORD";

    /**
     * {@inheritDoc}
     *
     * @see UserService#checkEmail(String, String)
     */
    @Override
    public User checkEmail(String email, String passwordFromRequest) throws ServiceException {
        UserDao userDao = new UserDaoImpl();
        User user;
        try {
            user = userDao.findEmail(email);
            if (user == null) {
                throw new ServiceException(MESSAGE_LOGIN_IS_INCORRECT_EXCEPTION);
            }
            checkPassword(user, passwordFromRequest);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return user;
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#changePassword(String, String, String, long)
     */
    private void checkPassword(User user, String passwordFromRequest) throws ServiceException {
        String checkedPassword = PasswordService.cryptPassword(passwordFromRequest);
        if (!user.getPassword().equals(checkedPassword)) {
            throw new ServiceException(MESSAGE_PASSWORD_IS_INCORRECT_EXCEPTION);
        }
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#signUp(String)
     */
    @Override
    public User signUp(String email) throws ServiceException {
        DataValidator dataValidator = new DataValidator();
        User user;
        dataValidator.validateEmail(email);
        user = findUserByEmail(email);
        if (user == null) {
            user = User.builder().build();
            user.setEmail(email);
            String password = PasswordService.generatePassword();
            String cryptedPassword = PasswordService.cryptPassword(password);
            user.setPassword(cryptedPassword);
            user.setRole(Byte.parseByte(USER_ROLE_ID));
            User createdUser = createUserAccount(user);
            if (createdUser != null) {
                createdUser.setPassword(password);
                MailService mail = new MailService();
                mail.sendMail(createdUser, REGISTRATION);
            }
        } else {
            user = null;
        }
        return user;
    }

    /**
     * {@inheritDoc}
     *
     * @see UserService#recoverUserPassword(String)
     */
    @Override
    public User recoverUserPassword(String email) throws ServiceException {
        UserDao userDao = new UserDaoImpl();
        User user;
        try {
            user = userDao.findEmail(email);
            if (user != null) {
                String password = PasswordService.generatePassword();
                String cryptedPassword = PasswordService.cryptPassword(password);
                user.setPassword(cryptedPassword);
                updateUser(user);
                user.setPassword(password);
                MailService mail = new MailService();
                mail.sendMail(user, RECOVER_PASSWORD);
            } else {
            }
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
        return user;
    }

    /**
     * Creates user account.
     *
     * @param user the User object
     * @return the User object
     * @throws ServiceException the service exception
     */
   private User createUserAccount(User user) throws ServiceException {
        TransactionHelper transaction = new TransactionHelper();
        PaymentDaoImpl paymentDaoImpl = new PaymentDaoImpl();
        UserDaoImpl userDaoImpl = new UserDaoImpl();
        ProfileDaoImpl profileDaoImpl = new ProfileDaoImpl();
        User newUser = null;
        try {
            Profile profile = Profile.builder().build();
            Payment payment = new Payment();
            transaction.beginTransaction(userDaoImpl, paymentDaoImpl, profileDaoImpl);
            userDaoImpl.create(user);
            newUser = userDaoImpl.findEmail(user.getEmail());
            payment.setUserId(newUser.getId());
            payment.setCurrentBalance(NEW_USER_BALANCE);
            paymentDaoImpl.createPayment(payment);
            profile.setUserId(newUser.getId());
            profileDaoImpl.create(profile);
            transaction.commit();
        } catch (DaoException e) {
            try {
                LOGGER.error(e.getMessage());
                transaction.rollback();
            } catch (DaoException ex) {
                throw new ServiceException(e.getMessage());
            }
        } finally {
            try {
                transaction.endTransaction();
            } catch (DaoException e) {
                throw new ServiceException(e.getMessage());
            }
        }
        return newUser;
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#findUserById(long)
     */
    @Override
    public User findUserById(long userId) throws ServiceException {
        UserDao userDao = new UserDaoImpl();
        User user;
        try {
            user = (User) userDao.findById(userId);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return user;
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#findUserByEmail(String)
     */
    @Override
    public User findUserByEmail(String email) throws ServiceException {
        UserDao userDao = new UserDaoImpl();
        User user;
        try {
            user = userDao.findEmail(email);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return user;
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#updateUser(User)
     */
    @Override
    public void updateUser(User user) throws ServiceException {
        UserDao userDao = new UserDaoImpl();
        try {
            userDao.update(user);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#changePassword(String, String, String, long)
     */
    public void changePassword(String oldPassword, String newPassword, String repeatNewPassword, long userId) throws ServiceException {
        DataValidator dataValidator = new DataValidator();
        dataValidator.validatePassword(newPassword);
        if (newPassword.equals(repeatNewPassword)) {
            User user;
            user = findUserById(userId);
            if (PasswordService.cryptPassword(oldPassword).equals(user.getPassword())) {
                String cryptedNewPassword = PasswordService.cryptPassword(newPassword);
                user.setPassword(cryptedNewPassword);
                updateUser(user);
            } else {
                throw new ServiceException(MESSAGE_WRONG_OLD_PASSWORD);
            }
        } else {
            throw new ServiceException(MESSAGE_PASSWORDS_NOT_EQUALS);
        }
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#findOrgInfo(String)
     */
    @Override
    public Egr findOrgInfo(String organizationUnp) throws ServiceException {
        Egr egr;
        RestClient restClient = new RestClient();
        try {
            String jsonEgr = restClient.getDataFromEgrSite(organizationUnp);
            egr = restClient.convertJsonToEntity(jsonEgr);
        } catch (Exception e) {
            throw new ServiceException(e.getMessage());
        }
        return egr;
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#findAllUsers(int, int)
     */
    @Override
    public List<User> findAllUsers(int currentPage, int rowsPerPage) throws ServiceException {
        List<User> users;
        UserDao userDao = new UserDaoImpl();
        int page = currentPage;
        if (currentPage == 1) {
            page = 0;
        }
        try {
            users = userDao.findUsers(page, rowsPerPage);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return users;
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#blockUser(long, boolean)
     */
    public void blockUser(long userId, boolean userStatus) throws ServiceException {
        UserDao userDao = new UserDaoImpl();
        try {
            if (userStatus) {
                userDao.blockUser(userId, false);
            } else {
                userDao.blockUser(userId, true);
            }
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#findUsersByTariffId(String, int, int)
     */
    public List<User> findUsersByTariffId(String tariffId, int currentPage, int rowsPerPage) throws ServiceException {
        UserDao userDao = new UserDaoImpl();
        List<User> users;
        int page;
        if (currentPage == 1) {
            page = 0;
        } else {
            page = (currentPage - 1) * rowsPerPage;
        }
        try {
            if (tariffId.equalsIgnoreCase(ALL_TARIFFS) || tariffId.equalsIgnoreCase(ALL_TARIFFS_RU)) {
                users = userDao.findUsers(page, rowsPerPage);
            } else {
                long tempTariffId = Long.parseLong(tariffId);
                users = userDao.findByTariffId(tempTariffId, page, rowsPerPage);
            }
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return users;
    }
    /**
     * {@inheritDoc}
     *
     * @see UserService#findUsersBySearchingValue(String, int, int, String)
     */
    public List<User> findUsersBySearchingValue(String tariffId, int currentPage, int rowsPerPage, String
            searchingValue) throws ServiceException {
        UserDao userDao = new UserDaoImpl();
        List<User> users;
        int page;
        if (currentPage == 1) {
            page = 0;
        } else {
            page = (currentPage - 1) * rowsPerPage;
        }
        try {
            if (tariffId.equalsIgnoreCase(ALL_TARIFFS) || tariffId.equalsIgnoreCase(ALL_TARIFFS_RU)) {
                users = userDao.findUsersByValue(searchingValue, page, rowsPerPage);
            } else {
                long tempTariffId = Long.parseLong(tariffId);
                users = userDao.findUsersByValueAndTariff(searchingValue, tempTariffId, page, rowsPerPage);
            }
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
        return users;
    }
}
