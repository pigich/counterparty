package com.epam.counterparty.service.impl;

import com.epam.counterparty.dao.AbilityDao;
import com.epam.counterparty.dao.impl.AbilityDaoImpl;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.exception.DaoException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.AbilityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * The {@code AbilityServiceImpl} class is the implementation of {@code AbilityService}
 *
 * @author Pavel
 * @see AbilityService
 * @since 17.06.2019
 */
public class AbilityServiceImpl implements AbilityService {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String MESSAGE_FIND_ABILITY_BY_NAME = "Error in findAbilityByName method";
    private static final String MESSAGE_DELETE_ABILITY_BY_ID = "Delete Ability from database error ";
    private static final String MESSAGE_FIND_ABILITY_BY_TARIFF_ID = "Error in findAbilityByTariffId method";
    private static final String MESSAGE_FIND_ABILITY_BY_TARIFF_STATE = "Error in findAbilitiesByTariffState method";
    private static final String MESSAGE_FIND_ABILITIES = "Error in findTariffs method";
    private static final String MESSAGE_UPDATE_ABILITY = "Error in updateAbility method";
    private static final String MESSAGE_CREATE_ABILITY = "Error in createAbility method";

    /**
     * {@inheritDoc}
     *
     * @see AbilityService#findAbilities()
     */
    @Override
    public List<Ability> findAbilities() throws ServiceException {
        List<Ability> abilities;
        AbilityDao abilityDaoImpl = new AbilityDaoImpl();
        try {
            abilities = abilityDaoImpl.findAllAbilities();
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_FIND_ABILITIES, e);
        }
        return abilities;
    }
    /**
     * {@inheritDoc}
     *
     * @see AbilityService#updateAbility(Ability)
     */
    @Override
    public void updateAbility(Ability ability) throws ServiceException {
        AbilityDao abilityDaoImpl = new AbilityDaoImpl();
        try {
            abilityDaoImpl.update(ability);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_UPDATE_ABILITY,e);
        }
    }
    /**
     * {@inheritDoc}
     *
     * @see AbilityService#addAbility(Ability)
     */
    @Override
    public Ability addAbility(Ability ability) throws ServiceException {
        Ability newAbility;
        newAbility = findAbilityByName(ability);
        if (newAbility == null) {
            newAbility = createAbility(ability);
        }
        return newAbility;
    }
    /**
     * {@inheritDoc}
     *
     * @see AbilityService#findAbilityByName(Ability)
     */
    @Override
    public Ability findAbilityByName(Ability ability) throws ServiceException {
        Ability newAbility;
        try {
            AbilityDao abilityDaoImpl = new AbilityDaoImpl();
            String nameEn = ability.getAbilityNameEn();
            String nameRu = ability.getAbilityNameRu();
            newAbility = abilityDaoImpl.findByName(nameEn, nameRu);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_FIND_ABILITY_BY_NAME, e);
        }
        return newAbility;
    }

    /**
     * Creates ability.
     *
     * @param ability the ability
     * @return the ability
     * @throws ServiceException the service exception
     */
    private Ability createAbility(Ability ability) throws ServiceException {
        Ability newAbility;
        try {
            AbilityDao abilityDaoImpl = new AbilityDaoImpl();
            abilityDaoImpl.create(ability);
            String nameEn = String.valueOf(ability.getAbilityNameEn());
            String nameRu = String.valueOf(ability.getAbilityNameRu());
            newAbility = abilityDaoImpl.findByName(nameEn, nameRu);
        } catch (DaoException e) {
            throw new ServiceException(MESSAGE_CREATE_ABILITY,e);
        }
        return newAbility;
    }
    /**
     * {@inheritDoc}
     *
     * @see AbilityService#deleteAbilityById(long)
     */
    @Override
    public void deleteAbilityById(long abilityId) throws ServiceException {
        AbilityDao abilityDaoImpl = new AbilityDaoImpl();
        try {
            abilityDaoImpl.delete(abilityId);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_DELETE_ABILITY_BY_ID, e);
        }
    }
    /**
     * {@inheritDoc}
     *
     * @see AbilityService#findAbilityByTariffId(long)
     */
    @Override
    public List<Ability> findAbilityByTariffId(long tariffId) throws ServiceException {
        List<Ability> abilities;
        AbilityDao abilityDaoImpl = new AbilityDaoImpl();
        try {
            abilities = abilityDaoImpl.findByTariffId(tariffId);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_FIND_ABILITY_BY_TARIFF_ID, e);
        }
        return abilities;
    }
    /**
     * {@inheritDoc}
     *
     * @see AbilityService#findAbilitiesByTariffState(boolean)
     */
    @Override
    public List<Ability> findAbilitiesByTariffState(boolean tariffState) throws ServiceException {
        List<Ability> abilities;
        AbilityDao abilityDaoImpl = new AbilityDaoImpl();
        try {
            abilities = abilityDaoImpl.findByTariffState(tariffState);
        } catch (DaoException e) {
            LOGGER.error(e.getMessage());
            throw new ServiceException(MESSAGE_FIND_ABILITY_BY_TARIFF_STATE, e);
        }
        return abilities;
    }
}
