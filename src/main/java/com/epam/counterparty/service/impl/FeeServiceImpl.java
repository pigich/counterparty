package com.epam.counterparty.service.impl;

import com.epam.counterparty.dao.FeesDao;
import com.epam.counterparty.dao.impl.FeesDaoImpl;
import com.epam.counterparty.entity.Fee;
import com.epam.counterparty.exception.DaoException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.FeeService;

import java.util.List;

import static com.epam.counterparty.command.Message.MESSAGE_EXCEPTION_IN_FIND_FEES_METHOD;

/**
 * The {@code FeeServiceImpl} class is the implementation of {@code FeeService}
 *
 * @author Pavel
 * @see FeeService
 * @since 26.07.2019
 */
public class FeeServiceImpl implements FeeService {
    /**
     * {@inheritDoc}
     *
     * @see FeeService#findFees(long, int, int, String)
     */
    @Override
    public List<Fee> findFees(long startPosition, int rowsPerPage, int orderColumn, String orderType) throws ServiceException {
        List<Fee> fees;
        FeesDao feesDao = new FeesDaoImpl();
        try {
            fees = feesDao.findAllFees(startPosition, rowsPerPage, orderColumn, orderType);
        } catch (DaoException e) {
            throw new ServiceException(MESSAGE_EXCEPTION_IN_FIND_FEES_METHOD, e);
        }
        return fees;
    }

    /**
     * {@inheritDoc}
     *
     * @see FeeService#findFeesBySearchingValue(long, int, String, int, String)
     */
    @Override
    public List<Fee> findFeesBySearchingValue(long startPosition, int rowsPerPage, String searchingValue, int orderColumn, String orderType) throws ServiceException {
        FeesDao feesDao = new FeesDaoImpl();
        List<Fee> fees;
        try {
            fees = feesDao.findFeesBySearchingValue(searchingValue, startPosition, rowsPerPage, orderColumn, orderType);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage());
        }
        return fees;
    }
}
