package com.epam.counterparty.service;

import com.epam.counterparty.entity.Egr;
import com.epam.counterparty.exception.ServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.jetbrains.annotations.Nullable;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

import static com.epam.counterparty.command.Message.MESSAGE_DATA_NOT_FOUND;
import static com.epam.counterparty.command.Message.MESSAGE_EXCEPTION_READ_JSON_FORMAT;

/**
 * The {@code RestClient} class represents logic for working with EGR web services
 * more info about EGR web API on {@code http://egr.gov.by/egrn/index.jsp?content=API}.
 *
 * @author Pavel
 * @since 13.06.2019
 */
public class RestClient {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String BASE_URL = "http://egr.gov.by/egrn/API.jsp";
    private static final String PARAM_NAME = "NM";

    /**
     * Converts {@code json} to {@code Egr} entity.
     *
     * @param jsonEgr the json egr
     * @return the egr
     * @see Egr
     */
    public Egr convertJsonToEntity(String jsonEgr) throws ServiceException {
        List<Egr> egrList;
        Egr egr;
        ObjectMapper mapper = new ObjectMapper();
        if (!jsonEgr.isEmpty()) {
            try {
                egrList = mapper.readValue(jsonEgr, TypeFactory.defaultInstance()
                        .constructCollectionType(List.class, Egr.class));
                egr = egrList.get(0);
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
                throw new ServiceException(MESSAGE_EXCEPTION_READ_JSON_FORMAT);
            }
        } else {
            throw new ServiceException(MESSAGE_DATA_NOT_FOUND);
        }
        return egr;
    }

    /**
     * Gets data from egr web service.
     *
     * @param parameterValue the parameter value
     * @return the data from egr site
     */
    @Nullable
    public String getDataFromEgrSite(String parameterValue) {
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        Response response = client
                .target(BASE_URL)
                .queryParam(PARAM_NAME, parameterValue)
                .request().accept(MediaType.APPLICATION_JSON).get();
        String jsonEgr = null;
        int status = response.getStatus();
        if (status == 200) {
            jsonEgr = response.readEntity(String.class);
        }
        return jsonEgr;
    }
}