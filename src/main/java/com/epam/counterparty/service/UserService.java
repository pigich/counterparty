package com.epam.counterparty.service;

import com.epam.counterparty.entity.Egr;
import com.epam.counterparty.entity.User;
import com.epam.counterparty.exception.ServiceException;

import java.util.List;

/**
 * The {@code UserService} interface represents methods for working with {@code User} entity.
 *
 * @author Pavel
 * @since 26.06.2019
 */
public interface UserService {
    /**
     * Checks email user.
     *
     * @param email               the email
     * @param passwordFromRequest the password from request
     * @return the user
     * @throws ServiceException the service exception
     */
    User checkEmail(String email, String passwordFromRequest) throws ServiceException;

    /**
     * Sign ups user.
     *
     * @param email the email
     * @return the user
     * @throws ServiceException the service exception
     */
    User signUp(String email) throws ServiceException;

    /**
     * Recovers user password user.
     *
     * @param email the email
     * @return the user
     * @throws ServiceException the service exception
     */
    User recoverUserPassword(String email) throws ServiceException;

    /**
     * Finds user by id user.
     *
     * @param userId the user id
     * @return the user
     * @throws ServiceException the service exception
     */
    User findUserById(long userId) throws ServiceException;

    /**
     * Finds user by email user.
     *
     * @param email the email
     * @return the user
     * @throws ServiceException the service exception
     */
    User findUserByEmail(String email) throws ServiceException;

    /**
     * Updates user.
     *
     * @param user the user
     * @throws ServiceException the service exception
     */
    void updateUser(User user) throws ServiceException;

    /**
     * Changes password.
     *
     * @param oldPassword       the old password
     * @param newPassword       the new password
     * @param repeatNewPassword the repeat new password
     * @param userId            the user id
     * @throws ServiceException the service exception
     */
    void changePassword(String oldPassword, String newPassword, String repeatNewPassword, long userId) throws ServiceException;

    /**
     * Finds org info egr.
     *
     * @param organizationUnp the organization unp
     * @return the egr
     * @throws ServiceException the service exception
     */
    Egr findOrgInfo(String organizationUnp) throws ServiceException;

    /**
     * Finds all users list.
     *
     * @param currentPage the current page
     * @param rowsPerPage the rows per page
     * @return the list
     * @throws ServiceException the service exception
     */
    List<User> findAllUsers(int currentPage, int rowsPerPage) throws ServiceException;

    /**
     * Blocks user.
     *
     * @param userId     the user id
     * @param userStatus the user status
     * @throws ServiceException the service exception
     */
    void blockUser(long userId, boolean userStatus) throws ServiceException;

    /**
     * Finds users by tariff id list.
     *
     * @param tariffId    the tariff id
     * @param currentPage the current page
     * @param rowsPerPage the rows per page
     * @return the list
     * @throws ServiceException the service exception
     */
    List<User> findUsersByTariffId(String tariffId, int currentPage, int rowsPerPage) throws ServiceException;

    /**
     * Finds users by searching value list.
     *
     * @param tariffId       the tariff id
     * @param currentPage    the current page
     * @param rowsCount      the rows count
     * @param searchingValue the searching value
     * @return the list
     * @throws ServiceException the service exception
     */
    List<User> findUsersBySearchingValue(String tariffId, int currentPage, int rowsCount, String searchingValue) throws ServiceException;

}
