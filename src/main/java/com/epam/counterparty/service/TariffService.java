package com.epam.counterparty.service;

import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.ServiceException;

import java.util.List;

/**
 * The {@code TariffService} interface represents methods for working with {@code Tariff} entity.
 *
 * @author Pavel
 * @since 26.06.2019
 */
public interface TariffService {
    /**
     * Finds tariff by name tariff.
     *
     * @param tariff the tariff
     * @return the tariff
     * @throws ServiceException the service exception
     */
    Tariff findTariffByName(Tariff tariff) throws ServiceException;

    /**
     * Finds tariffs list.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Tariff> findTariffs() throws ServiceException;

    /**
     * Finds working tariffs list.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Tariff> findWorkingTariffs() throws ServiceException;

    /**
     * Changes tariff.
     *
     * @param tariff the tariff
     * @param userId the user id
     * @throws ServiceException the service exception
     */
    void changeTariff(Tariff tariff, long userId) throws ServiceException;

    /**
     * Extends tariff.
     *
     * @param tariff the tariff
     * @param userId the user id
     * @throws ServiceException the service exception
     */
    void extendTariff(Tariff tariff, long userId) throws ServiceException;

    /**
     * Sets tariff archival.
     *
     * @param tariffId the tariff id
     * @throws ServiceException the service exception
     */
    void setTariffArchival(long tariffId) throws ServiceException;

    /**
     * Finds tariff by tariff id.
     *
     * @param tariffId the tariff id
     * @return the tariff
     * @throws ServiceException the service exception
     */
    Tariff findTariffById(long tariffId) throws ServiceException;

    /**
     * Adds tariff.
     *
     * @param tariff the tariff
     * @return the tariff
     * @throws ServiceException the service exception
     */
    Tariff addTariff(Tariff tariff) throws ServiceException;

    /**
     * Updates tariff.
     *
     * @param tariff the tariff
     * @throws ServiceException the service exception
     */
    void updateTariff(Tariff tariff) throws ServiceException;

    /**
     * Creates working tariffs list list.
     *
     * @return the list of Tariff objects
     * @throws ServiceException the service exception
     */
    List<Tariff> createWorkingTariffsList() throws ServiceException;
}
