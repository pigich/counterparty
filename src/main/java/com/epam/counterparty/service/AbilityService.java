package com.epam.counterparty.service;

import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.exception.ServiceException;

import java.util.List;

/**
 * The {@code AbilityService} interface represents methods for working with {@code Ability} entity.
 *
 * @author Pavel
 * @since 26.06.2019
 */
public interface AbilityService {
    /**
     * Finds abilities list.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Ability> findAbilities() throws ServiceException;

    /**
     * Updates ability.
     *
     * @param ability the ability
     * @throws ServiceException the service exception
     */
    void updateAbility(Ability ability) throws ServiceException;

    /**
     * Adds ability ability.
     *
     * @param ability the ability
     * @return the ability
     * @throws ServiceException the service exception
     */
    Ability addAbility(Ability ability) throws ServiceException;

    /**
     * Finds ability by name ability.
     *
     * @param ability the ability
     * @return the ability
     * @throws ServiceException the service exception
     */
    Ability findAbilityByName(Ability ability) throws ServiceException;

    /**
     * Deletes ability by id.
     *
     * @param abilityId the ability id
     * @throws ServiceException the service exception
     */
    void deleteAbilityById(long abilityId) throws ServiceException;

    /**
     * Finds ability by tariff id list.
     *
     * @param tariffId the tariff id
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Ability> findAbilityByTariffId(long tariffId) throws ServiceException;

    /**
     * Finds abilities by tariff state list.
     *
     * @param tariffState the tariff state
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Ability> findAbilitiesByTariffState(boolean tariffState) throws ServiceException;
}
