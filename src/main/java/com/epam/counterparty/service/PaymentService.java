package com.epam.counterparty.service;

import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.exception.ServiceException;

import java.math.BigDecimal;
import java.util.List;

/**
 * The {@code PaymentService} interface represents methods for working with {@code Payment} entity.
 *
 * @author Pavel
 * @since 04.07.2019
 */
public interface PaymentService {
    /**
     * Does payment transaction.
     *
     * @param userId the user id
     * @param amount the amount
     * @throws ServiceException the service exception
     */
    void doPaymentTransaction(long userId, BigDecimal amount) throws ServiceException;

    /**
     * Does payment.
     *
     * @param userId       the user id
     * @param stringAmount the string amount
     * @throws ServiceException the service exception
     */
    void doPayment(long userId, String stringAmount) throws ServiceException;

    /**
     * Finds payment payment.
     *
     * @param userId the user id
     * @return the payment
     * @throws ServiceException the service exception
     */
    Payment findPayment(long userId) throws ServiceException;

    /**
     * Finds payments list.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Payment> findPayments() throws ServiceException;

}
