package com.epam.counterparty.service;

import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.util.encoders.Hex;

import java.security.SecureRandom;

/**
 * The {@code PasswordService} class represents represents methods for working with passwords.
 *
 * @author Pavel
 * @since 06.06.2019
 */
public class PasswordService {
    private static final int PASSWORD_LENGTH = 12;
    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom rnd = new SecureRandom();

    private PasswordService() {
    }

    /**
     * Generates password.
     *
     * @return the string
     */
    public static String generatePassword() {
        StringBuilder sb = new StringBuilder(PASSWORD_LENGTH);
        for (int i = 0; i < PASSWORD_LENGTH; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    /**
     * Crypts password.
     *
     * @param pass the pass
     * @return the string
     */
    public static String cryptPassword(String pass) {
        SHA3.DigestSHA3 digestSHA3 = new SHA3.Digest256();
        byte[] encryptedData = digestSHA3.digest(pass.getBytes());
        return Hex.toHexString(encryptedData);
    }

}
