package com.epam.counterparty.service;

import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.exception.ServiceException;

import java.util.List;

/**
 * The {@code ProfileService} interface represents methods for working with {@code Profile} entity.
 *
 * @author Pavel
 * @since 04.07.2019
 */
public interface ProfileService {
    /**
     * Finds user profile by user id.
     *
     * @param userId the user id
     * @return the profile
     * @throws ServiceException the service exception
     */
    Profile findUserProfileById(long userId) throws ServiceException;

    /**
     * Changes user profile.
     *
     * @param name    the name
     * @param phone   the phone
     * @param orgUnp  the org unp
     * @param orgName the org name
     * @param userId  the user id
     * @return the profile
     * @throws ServiceException the service exception
     */
    Profile changeUserProfile(String name, String phone, String orgUnp, String orgName, long userId) throws ServiceException;

    /**
     * Finds profiles.
     *
     * @return the list
     * @throws ServiceException the service exception
     */
    List<Profile> findProfiles() throws ServiceException;
}
