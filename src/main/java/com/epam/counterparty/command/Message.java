package com.epam.counterparty.command;

/**
 * @since 04.07.2019
 * @author Pavel
 */
public class Message {
    /**
     * Messages for logs
     */
    public static final String MESSAGE_INVALID_EMAIL_ADDRESS = "Invalid email address:";
    public static final String MESSAGE_INVALID_EMAIL_BODY = "Bad message body:";
    public static final String MESSAGE_SEND_EMAIL_EXCEPTION = "Send email exception";
    public static final String MESSAGE_UNKNOWN_EMAIL_TYPE = "Unknown email type";
    public static final String MESSAGE_ERROR_IN_PARSE_JSON_STRING = "Error in parse Json string ";
    public static final String MESSAGE_EMPTY_COMMAND_MESSAGE = "Request without command!";
    public static final String MESSAGE_CONNECTION_POOL_ALREADY_EXISTS = "ConnectionPool already exists!";
    public static final String MESSAGE_ERROR_CREATING_CONNECTION = "An error while creating connection to database ";
    public static final String MESSAGE_ERROR_INIT_JDBC_DRIVER = "An error while initializing JDBC driver  ";
    public static final String MESSAGE_CONNECTION_NOT_CONTROLLED = "Connection not controlled by this connection pool: ";
    public static final String MESSAGE_EXCEPTION_CREATING_AVAILABLE_POOL_OF_CONNECTIONS = "Exception creating available pool of connections";
    public static final String MESSAGE_NO_AVAILABLE_CONNECTIONS_CREATED = "No available connections created";
    public static final String MESSAGE_EXCEPTION_PUT_CONNECTION_TO_POOL = "Exception while put connection to pool";
    public static final String MESSAGE_DESTROY_CONNECTION_POOL_EXCEPTION = "Destroy connection pool exception";
    public static final String MESSAGE_EXCEPTION_IN_TAKE_CONNECTION = "Exception while take connection";
    public static final String MESSAGE_EXCEPTION_IN_FIND_FEES_METHOD = "Exception in findFees method ";
    public static final String MESSAGE_NOT_RECOGNIZED_ENTITY_TYPE = "Not recognized Entity type";
    public static final String MESSAGE_ENTITY_CREATING_ERROR = "Entity creating error";
    public static final String MESSAGE_LOGIN_IS_INCORRECT_EXCEPTION = "Login is incorrect";
    public static final String MESSAGE_PASSWORD_IS_INCORRECT_EXCEPTION = "Password is incorrect";
    public static final String MESSAGE_DESTROY_CONNECTION_EXCEPTION = "Error destroying connection";
    public static final String MESSAGE_ERROR_WHILE_UPDATING_WORKING_TARIFFS_ATTRIBUTE = "Error while updating WORKING_TARIFFS attribute";
    public static final String MESSAGE_EXCEPTION_IN_CLOSING_PREPARED_STATEMENT = "Exception in closing PreparedStatement";
    public static final String MESSAGE_EXCEPTION_IN_CLOSING_RESULT_SET = "Exception in closing ResultSet";
    public static final String MESSAGE_EXCEPTION_IN_FIND_PAYMENTS_METHOD = "Exception in findPayments method";
    public static final String MESSAGE_EXCEPTION_IN_MAKE_FEE_METHOD = "Exception in makeFee method";
    public static final String MESSAGE_EXCEPTION_IN_FIND_PAYMENT_METHOD = "Exception in findPayment method";
    public static final String MESSAGE_EXCEPTION_IN_UPDATE_METHOD = "Exception in update method";
    public static final String MESSAGE_EXCEPTION_UPDATE_USER_PROFILE = "Error in updateUserProfile method";
    public static final String MESSAGE_EXCEPTION_FIND_PROFILES = "Find profiles error";
    public static final String MESSAGE_EXCEPTION_IN_FIND_TARIFF_BY_NAME_METHOD = "Find Tariff in database by name error";
    public static final String MESSAGE_EXCEPTION_DELETE_TARIFF_FROM_DATABASE = "Delete Tariff from database error";
    public static final String MESSAGE_EXCEPTION_IN_FIND_TARIFF_BY_ID_METHOD = "Error in findTariffById method";
    public static final String MESSAGE_EXCEPTION_IN_FIND_TARIFFS_METHOD = "Error in findTariffs method";
    public static final String MESSAGE_EXCEPTION_IN_FIND_WORKING_TARIFFS_METHOD = "Error in findTariffs method";
    public static final String MESSAGE_EXCEPTION_CHANGE_TARIFF = "Change Tariff in database error";
    public static final String MESSAGE_EXCEPTION_EXTEND_TARIFF = "Extend tariff";
    public static final String MESSAGE_EXCEPTION_READ_PROPERTIES_FILE = "Read properties file error";
    /**
     * Message with localization
     */
    public static final String MESSAGE_LOGIN_ERROR = "message.login.error";
    public static final String MESSAGE_CHANGE_PASSWORD_OK = "message.password.ok";
    public static final String MESSAGE_PROFILE_CHANGED = "message.profile.changed";
    public static final String MESSAGE_TARIFF_CHANGED = "message.tariff.changed";
    public static final String MESSAGE_NOT_ENOUGH_AMOUNT_ERROR = "message.not-enough-amount.error";
    public static final String MESSAGE_DATA_NOT_FOUND = "message.data-not-found";
    public static final String MESSAGE_EXCEPTION_READ_JSON_FORMAT = "message.data-rest-json-error";
    public static final String MESSAGE_BUSY_EMAIL = "message.busy_email";
    public static final String MESSAGE_NO_SUCH_EMAIL = "message.no_such_email";
    public static final String MESSAGE_BALANCE_UPDATE_OK = "message.balance.update_ok";
    public static final String MESSAGE_WRONG_ACTION = "message.wrongAction";
    public static final String MESSAGE_ABILITY_EXISTS = "message.ability_exists";
    public static final String MESSAGE_TARIFF_EXISTS = "message.tariff_exists";
    public static final String MESSAGE_TARIFF_EXTENDED = "message.tariff.extended";
    public static final String MESSAGE_TARIFF_NOT_ARCHIVED = "message.tariff.not-archived";
    public static final String MESSAGE_ABILITY_NOT_DELETED = "message.ability.not-deleted";
    public static final String MESSAGE_TARIFF_NOT_EXTENDED = "message.tariff.not.extended";
    public static final String MESSAGE_TAG_GREETING = "message.tag.greeting";
    public static final String MESSAGE_TAG_PROPOSITION = "message.tag.proposition";
    public static final String MESSAGE_TRANSACTION_ERROR = "message.transaction.error";
    /**
     * Messages for Validator
     */
    public static final String MESSAGE_PASSWORD_WRONG_FORMAT = "validator.password.wrong_format";
    public static final String MESSAGE_PASSWORDS_NOT_EQUALS = "validator.password.not_equal";
    public static final String MESSAGE_WRONG_OLD_PASSWORD = "validator.password.wrong_old_password";
    public static final String MESSAGE_AMOUNT_WRONG_FORMAT = "validator.amount.wrong_format";
    public static final String MESSAGE_WRONG_USER_NAME_FORMAT = "validator.data.wrong_name";
    public static final String MESSAGE_WRONG_PHONE_FORMAT = "validator.data.wrong_phone";
    public static final String MESSAGE_WRONG_ORG_UNP_FORMAT = "validator.data.wrong_unp";
    public static final String MESSAGE_WRONG_ORG_NAME_FORMAT = "validator.data.wrong_org_name";
    public static final String MESSAGE_WRONG_SEARCH_VALUE_FORMAT = "validator.data.wrong_search_value";
    public static final String MESSAGE_WRONG_EMAIL_FORMAT = "validator.data.wrong_email";
    public static final String MESSAGE_WRONG_NAME_FORMAT = "validator.data.wrong_tariff_ability_name_format";
    public static final String MESSAGE_WRONG_PERIOD_FORMAT = "validator.data.wrong_period_format";
    public static final String MESSAGE_PRICE_WRONG_FORMAT = "validator.data.wrong_price_format";

    private Message() {
    }
}
