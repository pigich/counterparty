package com.epam.counterparty.command;

/**
 * @author Pavel
 * @since 29.05.2019
 */
public class Attribute {
    /**
     * Attribute and Parameter names
     */
    public static final String ERROR_MESSAGE = "error_message";
    public static final String OK_MESSAGE = "ok_message";
    public static final String LIST_OF_USERS = "listOfObjects";
    public static final String QUANTITY_OBJECT = "Quantity";
    public static final String CURRENT_PAGE = "current_page";
    public static final String SEARCHING_VALUE = "searching_value";
    public static final String ROWS_PER_PAGE = "rows_per_page";
    public static final String ROWS_COUNT = "rows_count";
    public static final String PAGES_COUNT = "pages_count";
    public static final String USERS = "users";
    public static final String PAYMENTS = "payments";
    public static final String FEES = "fees";
    public static final String SEARCHING_UNP = "searching_unp";
    public static final String TARIFFS = "tariffs";
    public static final String TARIFFS_CHANGED = "tariffs_changed";
    public static final String WORKING_TARIFFS = "working_tariffs";
    public static final String ABILITIES = "abilities";
    public static final String ABILITY_ID_LIST = "ability_id_list";
    public static final String PERIOD = "period";
    public static final String PRICE = "price";
    public static final String TARIFF_NAME = "tariff_name";
    public static final String TARIFF_ID = "tariff_id";
    public static final String TARIFF_NAME_RU = "tariff_name_ru";
    public static final String TARIFF_NAME_EN = "tariff_name_en";
    public static final String ARCHIVAL = "archival";
    public static final String ABILITY_ID = "ability_id";
    public static final String ABILITY_NAME_RU = "ability_name_ru";
    public static final String ABILITY_NAME_EN = "ability_name_en";
    public static final String CURRENT_TARIFF = "current_tariff";
    public static final String CURRENT_TARIFF_ID = "current_tariff_id";
    public static final String CURRENT_ABILITY_ID = "current_ability_id";
    public static final String FEE_AMOUNT = "fee_amount";
    public static final String AMOUNT = "amount";
    public static final String FEE_DATE = "date";
    public static final String CURRENT_BALANCE = "current_balance";
    public static final String USER_TARIFF_END_PERIOD_STATUS = "user_tariff_end_period_status";
    public static final String PAYMENTS_START_DATE = "start_date";
    public static final String PAYMENTS_END_DATE = "end_date";
    public static final String REGISTRATION_PAGE_STATUS = "registration_page_status";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String ROLE_ID = "role_id";
    public static final String ROLE = "role";
    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
    public static final String GUEST_ROLE = "GUEST";
    public static final String DELETED = "deleted";
    public static final String LANG = "lang";
    public static final String USER_ID = "user_id";
    public static final String ID = "id";
    public static final String CURRENT_USER_ID = "current_user_id";
    public static final String CURRENT_USER_STATUS = "current_user_status";
    public static final String CURRENT_USER_PROFILE = "current_user_profile";
    public static final String ORGANIZATION_UNP = "organization_unp";
    public static final String ORGANIZATION_NAME = "organization_name";
    public static final String ORG_INFO = "orgInfo";
    public static final String COMMAND = "command";
    public static final String PHONE = "phone";
    public static final String FULL_NAME = "full_name";
    public static final String OLD_PWD = "old_pwd";
    public static final String NEW_PWD = "new_pwd";
    public static final String REPEAT_PWD = "repeat_pwd";
    public static final String CURRENT_JSP_PAGE = "page";
    public static final String ALL_TARIFFS = "All";
    public static final String ALL_TARIFFS_RU = "Все";
    /**
     * Command names
     */
    public static final String EXTEND_TARIFF = "extend_tariff";
    public static final String TARIFF_INFO = "tariff_info";
    public static final String PAYMENT_INFO = "payment_info";
    public static final String USER_INFO = "user_info";
    public static final String MAIN = "main";
    public static final String PROFILE = "profile";
    public static final String PROFILE_ADMIN = "profile_admin";
    public static final String USER_PAGE = "user_page";
    public static final String ADMIN_PAGE = "admin_page";
    public static final String REGISTERED = "registered";
    public static final String SIGN_UP = "sign_up";
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String RECOVER_PASSWORD = "recover_password";
    public static final String BLOCK_USER = "block_user";
    public static final String BLOCK_USER_AJAX = "block_user_ajax";
    public static final String FIND_FEES_AJAX = "find_fees_ajax";
    public static final String FIND_USERS_AJAX = "find_users_ajax";
    public static final String FIND_ABILITY_AJAX = "find_ability_ajax";
    public static final String FIND_ABILITIES = "find_abilities";
    public static final String DELETE_ABILITY = "delete_ability";
    public static final String UPDATE_ABILITY = "update_ability";
    public static final String ADD_ABILITY = "add_ability";
    public static final String ADD_TARIFF = "add_tariff";
    public static final String DELETE_TARIFF = "delete_tariff";
    public static final String UPDATE_TARIFF = "update_tariff";
    public static final String FIND_ORG_INFO = "find_org_info";
    public static final String TO_SIGN_UP = "to_sign_up";
    public static final String CHANGE_PWD = "change_pwd";
    public static final String CHANGE_PROFILE = "change_profile";
    public static final String CHANGE_TARIFF = "change_tariff";
    public static final String MAKE_FEE = "make_fee";
    public static final String TO_PROFILE = "to_profile";
    public static final String TO_ADMIN_PROFILE = "to_admin_profile";
    public static final String TO_ADMIN_PAGE = "to_admin_page";
    public static final String TO_USER_PAGE = "to_user_page";
    public static final String TO_REGISTERED_PAGE = "to_registered_page";
    public static final String TO_MAIN_PAGE = "to_main_page";
    public static final String TO_ERROR_PAGE = "to_error_page";
    public static final String TO_TARIFF_INFO = "to_tariff_info";
    public static final String TO_USER_INFO = "to_user_info";
    public static final String TO_PAYMENTS_INFO = "to_payments_info";
    public static final String DO_COMMAND = "/do?command=";
    /**
     * Table default attributes
     */
    public static final int DEFAULT_ROWS_PER_PAGE = 2;
    public static final int FIRST_PAGE = 1;
    public static final int DEFAULT_ORDER_COLUMN = 1;
    public static final String DEFAULT_SORT_TYPE = "asc";

    private Attribute() {
    }
}
