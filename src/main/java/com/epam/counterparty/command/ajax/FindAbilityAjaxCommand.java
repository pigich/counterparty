package com.epam.counterparty.command.ajax;

import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.AbilityService;
import com.epam.counterparty.service.impl.AbilityServiceImpl;

import java.util.List;

import static com.epam.counterparty.command.Attribute.TARIFF_ID;


/**
 * Finds ability.
 *
 * @author Pavel
 * @since 21.06.2019
 */

public class FindAbilityAjaxCommand implements AjaxCommand {
    @Override
    public String execute(RequestContent content) throws CommandException {
        long tariffId = Long.parseLong(content.getRequestParameters(TARIFF_ID, 0));
        AbilityService abilityService = new AbilityServiceImpl();
        List<Ability> abilities;
        try {
            abilities = abilityService.findAbilityByTariffId(tariffId);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage());
        }
        return setJsonString(abilities);
    }
}