package com.epam.counterparty.command.ajax;

import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.entity.Fee;
import com.epam.counterparty.entity.Quantity;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.FeeService;
import com.epam.counterparty.service.QuantityService;
import com.epam.counterparty.service.impl.FeeServiceImpl;
import com.epam.counterparty.service.impl.QuantityServiceImpl;
import com.epam.counterparty.util.DataValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.epam.counterparty.command.Message.MESSAGE_ERROR_IN_PARSE_JSON_STRING;

/**
 * Finds fees in database and returns json string
 * @see AjaxCommand
 * @author Pavel
 * @since 21.07.2019
 */
public class FindFeesAjaxCommand implements AjaxCommand {
    private static final Logger LOGGER = LogManager.getLogger();

    @SuppressWarnings("unchecked")
    @Override
    public String execute(RequestContent content) throws CommandException {
        String result;
        QuantityService quantityService = new QuantityServiceImpl();
        FeeService feeService = new FeeServiceImpl();
        Quantity quantity;
        List<Fee> fees;
        Map response = new LinkedHashMap();
        List<String[]> listOfFeesStrings = new LinkedList<>();
        Object[] arrayOfFees = {};
        ObjectMapper mapper = new ObjectMapper();
        String searchingValue = content.getRequestParameters("search[value]", 0);
        String orderType = content.getRequestParameters("order[0][dir]", 0);
        int rowsPerPage = Integer.parseInt(content.getRequestParameters("length", 0));
        int currentDraw = Integer.parseInt(content.getRequestParameters("draw", 0));
        int orderColumn = (1 + Integer.parseInt(content.getRequestParameters("order[0][column]", 0)));
        long startPosition = Integer.parseInt(content.getRequestParameters("start", 0));
        try {
            DataValidator dataValidator = new DataValidator();
            dataValidator.validateSearchValue(searchingValue);
            quantity = quantityService.findFeesCount();
            long totalCountOfFees = quantity.getRowsCount();
            if (searchingValue != null && !searchingValue.isEmpty()) {
                quantity = quantityService.findQuantityOfFeesByValue(searchingValue);
                fees = feeService.findFeesBySearchingValue(startPosition, rowsPerPage, searchingValue, orderColumn, orderType);
            } else {
                quantity.setRowsCount(totalCountOfFees);
                fees = feeService.findFees(startPosition, rowsPerPage, orderColumn, orderType);
            }
            for (Fee fee : fees) {
                String[] array = new String[4];
                array[0] = String.valueOf(fee.getId());
                array[1] = String.valueOf(fee.getUserId());
                array[2] = String.valueOf(fee.getAmount());
                array[3] = fee.getFeesDate();
                listOfFeesStrings.add(array);
            }
            arrayOfFees = listOfFeesStrings.toArray();
            response.put("draw", currentDraw);
            response.put("recordsTotal", totalCountOfFees);
            response.put("recordsFiltered", quantity.getRowsCount());
            response.put("data", arrayOfFees);
            result = mapper.writeValueAsString(response);
        } catch (IOException e) {
            throw new CommandException(MESSAGE_ERROR_IN_PARSE_JSON_STRING, e);
        } catch (ServiceException e) {
            try {
                response.put("draw", currentDraw);
                response.put("recordsTotal", 0);
                response.put("recordsFiltered", 0);
                response.put("data", arrayOfFees);
                return mapper.writeValueAsString(response);
            } catch (JsonProcessingException ex) {
                throw new CommandException(MESSAGE_ERROR_IN_PARSE_JSON_STRING, e);
            }
        }
        return result;
    }
}