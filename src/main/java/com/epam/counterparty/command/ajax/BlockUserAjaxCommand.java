package com.epam.counterparty.command.ajax;

import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.UserService;
import com.epam.counterparty.service.impl.UserServiceImpl;

import static com.epam.counterparty.command.Attribute.*;

/**
 * Blocks user
 *
 * @author Pavel
 * @since 27.06.2019
 */
public class BlockUserAjaxCommand implements AjaxCommand {
    @Override
    public String execute(RequestContent content) throws CommandException {
        UserService userService = new UserServiceImpl();
        String result;
        try {
            String tariffId = content.getRequestParameters(CURRENT_TARIFF_ID, 0);
            String searchingValue = content.getRequestParameters(SEARCHING_VALUE, 0);
            int currentPage = Integer.parseInt(content.getRequestParameters(CURRENT_PAGE, 0));
            int rowsCount = Integer.parseInt(content.getRequestParameters(ROWS_PER_PAGE, 0));
            long userId = Long.parseLong(content.getRequestParameters(CURRENT_USER_ID, 0));
            boolean isUserBlocked = Boolean.parseBoolean(content.getRequestParameters(CURRENT_USER_STATUS, 0));
            String locale = String.valueOf(content.getSessionAttribute(LANG));
            userService.blockUser(userId, isUserBlocked);
            result = setJsonStringWithQuantity(tariffId, currentPage, rowsCount, searchingValue, locale);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage());
        }
        return result;
    }
}