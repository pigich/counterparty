package com.epam.counterparty.command.ajax;

import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.exception.CommandException;

import static com.epam.counterparty.command.Attribute.*;

/**
 * Finds users in database and returns json string
 *
 * @author Pavel
 * @since 30.06.2019
 */
public class FindUsersAjaxCommand implements AjaxCommand {
    @Override
    public String execute(RequestContent content) throws CommandException {
        String result;
        String tariffId = content.getRequestParameters(CURRENT_TARIFF_ID, 0);
        String searchingValue = content.getRequestParameters(SEARCHING_VALUE, 0);
        int rowsCount = Integer.parseInt(content.getRequestParameters(ROWS_PER_PAGE, 0));
        int currentPage = Integer.parseInt(content.getRequestParameters(CURRENT_PAGE, 0));
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        result = setJsonStringWithQuantity(tariffId, currentPage, rowsCount, searchingValue, locale);
        return result;
    }
}