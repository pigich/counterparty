package com.epam.counterparty.command.ajax;

import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.entity.Entity;
import com.epam.counterparty.entity.Quantity;
import com.epam.counterparty.entity.User;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.QuantityService;
import com.epam.counterparty.service.UserService;
import com.epam.counterparty.service.impl.QuantityServiceImpl;
import com.epam.counterparty.service.impl.UserServiceImpl;
import com.epam.counterparty.util.DataValidator;
import com.epam.counterparty.util.MessageManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.epam.counterparty.command.Attribute.LIST_OF_USERS;
import static com.epam.counterparty.command.Attribute.QUANTITY_OBJECT;
import static com.epam.counterparty.command.Message.MESSAGE_ERROR_IN_PARSE_JSON_STRING;

/**
 * This interface represents ajax command
 *
 * @author Pavel
 * @see AjaxCommandFactory
 * @since 21.06.2019
 */
public interface AjaxCommand {
    /**
     * Executes business logic of defined command.
     *
     * @param requestContent {@link RequestContent} the request content
     * @return the string response
     * @throws CommandException the command exception
     */
    String execute(RequestContent requestContent) throws CommandException;

    /**
     * Sets json string which will returns on ajax request.
     *
     * @param list the list of Entity objects {@link Entity}
     * @return the json string to be send back on ajax request
     * @throws CommandException the command exception
     */
    default String setJsonString(List<? extends Entity> list) throws CommandException {
        String json;
        ObjectMapper mapper = new ObjectMapper();
        try {
            json = mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            throw new CommandException(MESSAGE_ERROR_IN_PARSE_JSON_STRING, e);
        }
        return json;
    }

    /**
     * returns string in json format with users array and quantity of users.
     *
     * @param tariffId       the tariff id
     * @param currentPage    the current page number
     * @param rowsPerPage    the rows count per page
     * @param searchingValue the searching value
     * @param locale         the current locale
     * @return the json string with quantity
     * @throws CommandException the command exception
     */
    @SuppressWarnings("unchecked")
    default String setJsonStringWithQuantity(String tariffId, int currentPage, int rowsPerPage, String searchingValue, String locale) throws CommandException {
        QuantityService quantityService = new QuantityServiceImpl();
        UserService userService = new UserServiceImpl();
        Quantity quantity;
        List<User> users;
        String result;
        ObjectMapper mapper = new ObjectMapper();
        try {
            DataValidator dataValidator = new DataValidator();
            dataValidator.validateSearchValue(searchingValue);
            if (!searchingValue.isEmpty()) {
                quantity = quantityService.findQuantityOfUsersByValue(searchingValue, tariffId);
                users = userService.findUsersBySearchingValue(tariffId, currentPage, rowsPerPage, searchingValue);
            } else {
                quantity = quantityService.findQuantityOfUsers(tariffId);
                users = userService.findUsersByTariffId(tariffId, currentPage, rowsPerPage);
            }
            long pagesCount = quantityService.setPageCount(quantity, rowsPerPage);
            quantity.setPagesCount(pagesCount);

            Map response = new HashMap<>();
            response.put(LIST_OF_USERS, users);
            response.put(QUANTITY_OBJECT, quantity);
            result = mapper.writeValueAsString(response);

        } catch (IOException e) {
            throw new CommandException(MESSAGE_ERROR_IN_PARSE_JSON_STRING, e);
        } catch (ServiceException e) {
            try {
                return mapper.writeValueAsString(MessageManager.getProperty(e.getMessage(), locale));
            } catch (JsonProcessingException ex) {
                throw new CommandException(MESSAGE_ERROR_IN_PARSE_JSON_STRING, e);
            }
        }
        return result;
    }
}
