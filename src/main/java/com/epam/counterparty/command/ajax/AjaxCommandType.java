package com.epam.counterparty.command.ajax;

/**
 * The enum Ajax command type.
 *
 * @author Pavel
 * @since 21.06.2019
 */
public enum AjaxCommandType {

    /**
     *  Finds fees.
     */
    FIND_FEES_AJAX{{
        this.command = new FindFeesAjaxCommand();
    }},
    /**
     * The ajax command which blocks users.
     */
    BLOCK_USER_AJAX {{
        this.command = new BlockUserAjaxCommand();
    }},
    /**
     *  Finds users.
     */
    FIND_USERS_AJAX {{
        this.command = new FindUsersAjaxCommand();
    }},
    /**
     *  Finds abilities.
     */
    FIND_ABILITY_AJAX {{
        this.command = new FindAbilityAjaxCommand();
    }};
    /**
     * The Command.
     */
    protected AjaxCommand command = null;

    /**
     * Gets command.
     *
     * @return the command
     */
    public AjaxCommand getCommand() {
        return command;
    }
}
