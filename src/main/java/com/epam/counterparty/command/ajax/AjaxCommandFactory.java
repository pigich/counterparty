package com.epam.counterparty.command.ajax;

import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.exception.CommandException;

import static com.epam.counterparty.command.Attribute.COMMAND;
import static com.epam.counterparty.command.Message.MESSAGE_EMPTY_COMMAND_MESSAGE;
import static com.epam.counterparty.command.Message.MESSAGE_WRONG_ACTION;

/**Defines ajax command.
 *
 * @since 21.06.2019
 * @author Pavel
 */
public class AjaxCommandFactory {

    private AjaxCommandFactory() {
    }

    /**
     * Defines ajax command from RequestContent object where command inserted
     * from HttpServletRequest in AjaxController {@link com.epam.counterparty.controller.AjaxController }
     *
     * @param content the RequestContent object {@link RequestContent}
     * @return the ajax command
     * @throws CommandException the command exception
     */
    public static AjaxCommand defineCommand(RequestContent content) throws CommandException {
        AjaxCommand ajaxCommand;
        String cmdName = content.getRequestParameters(COMMAND, 0);
        if (cmdName == null || cmdName.isEmpty()) {
            throw new CommandException(MESSAGE_EMPTY_COMMAND_MESSAGE);
        }
        try {
            AjaxCommandType ajaxCommandType = AjaxCommandType.valueOf(cmdName.toUpperCase());
            ajaxCommand = ajaxCommandType.getCommand();
            if (ajaxCommand == null) {
                throw new CommandException(MESSAGE_WRONG_ACTION);
            }
        } catch (IllegalArgumentException e) {
            throw new CommandException(MESSAGE_WRONG_ACTION);
        }
        return ajaxCommand;
    }
}
