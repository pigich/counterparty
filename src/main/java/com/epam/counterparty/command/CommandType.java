package com.epam.counterparty.command;

import com.epam.counterparty.command.impl.ability.AddAbilityCommand;
import com.epam.counterparty.command.impl.ability.DeleteAbilityCommand;
import com.epam.counterparty.command.impl.ability.UpdateAbilityCommand;
import com.epam.counterparty.command.impl.fee.MakeFeeCommand;
import com.epam.counterparty.command.impl.forwad.*;
import com.epam.counterparty.command.impl.tariff.*;
import com.epam.counterparty.command.impl.user.*;

/**
 * @since 21.05.2019
 * @author Pavel
 */
public enum CommandType {
    /**
     *  Shows not blocked tariffs.
     */
    SHOW_TARIFFS {{
        this.command = new ShowTariffsCommand();
    }},
    /**
     *   Extends current user tariff.
     */
    EXTEND_TARIFF {{
        this.command = new ExtendTariffCommand();
    }},
    /**
     *  Recover password.
     */
    RECOVER_PASSWORD {{
        this.command = new RecoverPassCommand();
    }},
    /**
     *  Find organization info by UNP.
     */
    FIND_ORG_INFO {{
        this.command = new FindOrgInfoCommand();
    }},
    /**
     *  Updates ability.
     */
    UPDATE_ABILITY {{
        this.command = new UpdateAbilityCommand();
    }},
    /**
     *  Adds ability.
     */
    ADD_ABILITY {{
        this.command = new AddAbilityCommand();
    }},
    /**
     *  Deletes ability.
     */
    DELETE_ABILITY {{
        this.command = new DeleteAbilityCommand();
    }},
    /**
     *  Updates tariff.
     */
    UPDATE_TARIFF {{
        this.command = new UpdateTariffCommand();
    }},
    /**
     *  Adds tariff.
     */
    ADD_TARIFF {{
        this.command = new AddTariffCommand();
    }},
    /**
     *  Deletes tariff.
     */
    DELETE_TARIFF {{
        this.command = new DeleteTariffCommand();
    }},
    /**
     *  Goes to tariff info page.
     */
    TO_TARIFF_INFO {{
        this.command = new ToTariffInfoCommand();
    }},
    /**
     *  Goes to payments info page.
     */
    TO_PAYMENTS_INFO {{
        this.command = new ToPaymentsInfoCommand();
    }},
    /**
     *  Goes to user info page.
     */
    TO_USER_INFO {{
        this.command = new ToUserInfoCommand();
    }},
    /**
     *  Goes to sign up page.
     */
    TO_SIGN_UP {{
        this.command = new ToSignUpCommand();
    }},
    /**
     *  Makes fee.
     */
    MAKE_FEE {{
        this.command = new MakeFeeCommand();
    }},
    /**
     *  Sign's up.
     */
    SIGN_UP {{
        this.command = new SignUpCommand();
    }},
    /**
     *  Changes password.
     */
    CHANGE_PWD {{
        this.command = new ChangePwdCommand();
    }},
    /**
     *  Changes profile.
     */
    CHANGE_PROFILE {{
        this.command = new ChangeProfileCommand();
    }},
    /**
     *  Changes tariff.
     */
    CHANGE_TARIFF {{
        this.command = new ChangeTariffCommand();
    }},
    /**
     *  Goes to registered page.
     */
    TO_REGISTERED_PAGE {{
        this.command = new ToRegisteredPageCommand();
    }},
    /**
     *  Goes to main page.
     */
    TO_MAIN_PAGE {{
        this.command = new ToMainPageCommand();
    }},
    /**
     *  Login.
     */
    LOGIN {{
        this.command = new LoginCommand();
    }},
    /**
     *  Logout.
     */
    LOGOUT {{
        this.command = new LogoutCommand();
    }},
    /**
     *  Goes to user page.
     */
    TO_USER_PAGE {{
        this.command = new ToUserPageCommand();
    }},
    /**
     *  Goes to admin page.
     */
    TO_ADMIN_PAGE {{
        this.command = new ToAdminPageCommand();
    }},
    /**
     *  Goes to profile page.
     */
    TO_PROFILE {{
        this.command = new ToProfilePageCommand();
    }},
    /**
     *  Goes to admin profile page.
     */
    TO_ADMIN_PROFILE {{
        this.command = new ToAdminProfileCommand();
    }},
    /**
     *  Changes lang.
     */
    LANG {{
        this.command = new ChangeLangCommand();
    }},
    /**
     *  To error page.
     */
    TO_ERROR {{
        this.command = new ToErrorPageCommand();
    }};
    /**
     *  Command.
     */
    protected Command command = null;

    /**
     * Gets command.
     *
     * @return the command
     */
    public Command getCommand() {
        return command;
    }
}
