package com.epam.counterparty.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * The {@code RequestContent} class represents request content
 *
 * @author Pavel
 * @since 29.05.2019
 */
public class RequestContent {
    private HttpSession session;
    private HashMap<String, Object> requestAttributes;
    private HashMap<String, String[]> requestParameters;
    private HashMap<String, Object> sessionAttributes;

    /**
     * Instantiates a new Request content.
     */
    public RequestContent() {
        requestParameters = new HashMap<>();
        requestAttributes = new HashMap<>();
        sessionAttributes = new HashMap<>();
    }

    /**
     * Extracts session attributes, request attributes, request parameters from HttpServletRequest object.
     *
     * @param request the HttpServletRequest object
     */
    public void extractValues(HttpServletRequest request) {

        Enumeration<String> attrNames = request.getAttributeNames();
        while (attrNames.hasMoreElements()) {
            String name = attrNames.nextElement();
            Object attr = request.getAttribute(name);
            requestAttributes.put(name, attr);
        }
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String name = paramNames.nextElement();
            String[] param = request.getParameterValues(name);
            requestParameters.put(name, param);
        }
        Enumeration<String> sessionAttrNames = request.getSession().getAttributeNames();
        while (sessionAttrNames.hasMoreElements()) {
            String name = sessionAttrNames.nextElement();
            Object sessionAttr = request.getSession().getAttribute(name);
            sessionAttributes.put(name, sessionAttr);
        }
        session = request.getSession(false);
    }

    /**
     * Inserts session and request attributes to HttpServletRequest object.
     *
     * @param request the HttpServletRequest object
     */
    public void insertAttributes(HttpServletRequest request) {
        for (Map.Entry<String, Object> entry : requestAttributes.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            request.setAttribute(key, value);
        }
        if (request.getSession(false) != null) {
            for (Map.Entry<String, Object> entry : sessionAttributes.entrySet()) {
                String k = entry.getKey();
                Object v = entry.getValue();
                request.getSession().setAttribute(k, v);
            }
        }
    }

    /**
     * Put attribute to request.
     *
     * @param name the name of attribute {@link Attribute}
     * @param attr the attr Object
     */
    public void putAttributeToRequest(String name, Object attr) {
        requestAttributes.put(name, attr);
    }

    /**
     * Put attribute to session.
     *
     * @param name the name of attribute {@link Attribute}
     * @param attr the attr Object
     */
    public void putAttributeToSession(String name, Object attr) {
        sessionAttributes.put(name, attr);
    }

    /**
     * Gets request parameters.
     *
     * @param name  the name of request parameter {@link Attribute}
     * @param index the index
     * @return the request parameters
     */
    public String getRequestParameters(String name, int index) {
        return requestParameters.get(name) == null ? null : requestParameters.get(name)[index];
    }

    /**
     * Gets session attribute.
     *
     * @param name the name of session attribute {@link Attribute}
     * @return the session attribute
     */
    public Object getSessionAttribute(String name) {
        return sessionAttributes.get(name);
    }

    /**
     * Invalidate session.
     */
    public void invalidateSession() {
        if (session != null) {
            session.invalidate();
        }
    }

//    /**
//     * Gets context.
//     *
//     * @return the context
//     */
//    public ServletContext getContext() {
//        return session.getServletContext();
//    }

    @Override
    public String toString() {
        return "RequestContent{" +
                ", requestAttributes=" + requestAttributes +
                ", requestParameters=" + requestParameters +
                ", sessionAttributes=" + sessionAttributes +
                '}';
    }
}
