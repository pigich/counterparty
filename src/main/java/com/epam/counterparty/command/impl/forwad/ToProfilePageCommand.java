package com.epam.counterparty.command.impl.forwad;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.ProfileService;
import com.epam.counterparty.service.TariffService;
import com.epam.counterparty.service.impl.ProfileServiceImpl;
import com.epam.counterparty.service.impl.TariffServiceImpl;
import com.epam.counterparty.util.ConfigurationManager;

import java.util.ArrayList;
import java.util.List;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.PathConstant.PROFILE_PAGE_PATH;

/**
 * Forwards to "profile.jsp".
 *
 * @author Pavel
 * @since 03.06.2019
 */
public class ToProfilePageCommand implements Command {
    @Override
    public Router execute(RequestContent content) throws CommandException {
        SendingType sendingType = SendingType.FORWARD;
        String page = ConfigurationManager.getProperty(PROFILE_PAGE_PATH);
        Profile profile;
        ProfileService profileService = new ProfileServiceImpl();
        TariffService tariffService = new TariffServiceImpl();
        List<Tariff> tariffs;
        List<Tariff> tariffsWithoutDemo;
        try {
            long userId = Long.parseLong(content.getSessionAttribute(USER_ID).toString());
            profile = profileService.findUserProfileById(userId);
            tariffs = tariffService.findTariffs();
            tariffsWithoutDemo = new ArrayList<>(tariffs.subList(1, (tariffs.size())));
            content.putAttributeToSession(TARIFFS, tariffsWithoutDemo);
            content.putAttributeToSession(FULL_NAME, profile.getFullName());
            content.putAttributeToSession(ORGANIZATION_NAME, profile.getOrgName());
            content.putAttributeToSession(ORGANIZATION_UNP, profile.getOrgUnp());
            content.putAttributeToSession(PHONE, profile.getPhone());
            content.putAttributeToSession(TARIFF_ID, profile.getTariffId());
            for (Tariff tariff : tariffs) {
                if (tariff.getId() == profile.getTariffId()) {
                    content.putAttributeToSession(CURRENT_TARIFF, tariff);
                    break;
                }
            }
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage());
        }
        return new Router(page, sendingType);
    }
}
