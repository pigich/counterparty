package com.epam.counterparty.command.impl.tariff;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.TariffService;
import com.epam.counterparty.service.impl.TariffServiceImpl;
import com.epam.counterparty.util.MessageManager;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.MESSAGE_TARIFF_EXISTS;

/**
 * Adds new tariff to database and redirects to "tariff-info.jsp".
 *
 * @author Pavel
 * @since 19.06.2019
 */
public class AddTariffCommand implements Command, BaseTariffCommand {
    @Override
    public Router execute(RequestContent content) {
        TariffService tariffService = new TariffServiceImpl();
        String page;
        SendingType sendingType = SendingType.REDIRECT;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            Tariff tariff = getTariff(content);
            tariff = tariffService.addTariff(tariff);
            content.putAttributeToSession(TARIFFS_CHANGED, true);
            if (tariff != null) {
                page = DO_COMMAND + TO_TARIFF_INFO;
            } else {
                content.putAttributeToSession(ERROR_MESSAGE,
                        MessageManager.getProperty(MESSAGE_TARIFF_EXISTS, locale));
                page = DO_COMMAND + TO_TARIFF_INFO;
            }
        } catch (ServiceException e) {
            page = DO_COMMAND + TO_TARIFF_INFO;
            content.putAttributeToSession(ERROR_MESSAGE,
                    MessageManager.getProperty(e.getMessage(), locale));
        }
        return new Router(page, sendingType);
    }
}
