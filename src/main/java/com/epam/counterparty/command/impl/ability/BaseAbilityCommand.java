package com.epam.counterparty.command.impl.ability;

import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.util.DataValidator;
import org.jetbrains.annotations.NotNull;

import static com.epam.counterparty.command.Attribute.*;

/**
 * Base interface for ability commands.
 *
 * @author Pavel
 * @since 20.06.2019
 */
interface BaseAbilityCommand {
    /**
     * Gets RequestContent object, creates and returns Ability object.
     *
     * @param content the RequestContent object {@link RequestContent}
     * @return the Ability object
     * @throws ServiceException the service exception
     */
    @NotNull
   default Ability getAbility(RequestContent content) throws ServiceException {
        Ability ability = new Ability();
        DataValidator dataValidator = new DataValidator();
        long abilityId = Long.parseLong(content.getRequestParameters(ABILITY_ID, 0));
        String nameEn = content.getRequestParameters(ABILITY_NAME_EN, 0);
        String nameRu = content.getRequestParameters(ABILITY_NAME_RU, 0);
        dataValidator.validateTariffAbilityName(nameEn);
        dataValidator.validateTariffAbilityName(nameRu);
        ability.setId(abilityId);
        ability.setAbilityNameEn(nameEn);
        ability.setAbilityNameRu(nameRu);
        return ability;
    }
}
