package com.epam.counterparty.command.impl.forwad;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.util.ConfigurationManager;

import static com.epam.counterparty.command.PathConstant.ERROR_PAGE_PATH;

/**
 * Forwards to "error.jsp".
 *
 * @author Pavel
 * @since 02.06.2019
 */
public class ToErrorPageCommand implements Command {
    @Override
    public Router execute(RequestContent content) {
        SendingType sendingType = SendingType.FORWARD;
        String page = ConfigurationManager.getProperty(ERROR_PAGE_PATH);
        return new Router(page, sendingType);
    }
}