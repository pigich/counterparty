package com.epam.counterparty.command.impl.forwad;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.entity.Quantity;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.entity.User;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.ProfileService;
import com.epam.counterparty.service.QuantityService;
import com.epam.counterparty.service.TariffService;
import com.epam.counterparty.service.UserService;
import com.epam.counterparty.service.impl.ProfileServiceImpl;
import com.epam.counterparty.service.impl.QuantityServiceImpl;
import com.epam.counterparty.service.impl.TariffServiceImpl;
import com.epam.counterparty.service.impl.UserServiceImpl;
import com.epam.counterparty.util.ConfigurationManager;

import java.util.List;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.PathConstant.USER_INFO_PATH;

/**
 * Forwards to "user-info.jsp".
 *
 * @author Pavel
 * @since 17.06.2019
 */
public class ToUserInfoCommand implements Command {
    @Override
    public Router execute(RequestContent content) throws CommandException {
        SendingType sendingType = SendingType.FORWARD;
        String page;
        List<User> users;
        Quantity quantity;
        List<Profile> profileList;
        try {
            TariffService tariffService = new TariffServiceImpl();
            List<Tariff> tariffs = tariffService.findTariffs();
            QuantityService quantityService = new QuantityServiceImpl();
            UserService userService = new UserServiceImpl();
            ProfileService profileService = new ProfileServiceImpl();
            profileList = profileService.findProfiles();
            users = userService.findAllUsers(FIRST_PAGE, DEFAULT_ROWS_PER_PAGE);
            quantity = quantityService.findQuantityOfUsers(ALL_TARIFFS);
            for (User user : users) {
                for (Profile profile : profileList) {
                    if (user.getId() == profile.getUserId()) {
                        user.setProfile(profile);
                        break;
                    }
                }
            }
            long rowsWithDataCount = quantity.getRowsCount();
            long pagesCount = quantityService.setPageCount(quantity, DEFAULT_ROWS_PER_PAGE);
            content.putAttributeToSession(PAGES_COUNT, pagesCount);
            content.putAttributeToSession(ROWS_COUNT, rowsWithDataCount);
            content.putAttributeToSession(TARIFFS, tariffs);
            content.putAttributeToSession(USERS, users);
            page = ConfigurationManager.getProperty(USER_INFO_PATH);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage());
        }
        return new Router(page, sendingType);
    }
}