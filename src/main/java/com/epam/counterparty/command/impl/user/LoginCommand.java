package com.epam.counterparty.command.impl.user;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.entity.User;
import com.epam.counterparty.entity.UserType;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.PaymentService;
import com.epam.counterparty.service.ProfileService;
import com.epam.counterparty.service.UserService;
import com.epam.counterparty.service.impl.PaymentServiceImpl;
import com.epam.counterparty.service.impl.ProfileServiceImpl;
import com.epam.counterparty.service.impl.UserServiceImpl;
import com.epam.counterparty.util.MessageManager;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.MESSAGE_LOGIN_ERROR;

/**
 * The Login command.
 *
 * @author Pavel
 * @since 25.05.2019
 */
public class LoginCommand implements Command, UserBaseCommand {
    private static final byte ADMIN = 2;
    private static final byte USER = 1;

    @Override
    public Router execute(RequestContent content) {
        UserService userService = new UserServiceImpl();
        PaymentService paymentService = new PaymentServiceImpl();
        ProfileService profileService = new ProfileServiceImpl();
        String page = null;
        SendingType sendingType = SendingType.REDIRECT;
        User user;
        Payment payment;
        Profile profile;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            String email = content.getRequestParameters(EMAIL, 0);
            String pwd = content.getRequestParameters(PASSWORD, 0);
            user = userService.checkEmail(email, pwd);
            if (user.getRole() == USER) {
                profile = profileService.findUserProfileById(user.getId());
                payment = paymentService.findPayment(user.getId());
                content.putAttributeToSession(USER_ID, user.getId());
                content.putAttributeToSession(ROLE, UserType.USER.name());
                content.putAttributeToSession(CURRENT_USER_STATUS, user.isDeleted());
                content.putAttributeToSession(PROFILE, profile);
                content.putAttributeToSession(CURRENT_BALANCE, payment.getCurrentBalance());
                checkUserTariffEndPeriod(content, payment);
                page = DO_COMMAND + TO_USER_PAGE;
            } else if (user.getRole() == ADMIN) {
                content.putAttributeToSession(USER_ID, user.getId());
                content.putAttributeToSession(ROLE, UserType.ADMIN.name());
                content.putAttributeToSession(CURRENT_USER_STATUS, user.isDeleted());
                page = DO_COMMAND + TO_ADMIN_PAGE;
            }
        } catch (ServiceException e) {
            content.putAttributeToSession(ERROR_MESSAGE,
                    MessageManager.getProperty(MESSAGE_LOGIN_ERROR, locale));
            page = DO_COMMAND + TO_MAIN_PAGE;
        }
        return new Router(page, sendingType);
    }
}
