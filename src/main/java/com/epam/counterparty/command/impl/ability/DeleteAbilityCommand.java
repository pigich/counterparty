package com.epam.counterparty.command.impl.ability;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.AbilityService;
import com.epam.counterparty.service.impl.AbilityServiceImpl;
import com.epam.counterparty.util.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.MESSAGE_ABILITY_NOT_DELETED;

/**
 * Deletes ability from database and redirects to tariff-info page.
 *
 * @author Pavel
 * @since 20.06.2019
 */
public class DeleteAbilityCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public Router execute(RequestContent content) {
        String page;
        SendingType sendingType = SendingType.REDIRECT;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            AbilityService abilityService = new AbilityServiceImpl();
            long abilityId = Long.parseLong(content.getRequestParameters(CURRENT_ABILITY_ID, 0));
            Ability ability = new Ability();
            ability.setId(abilityId);
            abilityService.deleteAbilityById(abilityId);
            content.putAttributeToSession(TARIFFS_CHANGED, true);
            page = DO_COMMAND + TO_TARIFF_INFO;
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = DO_COMMAND + TO_TARIFF_INFO;
            content.putAttributeToSession(ERROR_MESSAGE,
                    MessageManager.getProperty(MESSAGE_ABILITY_NOT_DELETED, locale));
        }
        return new Router(page, sendingType);
    }
}
