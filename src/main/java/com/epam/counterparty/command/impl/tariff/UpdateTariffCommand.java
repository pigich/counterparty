package com.epam.counterparty.command.impl.tariff;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.TariffService;
import com.epam.counterparty.service.impl.TariffServiceImpl;
import com.epam.counterparty.util.MessageManager;

import static com.epam.counterparty.command.Attribute.*;

/**
 * Updates tariff in database and redirects to "tariff-info.jsp".
 *
 * @author Pavel
 * @since 20.06.2019
 */
public class UpdateTariffCommand implements Command, BaseTariffCommand {
    @Override
    public Router execute(RequestContent content) {
        TariffService tariffService = new TariffServiceImpl();
        String page;
        SendingType sendingType = SendingType.REDIRECT;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            Tariff tariff = getTariff(content);
            tariffService.updateTariff(tariff);
            content.putAttributeToSession(TARIFFS_CHANGED, true);
            page = DO_COMMAND + TO_TARIFF_INFO;
        } catch (ServiceException e) {
            page = DO_COMMAND + TO_TARIFF_INFO;
            content.putAttributeToSession(ERROR_MESSAGE,
                    MessageManager.getProperty(e.getMessage(), locale));
        }
        return new Router(page, sendingType);
    }
}
