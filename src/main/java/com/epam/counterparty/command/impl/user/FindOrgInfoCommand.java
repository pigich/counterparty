package com.epam.counterparty.command.impl.user;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Egr;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.UserService;
import com.epam.counterparty.service.impl.UserServiceImpl;
import com.epam.counterparty.util.DataValidator;
import com.epam.counterparty.util.MessageManager;

import static com.epam.counterparty.command.Attribute.*;

/**
 * The {@code FindOrgInfoCommand} is a {@code Command} implementation class which finds organization information.
 *
 * @author Pavel
 * @since 24.06.2019
 */
public class FindOrgInfoCommand implements Command {
    @Override
    public Router execute(RequestContent content) {
        UserService userService = new UserServiceImpl();
        DataValidator dataValidator = new DataValidator();
        String page;
        SendingType sendingType = SendingType.REDIRECT;
        Egr egr;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            String organizationUnp = content.getRequestParameters(SEARCHING_UNP, 0);
            dataValidator.validateUnp(organizationUnp);
            egr = userService.findOrgInfo(organizationUnp);
            content.putAttributeToSession(ORG_INFO, egr);
            page = DO_COMMAND + TO_USER_PAGE;
        } catch (ServiceException e) {
            content.putAttributeToSession(ORG_INFO, null);
            content.putAttributeToSession(ERROR_MESSAGE,
                    MessageManager.getProperty(e.getMessage(), locale));
            page = DO_COMMAND + TO_USER_PAGE;
        }
        return new Router(page, sendingType);
    }
}