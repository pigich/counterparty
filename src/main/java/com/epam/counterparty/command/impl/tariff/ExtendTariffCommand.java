package com.epam.counterparty.command.impl.tariff;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.PaymentService;
import com.epam.counterparty.service.TariffService;
import com.epam.counterparty.service.impl.PaymentServiceImpl;
import com.epam.counterparty.service.impl.TariffServiceImpl;
import com.epam.counterparty.util.MessageManager;

import java.util.List;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.MESSAGE_TARIFF_EXTENDED;
import static com.epam.counterparty.command.Message.MESSAGE_TARIFF_NOT_EXTENDED;

/**
 * Extends tariff in database and redirects to "tariff-info.jsp".
 *
 * @author Pavel
 * @since 07.07.2019
 */
public class ExtendTariffCommand implements Command {
    @SuppressWarnings("unchecked")
    @Override
    public Router execute(RequestContent content) {
        TariffService tariffService = new TariffServiceImpl();
        PaymentService paymentService = new PaymentServiceImpl();
        String page;
        SendingType sendingType = SendingType.REDIRECT;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            long userId = Long.parseLong(content.getSessionAttribute(USER_ID).toString());
            long tariffId = Long.parseLong(content.getRequestParameters(CURRENT_TARIFF_ID, 0));
            List<Tariff> tariffs = (List<Tariff>) content.getSessionAttribute(TARIFFS);
            Tariff tariff = new Tariff();
            for (Tariff tempTariff : tariffs) {
                if (tempTariff.getId() == tariffId) {
                    tariff = tempTariff;
                }
            }
            tariffService.extendTariff(tariff, userId);
            content.putAttributeToSession(CURRENT_TARIFF, tariff);
            Payment payment;
            payment = paymentService.findPayment(userId);
            content.putAttributeToSession(CURRENT_BALANCE, payment.getCurrentBalance());
            page = DO_COMMAND + TO_PROFILE;
            content.putAttributeToSession(OK_MESSAGE,
                    MessageManager.getProperty(MESSAGE_TARIFF_EXTENDED,locale));
        } catch (ServiceException e) {
            page = DO_COMMAND + TO_PROFILE;
            content.putAttributeToSession(ERROR_MESSAGE,
                    MessageManager.getProperty(MESSAGE_TARIFF_NOT_EXTENDED, locale));
        }
        return new Router(page, sendingType);
    }
}