package com.epam.counterparty.command.impl.user;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.util.ConfigurationManager;
import com.epam.counterparty.util.LangType;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.PathConstant.*;

/**
 * The {@code ChangeLangCommand} is a change language command.
 *
 * @author Pavel
 * @since 25.05.2019
 */
public class ChangeLangCommand implements Command {

    @Override
    public Router execute(RequestContent content) {
        String lang = (String) content.getSessionAttribute(LANG);
        if (lang == null) {
            content.putAttributeToSession(LANG, LangType.EN.getLocale());
        } else {
            if (lang.equals(LangType.EN.getLocale())) {
                content.putAttributeToSession(LANG, LangType.RU.getLocale());
            } else {
                content.putAttributeToSession(LANG, LangType.EN.getLocale());
            }
        }
        String curPage = (String) content.getSessionAttribute(CURRENT_JSP_PAGE);
        String page = null;
        switch (curPage) {
            case TARIFF_INFO:
                page = ConfigurationManager.getProperty(TARIFF_INFO_PATH);
                break;
            case PAYMENT_INFO:
                page = ConfigurationManager.getProperty(PAYMENTS_INFO_PATH);
                break;
            case USER_INFO:
                page = ConfigurationManager.getProperty(USER_INFO_PATH);
                break;
            case LOGIN:
                page = ConfigurationManager.getProperty(LOGIN_PAGE_PATH);
                break;
            case SIGN_UP:
                page = ConfigurationManager.getProperty(SIGN_UP_PAGE_PATH);
                break;
            case REGISTERED:
                page = ConfigurationManager.getProperty(REGISTERED_PAGE_PATH);
                break;
            case MAIN:
                page = ConfigurationManager.getProperty(MAIN_PAGE_PATH);
                break;
            case USER_PAGE:
                page = ConfigurationManager.getProperty(USER_PAGE_PATH);
                break;
            case ADMIN_PAGE:
                page = ConfigurationManager.getProperty(ADMIN_PAGE_PATH);
                break;
            case PROFILE:
                page = ConfigurationManager.getProperty(PROFILE_PAGE_PATH);
                break;
            case PROFILE_ADMIN:
                page = ConfigurationManager.getProperty(PROFILE_ADMIN_PAGE_PATH);
                break;
            default:
                break;
        }
        return new Router(page, SendingType.FORWARD);
    }
}