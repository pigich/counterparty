package com.epam.counterparty.command.impl.forwad;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.ProfileService;
import com.epam.counterparty.service.impl.ProfileServiceImpl;
import com.epam.counterparty.util.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.PathConstant.PROFILE_ADMIN_PAGE_PATH;

/**
 * Forwards to "profile-admin.jsp" page.
 *
 * @author Pavel
 * @since 01.07.2019
 */
public class ToAdminProfileCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public Router execute(RequestContent content) throws CommandException {
        SendingType sendingType = SendingType.FORWARD;
        String page;
        Profile profile;
        ProfileService profileService = new ProfileServiceImpl();
        try {
            long userId = Long.parseLong(content.getSessionAttribute(USER_ID).toString());
            profile = profileService.findUserProfileById(userId);
            content.putAttributeToSession(FULL_NAME, profile.getFullName());
            content.putAttributeToSession(ORGANIZATION_NAME, profile.getOrgName());
            content.putAttributeToSession(ORGANIZATION_UNP, profile.getOrgUnp());
            content.putAttributeToSession(PHONE, profile.getPhone());
            page = ConfigurationManager.getProperty(PROFILE_ADMIN_PAGE_PATH);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage());
        }
        return new Router(page, sendingType);
    }
}
