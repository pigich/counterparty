package com.epam.counterparty.command.impl.user;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.entity.UserType;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.ProfileService;
import com.epam.counterparty.service.impl.ProfileServiceImpl;
import com.epam.counterparty.util.MessageManager;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.*;

/**
 * @since 07.06.2019
 * @author Pavel
 */
public class ChangeProfileCommand implements Command {

    @Override
    public Router execute(RequestContent content) {
        ProfileService profileService = new ProfileServiceImpl();
        String page;
        SendingType sendingType = SendingType.FORWARD;
        Profile profile;
        String role = content.getSessionAttribute(ROLE).toString();
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            String fullName = content.getRequestParameters(FULL_NAME, 0);
            String phoneNumber = content.getRequestParameters(PHONE, 0);
            String organizationUnp = content.getRequestParameters(ORGANIZATION_UNP, 0);
            String organizationName = content.getRequestParameters(ORGANIZATION_NAME, 0);
            long userId = Long.parseLong(content.getSessionAttribute(USER_ID).toString());
            profile = profileService.changeUserProfile(fullName, phoneNumber, organizationUnp, organizationName, userId);
            if (role.equalsIgnoreCase(UserType.ADMIN.name())) {
                content.putAttributeToSession(CURRENT_USER_PROFILE, profile);
                content.putAttributeToSession(OK_MESSAGE,
                        MessageManager.getProperty(MESSAGE_PROFILE_CHANGED, locale));
                page = DO_COMMAND + TO_ADMIN_PROFILE;
            } else {
                content.putAttributeToSession(CURRENT_USER_PROFILE, profile);
                content.putAttributeToSession(OK_MESSAGE,
                        MessageManager.getProperty(MESSAGE_PROFILE_CHANGED, locale));
                page = DO_COMMAND + TO_PROFILE;
            }
        } catch (ServiceException e) {
            if (role.equalsIgnoreCase(UserType.ADMIN.name())) {
                content.putAttributeToSession(ERROR_MESSAGE,
                        MessageManager.getProperty(e.getMessage(), locale));
                page = DO_COMMAND + TO_ADMIN_PROFILE;
            } else {
                content.putAttributeToSession(ERROR_MESSAGE,
                        MessageManager.getProperty(e.getMessage(), locale));
                page = DO_COMMAND + TO_PROFILE;
            }
        }
        return new Router(page, sendingType);
    }
}