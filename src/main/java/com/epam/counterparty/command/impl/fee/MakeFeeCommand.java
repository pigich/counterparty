package com.epam.counterparty.command.impl.fee;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.PaymentService;
import com.epam.counterparty.service.impl.PaymentServiceImpl;
import com.epam.counterparty.util.MessageManager;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.MESSAGE_BALANCE_UPDATE_OK;

/**
 * The {@code MakeFeeCommand} class represent logic to put fee to database and redirect
 * to profile page.
 *
 * @author Pavel
 * @since 04.06.2019
 */
public class MakeFeeCommand implements Command {
    @Override
    public Router execute(RequestContent content)  {
        PaymentService paymentService = new PaymentServiceImpl();
        SendingType sendingType = SendingType.REDIRECT;
        String page;
        Payment payment;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            long userId = Long.parseLong(content.getSessionAttribute(USER_ID).toString());
            String stringAmount = content.getRequestParameters(FEE_AMOUNT, 0);
            paymentService.doPayment(userId, stringAmount);
            payment = paymentService.findPayment(userId);
            content.putAttributeToSession(CURRENT_BALANCE, payment.getCurrentBalance());
            page = DO_COMMAND + TO_PROFILE;
            content.putAttributeToSession(OK_MESSAGE,
                    MessageManager.getProperty(MESSAGE_BALANCE_UPDATE_OK, locale));
        } catch (ServiceException e) {
            page = DO_COMMAND + TO_PROFILE;
            content.putAttributeToSession(ERROR_MESSAGE,
                    MessageManager.getProperty(e.getMessage(), locale));
        }
        return new Router(page, sendingType);
    }
}
