package com.epam.counterparty.command.impl.user;


import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.UserType;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.util.MessageManager;
import com.epam.counterparty.service.UserService;
import com.epam.counterparty.service.impl.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.*;

/**
 * @since 06.06.2019
 * @author Pavel
 */
public class ChangePwdCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public Router execute(RequestContent content) {
        UserService userService = new UserServiceImpl();
        String page;
        SendingType sendingType = SendingType.REDIRECT;
        String oldPassword = content.getRequestParameters(OLD_PWD, 0);
        String role = content.getSessionAttribute(ROLE).toString();
        String newPassword = content.getRequestParameters(NEW_PWD, 0);
        String repeatNewPassword = content.getRequestParameters(REPEAT_PWD, 0);
        long userId = Long.parseLong(content.getSessionAttribute(USER_ID).toString());
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            userService.changePassword(oldPassword, newPassword, repeatNewPassword, userId);
            if (role.equalsIgnoreCase(UserType.ADMIN.name())) {
                page = DO_COMMAND + TO_ADMIN_PROFILE;
                content.putAttributeToSession(OK_MESSAGE,
                        MessageManager.getProperty(MESSAGE_CHANGE_PASSWORD_OK, locale));
            } else {
                page = DO_COMMAND + TO_PROFILE;
                content.putAttributeToSession(OK_MESSAGE,
                        MessageManager.getProperty(MESSAGE_CHANGE_PASSWORD_OK, locale));
            }
        } catch (ServiceException e) {
            if (role.equalsIgnoreCase(UserType.ADMIN.name())) {
                page = DO_COMMAND + TO_ADMIN_PROFILE;
                content.putAttributeToSession(ERROR_MESSAGE,
                        MessageManager.getProperty(e.getMessage(), locale));
            } else {
                page = DO_COMMAND + TO_PROFILE;
                content.putAttributeToSession(ERROR_MESSAGE,
                        MessageManager.getProperty(e.getMessage(), locale));
            }
        }
        return new Router(page, sendingType);
    }
}

