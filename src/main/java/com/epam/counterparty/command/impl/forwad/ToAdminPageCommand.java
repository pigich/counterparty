package com.epam.counterparty.command.impl.forwad;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.util.ConfigurationManager;

import static com.epam.counterparty.command.PathConstant.ADMIN_PAGE_PATH;

/**
 * Forwards to "admin-page.jsp".
 *
 * @author Pavel
 * @since 27.05.2019
 */
public class ToAdminPageCommand implements Command {

    @Override
    public Router execute(RequestContent content) {
        SendingType sendingType = SendingType.FORWARD;
        String page = ConfigurationManager.getProperty(ADMIN_PAGE_PATH);
        return new Router(page, sendingType);
    }
}
