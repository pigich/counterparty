package com.epam.counterparty.command.impl.forwad;

import com.epam.counterparty.command.*;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.AbilityService;
import com.epam.counterparty.service.TariffService;
import com.epam.counterparty.service.impl.AbilityServiceImpl;
import com.epam.counterparty.service.impl.TariffServiceImpl;
import com.epam.counterparty.util.ConfigurationManager;

import java.util.List;

import static com.epam.counterparty.command.Attribute.ABILITIES;
import static com.epam.counterparty.command.Attribute.TARIFFS;

/**
 * Forwards to "tariff-info.jsp".
 *
 * @author Pavel
 * @since 17.06.2019
 */
public class ToTariffInfoCommand implements Command {
    @Override
    public Router execute(RequestContent content) throws CommandException {
        SendingType sendingType = SendingType.FORWARD;
        String page;
        List<Tariff> tariffs;
        List<Ability> abilities;
        try {
            TariffService tariffService = new TariffServiceImpl();
            AbilityService abilityService = new AbilityServiceImpl();
            tariffs = tariffService.findTariffs();
            abilities = abilityService.findAbilities();
            content.putAttributeToSession(TARIFFS, tariffs);
            content.putAttributeToSession(ABILITIES, abilities);
            page = ConfigurationManager.getProperty(PathConstant.TARIFF_INFO_PATH);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage());
        }
        return new Router(page, sendingType);
    }
}