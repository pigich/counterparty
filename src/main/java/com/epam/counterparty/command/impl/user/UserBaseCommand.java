package com.epam.counterparty.command.impl.user;

import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.impl.forwad.ToUserPageCommand;
import com.epam.counterparty.entity.Payment;

import java.sql.Timestamp;

import static com.epam.counterparty.command.Attribute.USER_TARIFF_END_PERIOD_STATUS;

/**
 * The base interface for User commands.
 *
 * @author Pavel
 * @since 06.07.2019
 */
public interface UserBaseCommand {

    /**
     * Checks user tariff end period.
     * @see LoginCommand
     * @see ToUserPageCommand
     * @param content the content
     * @param payment the payment
     */
    default void checkUserTariffEndPeriod(RequestContent content, Payment payment) {
        Timestamp endTime = Timestamp.from(payment.getEndDate());
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        long currentTimeInSeconds = currentTime.getTime();
        long endTimeInSeconds = endTime.getTime();
        if ((endTimeInSeconds - currentTimeInSeconds) < 0) {
            content.putAttributeToSession(USER_TARIFF_END_PERIOD_STATUS, false);
        } else {
            content.putAttributeToSession(USER_TARIFF_END_PERIOD_STATUS, true);
        }
    }
}
