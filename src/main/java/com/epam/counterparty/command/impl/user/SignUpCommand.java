package com.epam.counterparty.command.impl.user;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.User;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.util.MessageManager;
import com.epam.counterparty.service.UserService;
import com.epam.counterparty.service.impl.UserServiceImpl;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.MESSAGE_BUSY_EMAIL;

/**
 * @since 25.05.2019
 * @author Pavel
 */
public class SignUpCommand implements Command {
    private static final String REGISTRATION_STATUS = "registration";
    private UserService userService = new UserServiceImpl();
    @Override
    public Router execute(RequestContent content) {
        String page;
        User user;
        SendingType sendingType = SendingType.REDIRECT;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            String email = content.getRequestParameters(EMAIL, 0);
            user = userService.signUp(email);
            if (user != null) {
                content.putAttributeToSession(EMAIL, email);
                content.putAttributeToSession(USER_ID, user.getId());
                content.putAttributeToSession(REGISTRATION_PAGE_STATUS, REGISTRATION_STATUS);
                page = DO_COMMAND + TO_REGISTERED_PAGE;
            } else {
                content.putAttributeToSession(ERROR_MESSAGE,
                        MessageManager.getProperty(MESSAGE_BUSY_EMAIL, locale));
                page = DO_COMMAND + TO_SIGN_UP;
            }
        } catch (ServiceException e) {
            content.putAttributeToSession(ERROR_MESSAGE,
                    MessageManager.getProperty(e.getMessage(),locale));
            page = DO_COMMAND + TO_SIGN_UP;
        }
        return new Router(page, sendingType);
    }
}
