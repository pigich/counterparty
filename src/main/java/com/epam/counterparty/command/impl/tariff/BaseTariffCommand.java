package com.epam.counterparty.command.impl.tariff;

import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.util.DataValidator;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.MESSAGE_AMOUNT_WRONG_FORMAT;

/**
 * Base interface for tariffs commands.
 *
 * @author Pavel
 * @since 20.06.2019
 */
interface BaseTariffCommand {
    /**
     * The constant SPLITTER.
     */
    String SPLITTER = ",";

    /**
     * Gets RequestContent object, creates and returns Tariff object.
     *
     * @param content the content
     * @return the Tariff object
     * @throws ServiceException the service exception
     */
    @NotNull
    default Tariff getTariff(RequestContent content) throws ServiceException {
        Tariff tariff = new Tariff();
        BigDecimal price;
        short period;
        DataValidator dataValidator = new DataValidator();
        long tariffId = Long.parseLong(content.getRequestParameters(TARIFF_ID, 0));
        String nameEn = content.getRequestParameters(TARIFF_NAME_EN, 0);
        String nameRu = content.getRequestParameters(TARIFF_NAME_RU, 0);
        String periodString = content.getRequestParameters(PERIOD, 0);
        String priceString = content.getRequestParameters(PRICE, 0);
        try {
            double amountFromString = Double.parseDouble(priceString);
            price = BigDecimal.valueOf(amountFromString);
        } catch (NumberFormatException e) {
            throw new ServiceException(MESSAGE_AMOUNT_WRONG_FORMAT);
        }
        dataValidator.validateTariffPrice(price);
        dataValidator.validateTariffAbilityName(nameEn);
        dataValidator.validateTariffAbilityName(nameRu);
        dataValidator.validateTariffPeriod(periodString);
        period = Short.parseShort(periodString);
        String abilityIds = content.getRequestParameters(ABILITY_ID_LIST, 0);
        tariff.setId(tariffId);
        tariff.setTariffNameEn(nameEn);
        tariff.setTariffNameRu(nameRu);
        tariff.setPrice(price);
        tariff.setPeriod(period);
        if (!abilityIds.isEmpty()) {
            List<String> abilityIdsList = new ArrayList<>(Arrays.asList(abilityIds.split(SPLITTER)));
            List<Ability> abilities = new ArrayList<>();
            for (String abilityIdString : abilityIdsList) {
                int id = Integer.parseInt(abilityIdString);
                Ability ability = new Ability(id);
                abilities.add(ability);
            }
            tariff.setAbilityList(abilities);
        }
        return tariff;
    }
}
