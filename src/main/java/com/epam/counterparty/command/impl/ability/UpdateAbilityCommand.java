package com.epam.counterparty.command.impl.ability;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.AbilityService;
import com.epam.counterparty.service.impl.AbilityServiceImpl;
import com.epam.counterparty.util.MessageManager;

import static com.epam.counterparty.command.Attribute.*;

/**
 * Updates ability in database and redirects to tariff-info page.
 *
 * @author Pavel
 * @since 20.06.2019
 */
public class UpdateAbilityCommand implements Command, BaseAbilityCommand {

    @Override
    public Router execute(RequestContent content) {
        AbilityService abilityService = new AbilityServiceImpl();
        String page;
        SendingType sendingType = SendingType.REDIRECT;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            Ability ability = getAbility(content);
            abilityService.updateAbility(ability);
            content.putAttributeToSession(TARIFFS_CHANGED, true);
            page = DO_COMMAND + TO_TARIFF_INFO;
        } catch (ServiceException e) {
            page = DO_COMMAND + TO_TARIFF_INFO;
            content.putAttributeToSession(ERROR_MESSAGE,
                    MessageManager.getProperty(e.getMessage(), locale));
        }
        return new Router(page, sendingType);
    }
}
