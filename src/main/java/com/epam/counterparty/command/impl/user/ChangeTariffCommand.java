package com.epam.counterparty.command.impl.user;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.PaymentService;
import com.epam.counterparty.service.TariffService;
import com.epam.counterparty.service.impl.PaymentServiceImpl;
import com.epam.counterparty.service.impl.TariffServiceImpl;
import com.epam.counterparty.util.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.List;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.*;

/**
 * @since 11.06.2019
 * @author Pavel
 */
public class ChangeTariffCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger();
    @SuppressWarnings("unchecked")
    @Override
    public Router execute(RequestContent content) {
        TariffService tariffService = new TariffServiceImpl();
        PaymentService paymentService = new PaymentServiceImpl();
        String page;
        SendingType sendingType = SendingType.FORWARD;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            double amountFromString = Double.parseDouble(content.getSessionAttribute(CURRENT_BALANCE).toString());
            BigDecimal amount = BigDecimal.valueOf(amountFromString);
            long userId = Long.parseLong(content.getSessionAttribute(USER_ID).toString());
            String tariffName = content.getRequestParameters(TARIFF_NAME, 0);
            List<Tariff> tariffs = (List<Tariff>) content.getSessionAttribute(TARIFFS);
            Tariff tariff = new Tariff();
            for (Tariff tempTariff : tariffs) {
                if (tempTariff.getTariffNameRu().equals(tariffName)
                        || tempTariff.getTariffNameEn().equals(tariffName)) {
                    tariff = tempTariff;
                }
            }
            if (amount.compareTo(tariff.getPrice()) >= 0) {
                tariffService.changeTariff(tariff, userId);
                content.putAttributeToSession(CURRENT_TARIFF, tariff);
                Payment payment;
                payment = paymentService.findPayment(userId);
                content.putAttributeToSession(CURRENT_BALANCE, payment.getCurrentBalance());
                content.putAttributeToSession(OK_MESSAGE,
                        MessageManager.getProperty(MESSAGE_TARIFF_CHANGED,locale));
                page = DO_COMMAND + TO_PROFILE;
            } else {
                content.putAttributeToSession(ERROR_MESSAGE,
                        MessageManager.getProperty(MESSAGE_NOT_ENOUGH_AMOUNT_ERROR, locale));
                page = DO_COMMAND + TO_PROFILE;
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
            page = DO_COMMAND + TO_ERROR_PAGE;
        }
        return new Router(page, sendingType);
    }
}