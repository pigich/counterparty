package com.epam.counterparty.command.impl.user;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.util.ConfigurationManager;

import static com.epam.counterparty.command.PathConstant.INDEX_PAGE_PATH;

/**
 * The Logout command.
 *
 * @author Pavel
 * @since 25.05.2019
 */
public class LogoutCommand implements Command {
    @Override
    public Router execute(RequestContent content) {
        String page = ConfigurationManager.getProperty(INDEX_PAGE_PATH);
        content.invalidateSession();
        return new Router(page, SendingType.REDIRECT);
    }
}
