package com.epam.counterparty.command.impl.tariff;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.TariffService;
import com.epam.counterparty.service.impl.TariffServiceImpl;
import com.epam.counterparty.util.MessageManager;

import java.util.List;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.MESSAGE_TARIFF_NOT_EXTENDED;

/**
 * Shows working(not archival) tariffs. Uses to initialize tariffs on "main.jsp".
 *
 * @author Pavel
 * @since 10.07.2019
 */
public class ShowTariffsCommand implements Command {
    @Override
    public Router execute(RequestContent content) throws CommandException {
        String page;
        SendingType sendingType = SendingType.FORWARD;
        TariffService tariffService = new TariffServiceImpl();
        List<Tariff> tariffs;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {

            tariffs = tariffService.createWorkingTariffsList();
            if (tariffs != null) {
                content.putAttributeToSession(WORKING_TARIFFS, tariffs);
                page = DO_COMMAND + TO_MAIN_PAGE;
            } else {
                page = DO_COMMAND + TO_MAIN_PAGE;
            }
        } catch (ServiceException e) {
            throw new CommandException(MessageManager.getProperty(MESSAGE_TARIFF_NOT_EXTENDED, locale), e);
        }
        return new Router(page, sendingType);
    }
}
