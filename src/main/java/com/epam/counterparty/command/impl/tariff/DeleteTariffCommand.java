package com.epam.counterparty.command.impl.tariff;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.TariffService;
import com.epam.counterparty.service.impl.TariffServiceImpl;
import com.epam.counterparty.util.MessageManager;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.Message.MESSAGE_TARIFF_NOT_ARCHIVED;

/**
 * Marks tariff archival in database and redirects to "tariff-info.jsp".
 *
 * @author Pavel
 * @since 18.06.2019
 */
public class DeleteTariffCommand implements Command {
    @Override
    public Router execute(RequestContent content) {
        TariffService tariffService = new TariffServiceImpl();
        String page;
        SendingType sendingType = SendingType.REDIRECT;
        String locale = String.valueOf(content.getSessionAttribute(LANG));
        try {
            long tariffId = Long.parseLong(content.getRequestParameters(CURRENT_TARIFF_ID, 0));
            Tariff tariff = new Tariff();
            tariff.setId(tariffId);
            tariffService.setTariffArchival(tariffId);
            content.putAttributeToSession(TARIFFS_CHANGED, true);
            page = DO_COMMAND + TO_TARIFF_INFO;
        } catch (ServiceException e) {
            page = DO_COMMAND + TO_TARIFF_INFO;
            content.putAttributeToSession(ERROR_MESSAGE,
                    MessageManager.getProperty(MESSAGE_TARIFF_NOT_ARCHIVED, locale));
        }
        return new Router(page, sendingType);
    }
}
