package com.epam.counterparty.command.impl.forwad;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.command.impl.user.UserBaseCommand;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.entity.User;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.PaymentService;
import com.epam.counterparty.service.ProfileService;
import com.epam.counterparty.service.UserService;
import com.epam.counterparty.service.impl.PaymentServiceImpl;
import com.epam.counterparty.service.impl.ProfileServiceImpl;
import com.epam.counterparty.service.impl.UserServiceImpl;
import com.epam.counterparty.util.ConfigurationManager;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.PathConstant.USER_CABINET_PATH;

/**
 * Forwards to "user-page.jsp".
 *
 * @author Pavel
 * @since 27.05.2019
 */
public class ToUserPageCommand implements Command, UserBaseCommand {
    @Override
    public Router execute(RequestContent content) throws CommandException {
        SendingType sendingType = SendingType.FORWARD;
        String page;
        Profile profile;
        Payment payment;
        User user;
        UserService userService = new UserServiceImpl();
        PaymentService paymentService = new PaymentServiceImpl();
        ProfileService profileService = new ProfileServiceImpl();
        try {
            long userId = Long.parseLong(content.getSessionAttribute(USER_ID).toString());
            user = userService.findUserById(userId);
            profile = profileService.findUserProfileById(userId);
            payment = paymentService.findPayment(userId);
            boolean status = user.isDeleted();
            content.putAttributeToSession(CURRENT_USER_STATUS, status);
            content.putAttributeToSession(PROFILE, profile);
            page = ConfigurationManager.getProperty(USER_CABINET_PATH);
            checkUserTariffEndPeriod(content, payment);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage());
        }
        return new Router(page, sendingType);
    }
}


