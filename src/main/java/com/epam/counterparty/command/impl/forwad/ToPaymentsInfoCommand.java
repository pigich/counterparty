package com.epam.counterparty.command.impl.forwad;

import com.epam.counterparty.command.Command;
import com.epam.counterparty.command.RequestContent;
import com.epam.counterparty.command.Router;
import com.epam.counterparty.command.SendingType;
import com.epam.counterparty.entity.Fee;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.exception.CommandException;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.FeeService;
import com.epam.counterparty.service.PaymentService;
import com.epam.counterparty.service.impl.FeeServiceImpl;
import com.epam.counterparty.service.impl.PaymentServiceImpl;
import com.epam.counterparty.util.ConfigurationManager;

import java.util.List;

import static com.epam.counterparty.command.Attribute.*;
import static com.epam.counterparty.command.PathConstant.PAYMENTS_INFO_PATH;

/**
 * Forwards to "payments-info.jsp".
 *
 * @author Pavel
 * @since 17.06.2019
 */
public class ToPaymentsInfoCommand implements Command {
    @Override
    public Router execute(RequestContent content) throws CommandException {
        SendingType sendingType = SendingType.FORWARD;
        String page;
        List<Payment> payments;
        List<Fee> fees;
        try {
            PaymentService paymentService = new PaymentServiceImpl();
            FeeService feeService = new FeeServiceImpl();
            payments = paymentService.findPayments();
            fees = feeService.findFees(FIRST_PAGE, DEFAULT_ROWS_PER_PAGE, DEFAULT_ORDER_COLUMN, DEFAULT_SORT_TYPE);
            content.putAttributeToSession(PAYMENTS, payments);
            content.putAttributeToSession(FEES, fees);
            page = ConfigurationManager.getProperty(PAYMENTS_INFO_PATH);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage());
        }
        return new Router(page, sendingType);
    }
}