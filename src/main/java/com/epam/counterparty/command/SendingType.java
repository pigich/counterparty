package com.epam.counterparty.command;

/**
 * The {@code SendingType} enum used in {@link Router} to indicate sending type.
 *
 * @author Pavel
 * @since 26.05.2019
 */
public enum SendingType {
    /**
     * Forward sending type.
     */
    FORWARD,
    /**
     * Redirect sending type.
     */
    REDIRECT
}
