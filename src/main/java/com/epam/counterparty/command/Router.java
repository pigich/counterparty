package com.epam.counterparty.command;

/**
 * The {@code Router} class defines "where" and "how" to transfer
 *
 * @author Pavel
 * @since 25.05.2019
 */
public class Router {
    private String path;
    private SendingType sendingType;

    /**
     * Instantiates a new Router.
     *
     * @param path        the path
     * @param sendingType the sending type
     */
    public Router(String path, SendingType sendingType) {
        this.path = path;
        this.sendingType = sendingType;
    }

    /**
     * Gets path.
     *
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * Gets sending type.
     *
     * @return the sending type
     */
    public SendingType getSendingType() {
        return sendingType;
    }
}
