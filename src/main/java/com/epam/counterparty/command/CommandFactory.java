package com.epam.counterparty.command;

import com.epam.counterparty.util.MessageManager;

import java.util.Optional;

import static com.epam.counterparty.command.Attribute.COMMAND;
import static com.epam.counterparty.command.Attribute.ERROR_MESSAGE;
import static com.epam.counterparty.command.Message.MESSAGE_WRONG_ACTION;

/**
 * The Command factory.
 *
 * @author Pavel
 * @since 25.05.2019
 */
public class CommandFactory {
    private CommandFactory() {
    }

    /**
     * Defines command name from RequestContent object {@link RequestContent}
     *
     * @param content the RequestContent object from request
     * @return the optional of command
     */
    public static Optional<Command> defineCommand(RequestContent content) {
        Command command = null;
        String cmdName = content.getRequestParameters(COMMAND, 0);
        if (cmdName == null || cmdName.isEmpty()) {
            return Optional.empty();
        }
        try {
            CommandType commandType = CommandType.valueOf(cmdName.toUpperCase());
            command = commandType.getCommand();
        } catch (IllegalArgumentException e) {
            content.putAttributeToRequest(ERROR_MESSAGE, cmdName
                    + MessageManager.getProperty(MESSAGE_WRONG_ACTION));
        }
        return Optional.of(command);
    }
}
