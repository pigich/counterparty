package com.epam.counterparty.command;

/**
 * The {@code PathConstant} class represents constant paths to jsp pages
 *
 * @author Pavel
 * @since 24.06.2019
 */
public class PathConstant {
    public static final String USER_CABINET_PATH = "path.page.user-page";
    public static final String ERROR_PAGE_PATH = "path.page.error";
    public static final String MAIN_PAGE_PATH = "path.page.main";
    public static final String INDEX_PAGE_PATH = "path.page.index";
    public static final String SIGN_UP_PAGE_PATH = "path.page.sign-up";
    public static final String LOGIN_PAGE_PATH = "path.page.login";
    public static final String PROFILE_PAGE_PATH = "path.page.profile-page";
    public static final String PROFILE_ADMIN_PAGE_PATH = "path.page.profile-admin";
    public static final String USER_PAGE_PATH = "path.page.user-page";
    public static final String ADMIN_PAGE_PATH = "path.page.admin-page";
    public static final String REGISTERED_PAGE_PATH = "path.page.registered";
    public static final String TARIFF_INFO_PATH = "path.page.tariff-info";
    public static final String PAYMENTS_INFO_PATH = "path.page.payment-info";
    public static final String USER_INFO_PATH = "path.page.user-info";

    private PathConstant() {
    }
}
