package com.epam.counterparty.command;

import com.epam.counterparty.exception.CommandException;

/**
 * The Command interface.
 *
 * @author Pavel
 * @since 26.05.2019
 */
public interface Command {
    /**
     * Executes command.
     *
     * @param content the content
     * @return the router
     * @throws CommandException the command exception
     */
    Router execute(RequestContent content) throws CommandException;
}
