package com.epam.counterparty.listener;

import com.epam.counterparty.connection.ConnectionPool;
import com.epam.counterparty.exception.ConnectionPoolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import static com.epam.counterparty.command.Message.MESSAGE_DESTROY_CONNECTION_EXCEPTION;

/**
 * The {@code ContextListenerImpl} class creates connection pool when context initializes.
 * Destroys connection pool when context destroying.
 *
 * @author Pavel
 * @since 25.05.2019
 */
@WebListener
public class ContextListenerImpl implements ServletContextListener {
    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ConnectionPool.getInstance();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            ConnectionPool.getInstance().destroyConnections();
        } catch (ConnectionPoolException e) {
            LOGGER.error(MESSAGE_DESTROY_CONNECTION_EXCEPTION, e);
        }
    }
}
