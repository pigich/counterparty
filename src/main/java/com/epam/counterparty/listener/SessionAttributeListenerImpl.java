package com.epam.counterparty.listener;

import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.TariffService;
import com.epam.counterparty.service.impl.TariffServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.List;

import static com.epam.counterparty.command.Attribute.TARIFFS_CHANGED;
import static com.epam.counterparty.command.Attribute.WORKING_TARIFFS;
import static com.epam.counterparty.command.Message.MESSAGE_ERROR_WHILE_UPDATING_WORKING_TARIFFS_ATTRIBUTE;

/**
 * Session attribute listener.
 * Listens tariff attribute changes and updates {@code working_tariffs} attribute
 *
 * @author Pavel
 * @since 31.05.2019
 */
@WebListener
public class SessionAttributeListenerImpl implements HttpSessionAttributeListener {
    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {
        if (event.getName().equals(TARIFFS_CHANGED)) {
            TariffService tariffService = new TariffServiceImpl();
            List<Tariff> tariffs;
            try {
                tariffs = tariffService.createWorkingTariffsList();
                event.getSession().getServletContext().setAttribute(WORKING_TARIFFS, tariffs);
            } catch (ServiceException e) {
                LOGGER.error(MESSAGE_ERROR_WHILE_UPDATING_WORKING_TARIFFS_ATTRIBUTE, e);
            }
        }
    }
}
