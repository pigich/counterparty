package com.epam.counterparty.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.epam.counterparty.command.Message.MESSAGE_EXCEPTION_READ_PROPERTIES_FILE;

/**
 * The {@code DatabaseProperty} class for reading {@code Properties} files with Database
 * configurations.
 *
 * @author Pavel
 * @since 17.05.2019
 */
class DatabaseProperty {
    private static final String DB_PROPERTIES = "/config/db.properties";
    private static final Logger LOGGER = LogManager.getLogger();
    private Properties properties = new Properties();

    /**
     * Sets property.
     */
    void setProperty() {
        properties = new Properties();
        try (InputStream in = DatabaseProperty.class.getResourceAsStream(DB_PROPERTIES)) {
            properties.load(in);
        } catch (IOException e) {
            LOGGER.fatal(MESSAGE_EXCEPTION_READ_PROPERTIES_FILE, e);
            throw new RuntimeException(MESSAGE_EXCEPTION_READ_PROPERTIES_FILE, e);
        }
    }

    /**
     * Gets properties.
     *
     * @return the properties
     */
    Properties getProperties() {
        return properties;
    }
}
