package com.epam.counterparty.connection;

import com.epam.counterparty.exception.ConnectionPoolException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import static com.epam.counterparty.command.Message.*;

/**
 * The {@code ConnectionPool} represents a persistent set of connections and methods to work
 * with them
 *
 * @author Pavel
 * @since 17.05.2019
 */
public class ConnectionPool {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final int POOL_SIZE;
    private static final Properties JDBC_PARAMETERS;
    private static final String URL;
    private static final AtomicInteger connectionsCurrentSize = new AtomicInteger(0);
    private static final AtomicInteger badConnectionsCurrentSize = new AtomicInteger(0);
    private static final String URL_PROPERTY = "url";
    private static final String POOL_SIZE_PROPERTY = "poolSize";
    private static ConnectionPool instance;
    private static ReentrantLock lock = new ReentrantLock();
    private static AtomicBoolean isVolatile = new AtomicBoolean(false);

    static {
        DatabaseProperty properties = new DatabaseProperty();
        properties.setProperty();
        JDBC_PARAMETERS = properties.getProperties();
        POOL_SIZE = Integer.valueOf(JDBC_PARAMETERS.getProperty(POOL_SIZE_PROPERTY));
        URL = JDBC_PARAMETERS.getProperty(URL_PROPERTY);
    }

    private BlockingQueue<ProxyConnection> availableConnections;
    private ArrayDeque<ProxyConnection> usedConnections = new ArrayDeque<>();


    private ConnectionPool() {
        if (instance != null) {
            throw new RuntimeException(MESSAGE_CONNECTION_POOL_ALREADY_EXISTS);
        }
        try {
            init();
        } catch (ConnectionPoolException e) {
            LOGGER.error(MESSAGE_ERROR_CREATING_CONNECTION, e);
        }
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static ConnectionPool getInstance() {
        if (!isVolatile.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    isVolatile.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Checks bad connection count.
     */
    private static void checkBadConnectionCount() {
        if (badConnectionsCurrentSize.intValue() == POOL_SIZE) {
            LOGGER.fatal(MESSAGE_NO_AVAILABLE_CONNECTIONS_CREATED);
            throw new ExceptionInInitializerError(MESSAGE_NO_AVAILABLE_CONNECTIONS_CREATED);
        }
    }

    /**
     * Inits connection pool.
     *
     * @throws ConnectionPoolException the connection pool exception
     */
    private void init() throws ConnectionPoolException {
        Connection connection;
        ProxyConnection proxy;
        availableConnections = new LinkedBlockingDeque<>(POOL_SIZE);
        try {
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_ERROR_INIT_JDBC_DRIVER, e);
            throw new ExceptionInInitializerError(MESSAGE_ERROR_INIT_JDBC_DRIVER);
        }
        for (int i = 0; i < POOL_SIZE; i++) {
            try {
                connection = DriverManager.getConnection(URL, JDBC_PARAMETERS);
                proxy = new ProxyConnection(connection);
                if (availableConnections.offer(proxy)) {
                    connectionsCurrentSize.getAndIncrement();
                } else {
                    badConnectionsCurrentSize.getAndIncrement();
                }
            } catch (SQLException e) {
                LOGGER.error(MESSAGE_ERROR_CREATING_CONNECTION, e);
                throw new ConnectionPoolException(MESSAGE_EXCEPTION_CREATING_AVAILABLE_POOL_OF_CONNECTIONS, e);
            }
        }
        checkBadConnectionCount();
    }

    /**
     * Gets connection.
     *
     * @return the connection
     */
    public ProxyConnection getConnection() {
        ProxyConnection con = null;
        try {
            con = availableConnections.take();
            lock.lock();
            usedConnections.offer(con);
            lock.unlock();
        } catch (InterruptedException e) {
            LOGGER.error(MESSAGE_EXCEPTION_IN_TAKE_CONNECTION, e);
            Thread.currentThread().interrupt();
        }
        return con;
    }

    /**
     * Releases used connection and puts them to available queue.
     *
     * @param connection the connection
     */
    public void releaseConnection(ProxyConnection connection) {
        lock.lock();
        try {
            if (!usedConnections.remove(connection)) {
                throw new IllegalArgumentException(MESSAGE_CONNECTION_NOT_CONTROLLED + connection);
            }
            if (availableConnections.offer(connection)) {
                connectionsCurrentSize.getAndIncrement();
            } else {
                badConnectionsCurrentSize.getAndIncrement();
            }
            checkBadConnectionCount();
        } finally {
            lock.unlock();
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    /**
     * Put connection to available queue.
     *
     * @param proxyConnection the proxy connection
     */
    void putConnection(ProxyConnection proxyConnection) {
        try {
            availableConnections.put(proxyConnection);
        } catch (InterruptedException e) {
            LOGGER.error(MESSAGE_EXCEPTION_PUT_CONNECTION_TO_POOL, e);
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Destroys connections.
     *
     * @throws ConnectionPoolException the connection pool exception
     */
    public void destroyConnections() throws ConnectionPoolException {
        try {
            for (int i = 0; i < POOL_SIZE; i++) {
                ProxyConnection proxyConnection = availableConnections.take();
                proxyConnection.closeConnection();
            }
            Enumeration<Driver> drivers = DriverManager.getDrivers();
            while (drivers.hasMoreElements()) {
                Driver driver = drivers.nextElement();
                DriverManager.deregisterDriver(driver);
            }
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_DESTROY_CONNECTION_POOL_EXCEPTION, e);
            throw new ConnectionPoolException(MESSAGE_DESTROY_CONNECTION_POOL_EXCEPTION, e);
        } catch (InterruptedException e) {
            LOGGER.error(MESSAGE_DESTROY_CONNECTION_POOL_EXCEPTION, e);
            Thread.currentThread().interrupt();
        }
    }
}
