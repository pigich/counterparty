package com.epam.counterparty.tag;

import com.epam.counterparty.util.MessageManager;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

import static com.epam.counterparty.command.Attribute.USER;
import static com.epam.counterparty.command.Message.MESSAGE_TAG_GREETING;
import static com.epam.counterparty.command.Message.MESSAGE_TAG_PROPOSITION;

/**
 * The {@code GreetingTag} is a class for representing JSP tag.
 *
 * @author Pavel
 * @since 05.06.2019
 */
public class GreetingTag extends TagSupport {
    private String role;
    private String lang;
    private String userName;
    private boolean greeting;

    @Override
    public int doStartTag() throws JspException {
        if (!greeting) {
            try {
                String message = null;
                if (role.equalsIgnoreCase(USER)) {
                    if (!userName.isEmpty()) {
                        message=  MessageManager.getProperty(MESSAGE_TAG_GREETING,lang) + userName;
                    } else {
                        message=  MessageManager.getProperty(MESSAGE_TAG_PROPOSITION);
                    }
                }
                pageContext.getOut().write("<hr/>" + message + "<hr/>");
            } catch (IOException e) {
                throw new JspException(e.getMessage());
            }
        }

        return SKIP_BODY;
    }
    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    /**
     * Sets greeting.
     *
     * @param greeting the greeting
     */
    public void setGreeting(boolean greeting) {
        this.greeting = greeting;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * Sets user name.
     *
     * @param userName the user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Sets lang.
     *
     * @param lang the lang
     */
    public void setLang(String lang) {
        this.lang = lang;
    }
}
