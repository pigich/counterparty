package com.epam.counterparty.service.impl;

import com.epam.counterparty.BaseTest;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.AbilityService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @since 21.07.2019
 * @author Pavel
 */
public class AbilityServiceImplTest extends BaseTest {
    private AbilityService abilityService = new AbilityServiceImpl();
    private Ability ability;
    private Ability existingAbility;

    public AbilityServiceImplTest(String name) {
        super(name);
    }

    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        ability = new Ability();
        ability.setAbilityNameRu("Тестовая функция");
        ability.setAbilityNameEn("Test ability");
        existingAbility = new Ability(7,"Bunkruptcy information","Информация о банкротстве");
    }

    @AfterClass
    public void tearDown() throws Exception {
        super.tearDown();
    }
    @Test
    public void testFindAbilities() throws Exception {
        super.restoreDb();
        int expected = 5;
        List<Ability> abilitiesFromDb = abilityService.findAbilities();
        int actual = abilitiesFromDb.size();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testUpdateAbility() throws ServiceException {
        ability.setTariffAbilityId(19);
        ability.setId(1);
        Ability expected = ability;
        abilityService.updateAbility(ability);
        Ability actual = abilityService.findAbilityByName(ability);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testAddAbility() throws Exception {
        Ability expected = ability;
        abilityService.addAbility(ability);
        Ability abilityFromDb = abilityService.findAbilityByName(ability);
        Ability actual = new Ability();
        actual.setAbilityNameEn(abilityFromDb.getAbilityNameEn());
        actual.setAbilityNameRu(abilityFromDb.getAbilityNameRu());
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindAbilityByName() throws ServiceException {
        Ability expected =existingAbility;
        Ability actual = abilityService.findAbilityByName(expected);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testDeleteAbilityById() throws Exception {
        abilityService.deleteAbilityById(7);
        Ability actual = abilityService.findAbilityByName(existingAbility);
        Assert.assertNull(actual);
    }

    @Test
    public void testFindAbilityByTariffId() throws Exception {
        super.restoreDb();
        List<Ability> expected = new ArrayList<>();
        expected.add(existingAbility);
        expected.add(new Ability(8,"Monitoring with notification","Мониторинг с уведомлением"));
        List<Ability> actual= abilityService.findAbilityByTariffId(40);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindAbilitiesByTariffState() throws Exception {
        super.restoreDb();
        int expected = 1;
        List<Ability> abilitiesFromDb = abilityService.findAbilitiesByTariffState(true);
        int actual = abilitiesFromDb.size();
        Assert.assertEquals(actual, expected);
    }
}