package com.epam.counterparty.service.impl;

import com.epam.counterparty.BaseTest;
import com.epam.counterparty.entity.Fee;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.FeeService;
import com.epam.counterparty.service.PaymentService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author Pavel
 * @since 21.07.2019
 */
public class PaymentServiceImplTest extends BaseTest {
    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private PaymentService paymentService = new PaymentServiceImpl();
    private FeeService feeService = new FeeServiceImpl();
    private Payment payment;
    private Payment existingPayment;
    private Fee fee;

    public PaymentServiceImplTest(String name) {
        super(name);
    }

    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        payment = new Payment();
        payment.setTariffId(1L);
        payment.setCurrentBalance(BigDecimal.valueOf(10));
        payment.setUserId(59);
        fee = new Fee();
        fee.setAmount(BigDecimal.valueOf(11));
        fee.setUserId(59);
        DateTimeFormatter f = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
        String startDateString = "2019-07-08 20:41:25";
        LocalDateTime startLdt = LocalDateTime.parse(startDateString, f);
        String endDateString = "2019-07-06 13:05:18";
        LocalDateTime endLdt = LocalDateTime.parse(endDateString, f);
        ZoneId zoneId = ZoneId.of("UTC");
        ZonedDateTime startZoneDateTime = startLdt.atZone(zoneId);
        ZonedDateTime endZoneDateTime = endLdt.atZone(zoneId);
        Instant startDate = startZoneDateTime.toInstant();
        Instant endDate = endZoneDateTime.toInstant();
        existingPayment = new Payment(55, 40, startDate, endDate, BigDecimal.valueOf(98));
    }

    @AfterMethod
    public void afterMethod() throws Exception {
        super.restoreDb();
    }

    @AfterClass
    public void tearDown() throws Exception {
        super.tearDown();
    }
    @Test
    public void testDoPayment() throws ServiceException {
        Payment expected = payment;
        paymentService.doPayment(59, "11");
        Payment actual = paymentService.findPayment(59);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testDoPaymentFeeCheck() throws ServiceException {
        Fee expected = fee;
        paymentService.doPayment(59, "11");
        List<Fee> feeList = feeService.findFees(1,20, 1, "ASC");
        Fee feeFromDb = new Fee();
        for (Fee temp : feeList) {
            if (temp.getUserId() == 59 && temp.getAmount().equals(BigDecimal.valueOf(11))) {
                feeFromDb = temp;
            } else {
                feeFromDb = null;
            }
        }
        Fee actual = new Fee();
        actual.setUserId(feeFromDb.getUserId());
        actual.setAmount(feeFromDb.getAmount());

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testDoPaymentTransaction() throws ServiceException {
        Payment expected = payment;
        paymentService.doPaymentTransaction(59, BigDecimal.valueOf(11));
        Payment actual = paymentService.findPayment(59);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindPayment() throws ServiceException {
        Payment expected = existingPayment;
        Payment actual = paymentService.findPayment(55);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindPayments() throws ServiceException {
        int expected = 4;
        List<Payment> payments = paymentService.findPayments();
        int actual = payments.size();
        Assert.assertEquals(actual, expected);
    }

}