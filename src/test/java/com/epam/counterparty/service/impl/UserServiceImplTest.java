package com.epam.counterparty.service.impl;

import com.epam.counterparty.BaseTest;
import com.epam.counterparty.entity.User;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.PasswordService;
import com.epam.counterparty.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;

/**
 * @since 18.07.2019
 * @author Pavel
 */
public class UserServiceImplTest extends BaseTest {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String CORRECT_PASSWORD = "1";
    private static final String CORRECT_EMAIL = "qwe@mail.ru";
    private static final String WRONG_EMAIL = "123@super.mail.us";
    private static final String WRONG_PASSWORD = "3333";
    private static final String NEW_EMAIL = "333@road-to-nowhere.us";
    private User user;
    private UserService userService = new UserServiceImpl();


    public UserServiceImplTest(String name) {
        super(name);
    }

    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {
        user = User.builder()
                .id(50L)
                .password("67b176705b46206614219f47a05aee7ae6a3edbe850bbbe214c536b989aea4d2")
                .role(Byte.valueOf("2"))
                .email("qwe@mail.ru")
                .deleted(false)
                .build();
    }

    @AfterMethod
    public void afterMethod() throws Exception {
        userService.updateUser(user);
        user = null;
    }
        @AfterClass
    public void tearDown() throws Exception {
            super.tearDown();
    }

    @Test
    public void testSignUp() throws ServiceException {
        User actual = userService.signUp(NEW_EMAIL);
        Assert.assertNotNull(actual);
    }

    @Test
    public void testCheckEmailTrue() throws ServiceException {
        user.setPassword("67b176705b46206614219f47a05aee7ae6a3edbe850bbbe214c536b989aea4d2");
        userService.updateUser(user);
        User actual = userService.checkEmail(CORRECT_EMAIL, CORRECT_PASSWORD);
        Assert.assertNotNull(actual);
    }

    @Test(expectedExceptions = ServiceException.class, expectedExceptionsMessageRegExp = "Login is incorrect")
    public void testCheckEmailWrongEmail() throws ServiceException {
        userService.checkEmail(WRONG_EMAIL, WRONG_PASSWORD);

    }

    @Test(expectedExceptions = ServiceException.class, expectedExceptionsMessageRegExp = "Password is incorrect")
    public void testCheckEmailWrongPassword() throws ServiceException {
        userService.checkEmail(CORRECT_EMAIL, WRONG_PASSWORD);
    }


    @Test
    public void testFindUserById() throws ServiceException {
        User expected = user;
        User actual = userService.findUserById(50L);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindUserByEmail() throws ServiceException {
        User expected = user;
        User actual = userService.findUserByEmail("qwe@mail.ru");
        Assert.assertEquals(actual, expected);
    }


    @Test
    public void testUpdateUser() throws ServiceException {
        user.setPassword("123");
        User expected = userService.findUserByEmail("qwe@mail.ru");
        userService.updateUser(user);
        User actual = userService.findUserByEmail("qwe@mail.ru");
        Assert.assertNotEquals(actual, expected);
    }

    @Test
    public void testChangePassword() throws ServiceException {
        String oldPassword = "1";
        String newPassword = "123";
        String repeatNewPassword = "123";
        String checkedPassword = PasswordService.cryptPassword(newPassword);
        user.setPassword(checkedPassword);
        User expected = user;
        userService.changePassword(oldPassword, newPassword, repeatNewPassword, user.getId());
        User actual = userService.findUserByEmail("qwe@mail.ru");
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindAllUsers() throws ServiceException {
        int expected = 8;
        List<User> usersFromDb =  userService.findAllUsers(1,10);
        int actual = usersFromDb.size();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testBlockUser() throws Exception {
        user.setDeleted(true);
        User expected = user;
        userService.blockUser(50L, true);
        User actual = userService.findUserByEmail("qwe@mail.ru");
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindUsersByTariffId() throws ServiceException {
        String expected = user.getEmail();
        List<User> usersFromDb = userService.findUsersByTariffId("19", 1, 5);
        String actual = usersFromDb.get(0).getEmail();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindUsersBySearchingValue() throws Exception {
        String expected = user.getEmail();
        List<User> usersFromDb = userService.findUsersBySearchingValue("Все", 1, 5, "admin");
        String actual = usersFromDb.get(0).getEmail();
        Assert.assertEquals(actual, expected);
    }
}