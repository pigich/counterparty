package com.epam.counterparty.service.impl;

import com.epam.counterparty.BaseTest;
import com.epam.counterparty.entity.Fee;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.FeeService;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Pavel on 26.07.2019 11:20
 *
 * @author Pavel
 */
public class FeeServiceImplTest extends BaseTest {
    FeeService feeService = new FeeServiceImpl();
    private Fee fee;

    public FeeServiceImplTest(String name) {
        super(name);
    }

    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
        fee = new Fee();
        fee.setAmount(BigDecimal.valueOf(11));
        fee.setUserId(59);
    }

    @AfterMethod
    public void afterMethod() throws Exception {
        super.restoreDb();
    }
    @AfterClass
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testFindFees() throws ServiceException {
        int expected = 13;
        List<Fee> fees = feeService.findFees(0,20,1, "ASC");
        int actual = fees.size();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindFeesBySearchingValue() throws ServiceException {
        int expected = 1;
        List<Fee> fees = feeService.findFeesBySearchingValue(0,10,"100",1, "ASC");
        int actual = fees.size();
        Assert.assertEquals(actual, expected);
    }
}