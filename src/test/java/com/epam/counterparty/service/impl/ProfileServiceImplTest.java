package com.epam.counterparty.service.impl;

import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.ProfileService;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

/**
 * @since 20.07.2019
 * @author Pavel
 */
public class ProfileServiceImplTest {
    private Profile profile;
    private ProfileService profileService = new ProfileServiceImpl();

    @BeforeMethod
    public void BeforeMethod() throws Exception {
        profile = Profile.builder()
                .userId(50)
                .fullName("Паша")
                .orgName("EPAM")
                .orgUnp(123875411)
                .phone("80295489745")
                .tariffId(19)
                .build();
    }

    @AfterMethod
    public void AfterMethod() throws Exception {
        profile = null;
    }

    @Test
    public void testFindUserProfileById() throws ServiceException {
        long userId = 50L;
        Profile expected = profile;
        Profile actual = profileService.findUserProfileById(userId);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testChangeUserProfile() throws ServiceException {
        long userId = 54L;
        profile.setUserId(userId);
        profile.setTariffId(0);
        Profile expected = profile;
        String name = profile.getFullName();
        String orgName = profile.getOrgName();
        String phone = profile.getPhone();
        String orgUnp = String.valueOf(profile.getOrgUnp());
        Profile actual = profileService.changeUserProfile(name, phone, orgUnp,orgName, userId);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindProfiles() throws ServiceException {
        int expected = 8;
        List<Profile> profilesFromDb =  profileService.findProfiles();
        int actual = profilesFromDb.size();
        Assert.assertEquals(actual, expected);
    }
}