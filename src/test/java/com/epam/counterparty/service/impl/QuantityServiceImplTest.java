package com.epam.counterparty.service.impl;

import com.epam.counterparty.entity.Quantity;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.QuantityService;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @since 21.07.2019
 * @author Pavel
 */
public class QuantityServiceImplTest {
    private Quantity quantity;
    private QuantityService quantityService = new QuantityServiceImpl();

    @BeforeClass
    public void beforeClass() {
        quantity = Quantity.builder()
                .rowsCount(5)
                .pagesCount(0)
                .build();
    }

    @Test
    public void testFindQuantityOfUsers() throws ServiceException {
        quantity.setRowsCount(5);
        Quantity expected = quantity;
        Quantity actual = quantityService.findQuantityOfUsers("29");
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testSetPageCountDefault() {
        quantity.setRowsCount(7);
        long expected = 4;
        long actual = quantityService.setPageCount(quantity, 0);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testSetPageCount() {
        quantity.setRowsCount(7);
        long expected = 2;
        long actual = quantityService.setPageCount(quantity, 5);
        Assert.assertEquals(actual, expected);
    }


    @Test
    public void testFindQuantityOfUsersByValue() throws ServiceException {
        quantity.setRowsCount(1);
        String value = "SONY";
        Quantity expected = quantity;
        Quantity actual = quantityService.findQuantityOfUsersByValue(value, "29");
        Assert.assertEquals(actual, expected);
    }


    @Test
    public void testFindQuantityOfFeesByValue() throws ServiceException {
        quantity.setRowsCount(2);
        Quantity expected = quantity;
        Quantity actual = quantityService.findQuantityOfFeesByValue("2019-06-12");
        System.out.println("expected " + expected);
        System.out.println(" actual " + actual);
        Assert.assertEquals(actual, expected);
    }
}