package com.epam.counterparty.service.impl;

import com.epam.counterparty.BaseTest;
import com.epam.counterparty.entity.Ability;
import com.epam.counterparty.entity.Payment;
import com.epam.counterparty.entity.Profile;
import com.epam.counterparty.entity.Tariff;
import com.epam.counterparty.exception.ServiceException;
import com.epam.counterparty.service.PaymentService;
import com.epam.counterparty.service.ProfileService;
import com.epam.counterparty.service.TariffService;
import org.testng.Assert;
import org.testng.annotations.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @since 20.07.2019
 * @author Pavel
 */
public class TariffServiceImplTest extends BaseTest {
    private Tariff tariff;
    private TariffService tariffService = new TariffServiceImpl();
    private Profile profile;
    private ProfileService profileService = new ProfileServiceImpl();
    private Payment payment;
    private PaymentService paymentService = new PaymentServiceImpl();

    public TariffServiceImplTest(String name) {
        super(name);
    }

    @BeforeClass
    public void setUp() throws Exception {
        super.setUp();
    }
    @BeforeMethod
    public void beforeMethod() throws Exception {
        BigDecimal price = BigDecimal.valueOf(30);
        short period = 30;
        List<Ability> abilityList = new ArrayList<>();
        tariff = new Tariff(19L, "Базовый", "Base", price, period, abilityList, false);
        profile = Profile.builder()
                .userId(55)
                .fullName("Jim Carry")
                .phone("80327412387")
                .orgUnp(101541671)
                .orgName("IBM")
                .tariffId(19)
                .build();
        payment = new Payment();
        payment.setCurrentBalance(BigDecimal.valueOf(68));
        payment.setTariffId(19L);
    }

    @AfterMethod
    public void afterMethod() throws Exception {
        tariff = null;
    }
    @AfterClass
    public void tearDown() throws Exception {
        super.tearDown();
    }
    @Test
    public void testAddTariff() throws Exception {
        super.restoreDb();
        BigDecimal price = BigDecimal.valueOf(15);
        short period = 15;
        Tariff expected = new Tariff("Новый", "New", price, period, false);
        tariffService.addTariff(expected);
        Tariff actual = tariffService.findTariffByName(expected);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindTariffByName() throws ServiceException {
        Tariff expected = tariff;
        Tariff actual = tariffService.findTariffByName(tariff);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindWorkingTariffs() throws Exception {
        super.restoreDb();
        int expected = 4;
        List<Tariff> tariffs = tariffService.findWorkingTariffs();
        int actual = tariffs.size();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testFindTariffs() throws Exception {
        super.restoreDb();
        int expected = 5;
        List<Tariff> tariffs = tariffService.findTariffs();
        int actual = tariffs.size();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testChangeTariffPayment() throws ServiceException {
        long userId = 55L;
        tariffService.changeTariff(tariff, userId);
        Payment expected = payment;
        Payment paymentFromDb = paymentService.findPayment(userId);
        Payment actual = new Payment();
        actual.setCurrentBalance(paymentFromDb.getCurrentBalance());
        actual.setTariffId(paymentFromDb.getTariffId());
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testChangeTariffProfile() throws ServiceException {
        long userId = 55L;
        Profile expected = profile;
        Profile actual = profileService.findUserProfileById(userId);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testExtendTariff() throws ServiceException {
    }

    @Test
    public void testFindTariffById() throws ServiceException {
        Tariff expected = tariff;
        Tariff actual = tariffService.findTariffById(19L);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testSetTariffArchival() throws ServiceException {
        tariffService.setTariffArchival(19L);
        tariff.setArchival(true);
        Tariff expected = tariff;
        Tariff actual = tariffService.findTariffById(19L);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testUpdateTariff() throws ServiceException {
        BigDecimal price = BigDecimal.valueOf(15);
        short period = 15;
        tariff.setTariffNameRu("Обновленное имя");
        tariff.setTariffNameEn("Updated name");
        tariff.setPrice(price);
        tariff.setPeriod(period);
        Tariff expected = tariff;
        tariffService.updateTariff(tariff);
        Tariff actual = tariffService.findTariffByName(expected);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testCreateWorkingTariffsList() throws Exception {
        super.restoreDb();
        int expected = 4;
        List<Tariff> tariffList =  tariffService.createWorkingTariffsList();
        int actual = tariffList.size();
        Assert.assertEquals(actual, expected);
    }
}