package com.epam.counterparty;

import com.epam.counterparty.connection.ConnectionPool;
import com.epam.counterparty.connection.ProxyConnection;
import org.dbunit.DBTestCase;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;

import java.io.InputStream;
import java.sql.Connection;

/**
 * Base Test class
 *
 * @author Pavel
 * @since 21.07.2019
 */
public class BaseTest extends DBTestCase {
    private static final String DATA_DATASET_PATH = "/data/dataset.xml";
    private IDataSet dataSet;
    private Connection connection;
    private IDatabaseConnection iDatabaseConnection;

    public BaseTest(String name) {
        super(name);
    }

    @Override
    protected void setUpDatabaseConfig(DatabaseConfig config) {
        config.setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, true);
        config.setProperty(DatabaseConfig.PROPERTY_BATCH_SIZE, 200);
        config.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);
        config.setProperty(DatabaseConfig.FEATURE_BATCHED_STATEMENTS, true);
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
        config.setProperty(DatabaseConfig.FEATURE_DATATYPE_WARNING, false);
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        InputStream in = BaseTest.class.getResourceAsStream(DATA_DATASET_PATH);
        return new FlatXmlDataSetBuilder().build(in);
    }

    public void setUp() throws Exception {
        ConnectionPool.getInstance();
        connection = ConnectionPool.getInstance().getConnection();
        IDatabaseConnection jdbcConnection = new DatabaseConnection(connection);
        DatabaseConfig config = jdbcConnection.getConfig();
        setUpDatabaseConfig(config);
        iDatabaseConnection = new DatabaseConnection(connection);
        dataSet = getDataSet();
    }

    protected void restoreDb() throws Exception {
        DatabaseOperation.DELETE_ALL.execute(iDatabaseConnection, dataSet);
        DatabaseOperation.INSERT.execute(iDatabaseConnection, dataSet);
    }

    public void tearDown() throws Exception {
        ConnectionPool.getInstance().releaseConnection((ProxyConnection) connection);
    }
}