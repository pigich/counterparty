package com.epam.counterparty.util;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Pavel on 10.08.2019 22:21
 *
 * @author Pavel
 */
public class LangTypeTest {

    @Test
    public void testGetLocale() {
        String expected = "ru_RU";
        String actual = LangType.RU.getLocale();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testGetNameByValue() {
        LangType expected = LangType.EN;
        LangType actual = LangType.getNameByValue("en_US");
        Assert.assertEquals(actual, expected);
    }
}