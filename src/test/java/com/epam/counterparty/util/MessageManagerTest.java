package com.epam.counterparty.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import static com.epam.counterparty.command.Message.MESSAGE_WRONG_ACTION;

/**
 * Created by Pavel on 10.08.2019 22:26
 *
 * @author Pavel
 */
public class MessageManagerTest {
    @Test
    public void testGetProperty() {
        String expected = "не верная комманда!";
        String actual = MessageManager.getProperty(MESSAGE_WRONG_ACTION);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testGetProperty1() {
        String expected = "Command not found or wrong";
        String actual = MessageManager.getProperty(MESSAGE_WRONG_ACTION,"en_US");
        Assert.assertEquals(actual, expected);
    }
}