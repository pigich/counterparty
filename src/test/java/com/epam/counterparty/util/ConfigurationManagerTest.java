package com.epam.counterparty.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import static com.epam.counterparty.command.PathConstant.PROFILE_ADMIN_PAGE_PATH;
import static org.testng.Assert.*;

/**
 * Created by Pavel on 10.08.2019 22:18
 *
 * @author Pavel
 */
public class ConfigurationManagerTest {

    @Test
    public void testGetProperty() {
        String expected = "/jsp/profile-admin.jsp";
        String actual = ConfigurationManager.getProperty(PROFILE_ADMIN_PAGE_PATH);
        Assert.assertEquals(actual, expected);
    }
}