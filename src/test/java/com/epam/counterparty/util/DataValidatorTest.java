package com.epam.counterparty.util;

import com.epam.counterparty.exception.ServiceException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static com.epam.counterparty.command.Message.*;

/**
 * Created by Pavel on 10.08.2019 18:50
 *
 * @author Pavel
 */
public class DataValidatorTest {
    DataValidator dataValidator = new DataValidator();
    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }
    @DataProvider(name = "badDataValidateAmount")
    public static Object[][] dataProviderForValidateAmountOne() {
        return new Object[][]{
                { 1},
                { 1_000_001}
        };
    }
    @DataProvider(name = "goodDataValidateAmount")
    public static Object[][] dataProviderValidateAmountTwo() {
        return new Object[][]{
                { 2},
                { 1_000_000}
        };
    }
    @DataProvider(name = "badDataValidatePassword")
    public static Object[][] dataProviderValidatePasswordOne() {
        return new Object[][]{
                { "11"},
                { "DS#@"},
                { "asssssssssssssss"},
                { "-123"}
        };
    }
    @DataProvider(name = "goodDataValidatePassword")
    public static Object[][] dataProviderValidatePasswordTwo() {
        return new Object[][]{
                { "123"},
                { "asdfUds2da"}
        };
    }
    @DataProvider(name = "badDataValidateName")
    public static Object[][] dataProviderValidateNameOne() {
        return new Object[][]{
                { "11"},
                { "DS#@"},
                { "asdfU ds2da"},
                { "SD"}
        };
    }
    @DataProvider(name = "goodDataValidateName")
    public static Object[][] dataProviderValidateNameTwo() {
        return new Object[][]{
                { "Федя"},
                { "Mike Jordan"}
        };
    }
    @DataProvider(name = "badDataValidateUnp")
    public static Object[][] dataProviderValidateUnpOne() {
        return new Object[][]{
                { "11"},
                { "DS#@"},
                { "1231212121"},
                { "12312245"},
                { "SD"}
        };
    }
    @DataProvider(name = "goodDataValidateUnp")
    public static Object[][] dataProviderValidateUnpTwo() {
        return new Object[][]{
                { "123122452"}
        };
    }
    @DataProvider(name = "badDataValidatePhone")
    public static Object[][] dataProviderValidatePhoneOne() {
        return new Object[][]{
                { "+375 29 391 32 44 22"},
                { "+375 29d 391 32 44"},
                { "@80294321242"},
                { "8 029 43"},
                { "12"}
        };
    }
    @DataProvider(name = "goodDataValidatePhone")
    public static Object[][] dataProviderValidatePhoneTwo() {
        return new Object[][]{
                { "+375 29 391 32 44"},
                { "+375 29 391 32 44"},
                { "80294321242"},
                { "8029 432 12 42"},
                { "8-029-432-12 42"},
                { "8 029 432 12 42"},
                { "017 2 31243"}
        };
    }
    @DataProvider(name = "badDataValidateOrgName")
    public static Object[][] dataProviderValidateOrgNameOne() {
        return new Object[][]{
                { "+SDF 32 44"},
                { "@Чистота"},
                { "Чистота и порядок + dsf"}
        };
    }
    @DataProvider(name = "goodDataValidateOrgName")
    public static Object[][] dataProviderValidateOrgNameTwo() {
        return new Object[][]{
                { "\"Фирма\""},
                { "EPAM"},
                { "Чистота и порядок"},
                { "Coca-Cola"},
                { "\"Фирма DS\""}
        };
    }
  @DataProvider(name = "badDataValidateSearchValue")
    public static Object[][] dataProviderValidateSearchValueOne() {
        return new Object[][]{
                { "@ЧистотаЧистотаЧистотаЧистотаЧистотаЧистота"},
        };
    }
    @DataProvider(name = "goodDataValidateSearchValue")
    public static Object[][] dataProviderValidateSearchValueTwo() {
        return new Object[][]{
                { "+375 29 391 32 44"},
                { "8 029 432 12 42"},
                { "Чистота и порядок"},
                { "Coca-Cola"},
                { "\"Фирма DS\""}
        };
    }
    @DataProvider(name = "badDataValidateEmail")
    public static Object[][] dataProviderValidateEmailOne() {
        return new Object[][]{
                { "adsf@adf.asdf.32"},
                { "adsf@@adf.asdf.32"},
                { "@1adf.asdf.32"},
                { "asdf@mail.republic"}
        };
    }
    @DataProvider(name = "goodDataValidateEmail")
    public static Object[][] dataProviderValidateEmailTwo() {
        return new Object[][]{
                { "asdf@mail.ru"},
                { "asd-ad123f@ma312il.by.ua"}
        };
    }
    @DataProvider(name = "badDataValidateAbilityName")
    public static Object[][] dataProviderAbilityNameOne() {
        return new Object[][]{
                { "adsf@adf.asdf.32"},
                { "adsf@@adf.asdf.32"},
                { "@1adf.asdf.32"},
                { "asdf@mail.republic"}
        };
    }
    @DataProvider(name = "goodDataValidateAbilityName")
    public static Object[][] dataProviderAbilityNameTwo() {
        return new Object[][]{
                { "Чистота и порядок"},
                { "Coca-Cola"},
        };
    }
    @DataProvider(name = "badDataValidateTariffPeriod")
    public static Object[][] dataProviderTariffPeriodOne() {
        return new Object[][]{
                { "2334"},
                { "-12"},
                { "0"},
        };
    }
    @DataProvider(name = "goodDataValidateTariffPeriod")
    public static Object[][] dataProviderTariffPeriodTwo() {
        return new Object[][]{
                { "125"},
        };
    }
    @DataProvider(name = "badDataValidateTariffPrice")
    public static Object[][] dataProviderForValidateTariffPriceOne() {
        return new Object[][]{
                { 1},
                { 1_000_001}
        };
    }
    @DataProvider(name = "goodDataValidateTariffPrice")
    public static Object[][] dataProviderValidateTariffPriceTwo() {
        return new Object[][]{
                { 5},
                { 9_000}
        };
    }
    @Test(dataProvider = "badDataValidateAmount", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_AMOUNT_WRONG_FORMAT)
    public void testValidateAmountOne(long amount) throws Exception {
        BigDecimal actual = BigDecimal.valueOf(amount);
        dataValidator.validateAmount(actual);
    }
    @Test(dataProvider = "goodDataValidateAmount")
    public void testValidateAmountTwo(long amount) throws Exception {
        BigDecimal actual = BigDecimal.valueOf(amount);
        dataValidator.validateAmount(actual);
    }

    @Test(dataProvider = "badDataValidatePassword", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_PASSWORD_WRONG_FORMAT)
    public void testValidatePasswordOne(String actual) throws ServiceException {
        dataValidator.validatePassword(actual);
    }
    @Test(dataProvider = "goodDataValidatePassword")
    public void testValidatePasswordTwo(String actual) throws ServiceException {
        dataValidator.validatePassword(actual);
    }
    @Test(dataProvider = "badDataValidateName", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_WRONG_USER_NAME_FORMAT)
    public void testValidateNameOne(String actual) throws ServiceException {
        dataValidator.validateName(actual);
    }
    @Test(dataProvider = "goodDataValidateName")
    public void testValidateNameTwo(String actual) throws ServiceException {
        dataValidator.validateName(actual);
    }

    @Test(dataProvider = "badDataValidateUnp", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_WRONG_ORG_UNP_FORMAT)
    public void testValidateUnpOne(String actual) throws ServiceException {
        dataValidator.validateUnp(actual);
    }
    @Test(dataProvider = "goodDataValidateUnp")
    public void testValidateUnpTwo(String actual) throws ServiceException {
        dataValidator.validateUnp(actual);
    }

    @Test(dataProvider = "badDataValidatePhone", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_WRONG_PHONE_FORMAT)
    public void testValidatePhoneOne(String actual) throws ServiceException {
        dataValidator.validatePhone(actual);
    }
    @Test(dataProvider = "goodDataValidatePhone")
    public void testValidatePhoneTwo(String actual) throws ServiceException {
        dataValidator.validatePhone(actual);
    }


    @Test(dataProvider = "badDataValidateOrgName", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_WRONG_ORG_NAME_FORMAT)
    public void testValidateOrgNameOne(String actual) throws ServiceException {
        dataValidator.validateOrgName(actual);
    }
    @Test(dataProvider = "goodDataValidateOrgName")
    public void testValidateOrgNameTwo(String actual) throws ServiceException {
        dataValidator.validateOrgName(actual);
    }

    @Test(dataProvider = "badDataValidateSearchValue", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_WRONG_SEARCH_VALUE_FORMAT)
    public void testValidateSearchValueOne(String actual) throws ServiceException {
        dataValidator.validateSearchValue(actual);
    }
    @Test(dataProvider = "goodDataValidateSearchValue")
    public void testValidateSearchValueTwo(String actual) throws ServiceException {
        dataValidator.validateSearchValue(actual);
    }

    @Test(dataProvider = "badDataValidateEmail", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_WRONG_EMAIL_FORMAT)
    public void testValidateEmailOne(String actual) throws ServiceException {
        dataValidator.validateEmail(actual);
    }
    @Test(dataProvider = "goodDataValidateEmail")
    public void testValidateEmailTwo(String actual) throws ServiceException {
        dataValidator.validateEmail(actual);
    }
    @Test(dataProvider = "badDataValidateAbilityName", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_WRONG_NAME_FORMAT)
    public void testValidateAbilityNameOne(String actual) throws ServiceException {
        dataValidator.validateTariffAbilityName(actual);
    }
    @Test(dataProvider = "goodDataValidateAbilityName")
    public void testValidateAbilityNameTwo(String actual) throws ServiceException {
        dataValidator.validateTariffAbilityName(actual);
    }
    @Test(dataProvider = "badDataValidateTariffPeriod", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_WRONG_PERIOD_FORMAT)
    public void testValidateTariffPeriodOne(String actual) throws ServiceException {
        dataValidator.validateTariffPeriod(actual);
    }
    @Test(dataProvider = "goodDataValidateTariffPeriod")
    public void testValidateTariffPeriodTwo(String actual) throws ServiceException {
        dataValidator.validateTariffPeriod(actual);
    }

    @Test(dataProvider = "badDataValidateTariffPrice", expectedExceptions = ServiceException.class,
            expectedExceptionsMessageRegExp = MESSAGE_PRICE_WRONG_FORMAT)
    public void testValidateTariffPriceOne(long amount) throws ServiceException {
        BigDecimal actual = BigDecimal.valueOf(amount);
        dataValidator.validateTariffPrice(actual);
    }
    @Test(dataProvider = "goodDataValidateTariffPrice")
    public void testValidateTariffPriceTwo(long amount) throws ServiceException {
        BigDecimal actual = BigDecimal.valueOf(amount);
        dataValidator.validateTariffPrice(actual);
    }
}