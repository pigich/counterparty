<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="grt" uri="tag/greeting-tag.tld" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../include/begin-logged-in.jsp" %>
<fmt:setLocale value="${lang}" scope="session"/>
<c:set var="page" scope="session" value="user_page"/>

<div class="text-center info-message text-message">
    <grt:greeting role="${role}" lang="${lang}" userName="${profile.fullName}" greeting="${greeting}"/>
</div>
<div class="text-center ok-message text-message">
    <c:set var="greeting" scope="session" value="true"/>
    <c:out value="${sessionScope.ok_message}"/>
    <c:remove var="ok_message" scope="session"/>
</div>
<div class="text-center error-message text-message">
    <c:out value="${sessionScope.error_message}"/>
    <c:remove var="error_message" scope="session"/>
</div>

<div class="text-center">
    <div class="text-center error-message text-message">
        <h4 class="error_class"><c:if test="${current_user_status eq true}"><fmt:message
                key="label.user_page.user_blocked_one" bundle="${ rb }"/>
            <br><fmt:message key="label.user_page.user_blocked_two" bundle="${ rb }"/> </c:if></h4>
    </div>
</div>
<div class="text-center error-message text-message">
    <div class="text-center">
        <h4 class="error_class">
            <c:if test="${user_tariff_end_period_status eq false}"><fmt:message
                    key="label.user_blocked_tariff_one"
                    bundle="${ rb }"/>
                <br><fmt:message key="label.user_blocked_tariff_two" bundle="${ rb }"/>
            </c:if>
        </h4>
    </div>
</div>
<div class="jumbotron text-center">
    <p><fmt:message key="user.label.find" bundle="${ rb }"/></p>
    <form class="form-inline" action="do?command=find_org_info" method="post">
        <div class="input-group">
            <c:if test="${current_user_status eq false and user_tariff_end_period_status eq true}">
                <input type="number" name="searching_unp" class="form-control" size="50"
                       placeholder="<fmt:message key="user.find.unp" bundle="${ rb }"/>" required>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success"><fmt:message key="button.find"
                                                                               bundle="${ rb }"/></button>
                </div>
            </c:if>
            <c:if test="${current_user_status eq true or user_tariff_end_period_status eq false}">
                <input type="number" name="searching_unp" class="form-control " size="50" disabled
                       placeholder="<fmt:message key="user.find.unp" bundle="${ rb }"/>" required>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success disabled" disabled><fmt:message key="button.find"
                                                                                                 bundle="${ rb }"/></button>
                </div>
            </c:if>
        </div>
    </form>

<c:if test="${not empty orgInfo.unp}">
        <div class="container">
            <div class="col-sm-11 col-xs-12">

                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th><fmt:message key="label.org_info.unp" bundle="${ rb }"/></th>
                            <td>${orgInfo.unp}</td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.registration_date" bundle="${ rb }"/></th>
                            <td>${orgInfo.registrationDate}</td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.exclusion_date" bundle="${ rb }"/></th>
                            <td>${orgInfo.exclusionDate}</td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.org_name" bundle="${ rb }"/></th>
                            <td>${orgInfo.nameOrgan}</td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.full_name_ru" bundle="${ rb }"/></th>
                            <td>${orgInfo.fullNameRu}</td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.short_name_ru" bundle="${ rb }"/></th>
                            <td>${orgInfo.shortNameRu}</td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.firm_name_ru" bundle="${ rb }"/></th>
                            <td>${orgInfo.firmNameRu}</td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.activity" bundle="${ rb }"/></th>
                            <td><c:if test="${orgInfo.activity eq 'true'}"><fmt:message key="label.org_info.activity.active" bundle="${ rb }"/></c:if>
                                <c:if test="${orgInfo.activity eq 'false'}"><fmt:message key="label.org_info.activity.excluded" bundle="${ rb }"/></c:if></td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.zapret" bundle="${ rb }"/></th>
                            <td><c:if test="${orgInfo.zapret eq 'true'}"><fmt:message key="label.org_info.zapret.no" bundle="${ rb }"/></c:if>
                                <c:if test="${orgInfo.zapret eq 'false'}"><fmt:message key="label.org_info.zapret.yes" bundle="${ rb }"/></c:if></td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.full_name_by" bundle="${ rb }"/></th>
                            <td>${orgInfo.fullNameBy}</td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.short_name_by" bundle="${ rb }"/></th>
                            <td>${orgInfo.shortNameBy}</td>
                        </tr>
                        <tr>
                            <th><fmt:message key="label.org_info.firm_name_by" bundle="${ rb }"/></th>
                            <td>${orgInfo.firmNameBy}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</c:if>
</div>
<%@ include file="../include/end-html.jsp" %>