<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="properties/pageContent" var="rb"/>
<c:set var="page" scope="session" value="error"/>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="${contextPath}/css/bootstrap.css" rel="stylesheet">
    <link href="${contextPath}/css/font-awesome.css" rel="stylesheet">
    <link href="${contextPath}/css/styles.css" rel="stylesheet">
    <script type="text/javascript" src="${contextPath}/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="${contextPath}/js/bootstrap.js"></script>
</head>
<body>
<%--<style type="text/css">--%>
    <%--* {--%>
        <%---webkit-box-sizing: border-box;--%>
        <%--box-sizing: border-box;--%>
    <%--}--%>

    <%--body {--%>
        <%--padding: 0;--%>
        <%--margin: 0;--%>
    <%--}--%>

    <%--#notfound {--%>
        <%--position: relative;--%>
        <%--height: 100vh;--%>
    <%--}--%>

    <%--#notfound .notfound {--%>
        <%--position: absolute;--%>
        <%--left: 50%;--%>
        <%--top: 50%;--%>
        <%---webkit-transform: translate(-50%, -50%);--%>
        <%---ms-transform: translate(-50%, -50%);--%>
        <%--transform: translate(-50%, -50%);--%>
    <%--}--%>

    <%--.notfound {--%>
        <%--max-width: 767px;--%>
        <%--width: 100%;--%>
        <%--line-height: 1.4;--%>
        <%--padding: 0px 15px;--%>
    <%--}--%>

    <%--.notfound .notfound-404 {--%>
        <%--position: relative;--%>
        <%--height: 150px;--%>
        <%--line-height: 150px;--%>
        <%--margin-bottom: 25px;--%>
    <%--}--%>

    <%--.notfound .notfound-404 h1 {--%>
        <%--font-family: 'Titillium Web', sans-serif;--%>
        <%--font-size: 186px;--%>
        <%--font-weight: 900;--%>
        <%--margin: 0px;--%>
        <%--text-transform: uppercase;--%>
        <%--background: url('../img/404.png');--%>
        <%---webkit-background-clip: text;--%>
        <%---webkit-text-fill-color: transparent;--%>
        <%--background-size: cover;--%>
        <%--background-position: center;--%>
    <%--}--%>

    <%--.notfound h2 {--%>
        <%--font-family: 'Titillium Web', sans-serif;--%>
        <%--font-size: 26px;--%>
        <%--font-weight: 700;--%>
        <%--margin: 0;--%>
    <%--}--%>

    <%--.notfound p {--%>
        <%--font-family: 'Montserrat', sans-serif;--%>
        <%--font-size: 14px;--%>
        <%--font-weight: 500;--%>
        <%--margin-bottom: 0px;--%>
        <%--text-transform: uppercase;--%>
    <%--}--%>

    <%--.notfound a:hover {--%>
        <%--opacity: 0.8;--%>
    <%--}--%>

    <%--@media only screen and (max-width: 767px) {--%>
        <%--.notfound .notfound-404 {--%>
            <%--height: 110px;--%>
            <%--line-height: 110px;--%>
        <%--}--%>
        <%--.notfound .notfound-404 h1 {--%>
            <%--font-size: 120px;--%>
        <%--}--%>
    <%--}--%>

<%--</style>--%>

<c:if test="${pageContext.errorData.statusCode ne 404}">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="error-template">
                    <fmt:message key="page.error.text" bundle="${ rb }"/>
                    <div class="text-center error-message text-message" id="error-message">
                        <br>
                        <c:out value="${param.error_message}"/>
                        <c:remove var="error_message" scope="request"/>
                        <c:out value="${sessionScope.error_message}"/>
                        <c:remove var="error_message" scope="session"/>
                    </div>
                    <br>
                    Request from ${pageContext.errorData.requestURI} is failed
                    <br>
                    Status code: ${pageContext.errorData.statusCode}
                    <br/>
                    Exception: ${pageContext.errorData.throwable}
                    <br/>
                    Message from exception: ${pageContext.exception.message}
                </div>
            </div>
        </div>
    </div>
</c:if>
<c:if test="${pageContext.errorData.statusCode eq 404}">
    <div id="notfound">
        <div class="notfound">
            <div class="notfound-404">
                <h1>404</h1>
            </div>
            <h2>  <fmt:message key="page.error.page_not_found" bundle="${ rb }"/></h2>
            <br>
            <a href="do?command=to_main_page" class="btn btn-primary btn-lg">
                <fmt:message key="page.error.to_main" bundle="${ rb }"/></a>
        </div>
    </div>
<%--    <div class="container">--%>
<%--        <div class="row">--%>
<%--            <div id="notfound">--%>
<%--                <div class="col-md-12 notfound">--%>
<%--                    <div class="error-template">--%>
<%--                        <div class="notfound-404">--%>
<%--                            <h1>404</h1>--%>
<%--                        </div>--%>
<%--                        <div class="error-details">--%>
<%--                            <fmt:message key="page.error.page_not_found" bundle="${ rb }"/>--%>
<%--                        </div>--%>
<%--                        <div class="error-actions">--%>
<%--                            <a href="do?command=to_main_page" class="btn btn-primary btn-lg">--%>
<%--                                <span class="glyphicon glyphicon-home"></span>--%>
<%--                                <fmt:message key="page.error.to_main" bundle="${ rb }"/> </a>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
</c:if>

</body>
</html>
