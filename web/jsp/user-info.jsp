<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="rows_on_page" scope="page" value=" ${fn:length(users)}"/>
<c:set var="page" scope="session" value="user_info"/>
<fmt:setLocale value="${lang}" scope="session"/>
<%@ include file="../include/begin-logged-in-admin.jsp" %>
<style type="text/css">

    .table-wrapper {
        background: #fff;
        padding: 20px 25px;
        margin: 20px 0;
        border-radius: 3px;
        box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
    }
    .table-title {
        padding-bottom: 15px;
        background: #435d7d;
        color: #fff;
        padding: 16px 30px;
        margin: -20px -25px 10px;
        border-radius: 3px 3px 0 0;
    }
    .table-title h2 {
        margin: 5px 0 0;
        font-size: 24px;
        float: left;
    }
    .table-title .btn {
        float: right;
    }
    .table-title .btn {
        color: #fff;
        float: right;
        /*font-size: 13px;*/
        border: none;
        min-width: 45px;
        border-radius: 2px;
        border: none;
        margin-left: 10px;
    }
    .table-title .btn i {
        float: left;
    }
    .iconsClass:hover {
        text-shadow: 2px 2px 15px #272634;
    }
    .table-title .btn span {
        float: left;
    }
    table.table tr th:last-child {
        width: 100px;
    }
    table.table td:last-child i {
        opacity: 0.9;
        font-size: 22px;
        margin: 0 5px;
    }
    table.table td a {
        font-weight: bold;
        color: #566787;
        display: inline-block;
        text-decoration: none;
        outline: none !important;
    }
    table.table td a:hover {
        color: #2196F3;

    }
    table.table td i {
        font-size: 19px;
    }
    table.table {
        border-radius: 50%;
        vertical-align: middle;
        margin-right: 10px;
    }

    .pagination li a {
        border: none;
        font-size: 13px;
        min-width: 30px;
        min-height: 30px;
        color: #999;
        margin: 0 2px;
        line-height: 30px;
        border-radius: 2px !important;
        text-align: center;
        padding: 0 6px;
    }

    .pagination li a:hover {
        color: #666;
    }
    .pagination li.active a, .pagination li.active {
        background: #03A9F4;
    }

    .pagination li.active a:hover {
        background: #0397d6;
    }

    .pagination li.disabled i {
        color: #ccc;
    }
    .pagination li i {
        font-size: 16px;
        padding-top: 6px
    }
    .checkbox label {
        display: inline-block;
        position: relative;
        padding-left: 5px; }
    .checkbox label::before {
        content: "";
        display: inline-block;
        position: absolute;
        width: 17px;
        height: 17px;
        left: 0;
        margin-left: -20px;
        border: 1px solid #cccccc;
        border-radius: 3px;
        background-color: #fff;
        -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        transition: border 0.15s ease-in-out, color 0.15s ease-in-out; }
    .checkbox label::after {
        display: inline-block;
        position: absolute;
        width: 16px;
        height: 16px;
        left: 0;
        top: 0;
        margin-left: -20px;
        padding-left: 3px;
        padding-top: 1px;
        font-size: 11px;
        color: #555555; }
    .checkbox input[type="checkbox"] {
        opacity: 0; }
    .checkbox input[type="checkbox"]:focus + label::before {
        outline: thin dotted;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px; }
    .checkbox input[type="checkbox"]:checked + label::after {
        font-family: 'FontAwesome';
        content: "\f00c"; }
    .checkbox input[type="checkbox"]:disabled + label {
        opacity: 0.65; }
    .checkbox input[type="checkbox"]:disabled + label::before {
        background-color: #eeeeee;
        cursor: not-allowed; }

    .checkbox-primary input[type="checkbox"]:checked + label::before {
        background-color: #428bca;
        border-color: #428bca; }
    .checkbox-primary input[type="checkbox"]:checked + label::after {
        color: #fff; }
    .modal .form-control {
        border-radius: 2px;
        box-shadow: none;
        border-color: #dddddd;
    }

    .modal textarea.form-control {
        resize: vertical;
    }

    .modal .btn {
        border-radius: 2px;
        min-width: 100px;
    }

    .modal form label {
        font-weight: normal;
    }

    /*filter table */
    .table-filter .filter-group {
        float: right;
        margin-left: 15px;
    }

    .table-filter input, .table-filter select {
        height: 34px;
        border-radius: 3px;
        border-color: #ddd;
        box-shadow: none;
    }
    .table-filter {
        padding: 5px 0 15px;
        border-bottom: 1px solid #e9e9e9;
        margin-bottom: 5px;
    }
    .table-filter .btn {
        height: 34px;
    }
    .table-filter label {
        font-weight: normal;
        margin-left: 5px;
    }
    .table-filter select, .table-filter input {
        display: inline-block;
        margin-left: 5px;
    }
    .table-filter input {
        width: 280px;
        display: inline-block;
    }
    .filter-group select.form-control {
        width: 80px;
    }

</style>
<div class="text-center ok-message text-message">
    <c:out value="${sessionScope.ok_message}" />
    <c:remove var="ok_message" scope="session" />
</div>
<div class="text-center error-message text-message" id="errorInfoLabel">
    <c:out value="${sessionScope.error_message}" />
    <c:remove var="error_message" scope="session" />
</div>

<div class="jumbotron text-center">
    <div class="btn-group">
        <form action="do" method="post">
            <input type="hidden" id="currentLang" value="${lang}"/>
            <button type="submit" class="btn btn-success" name="command" value="to_user_info">
                <fmt:message key="button.users" bundle="${ rb }"/>
            </button>
            <button type="submit" class="btn btn-success" name="command" value="to_payments_info">
                <fmt:message key="button.payments" bundle="${ rb }"/>
            </button>
            <button type="submit" class="btn btn-success" name="command" value="to_tariff_info">
                <fmt:message key="button.tariffs" bundle="${ rb }"/>
            </button>
        </form>
    </div>
    <br>
    <br>

    <div class="modal fade" id="myModal" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" id="errorInfoClass">
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="table-wrapper">
            <div class="table-title" style="background: #03A9F4">
                <div class="row">
                    <div class="col-sm-6">
                        <h2><b><fmt:message key="button.users" bundle="${ rb }"/></b></h2>
                    </div>
                </div>
            </div>
            <div class="table-filter">
                <div class="row">
                    <div class="col-sm-2 ">
                        <div >
                            <select class="form-control" id="showRowsOnPage">
                                <option value="2" selected="selected" myTag="tag1">2</option>
                                <option value="5" myTag="tag2" >5</option>
                                <option value="10" myTag="tag3">10</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="filter-group">
                            <button type="button" class="btn btn-primary" id="search_button"><i class="fa fa-search"></i></button>
                        </div>
                        <div class="filter-group ">
                            <label><fmt:message key="label.find" bundle="${ rb }"/></label>
                            <input type="text" class="form-control" id="search_input">
                        </div>
                        <div class="filter-group" >
                            <label><fmt:message key="label.payment_info.tariff_id" bundle="${ rb }"/></label>
                            <select class="form-control" id="tableIdSearch" >
                                <option selected="selected"><fmt:message key="label.tariff_info.all_tariffs" bundle="${ rb }"/></option>
                                <c:forEach items="${tariffs}" var="currentTariff" varStatus="loop">
                                    <option id="tariffId${loop.count}" name="current_tariff_id"><c:out value="${currentTariff.id}"/></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <table  class="table table-striped table-bordered table-condensed table-responsive">
                <thead>
                <tr style="text-align:center">
                    <th><fmt:message key="label.payment_info.user_id" bundle="${ rb }"/></th>
                    <th><fmt:message key="profile.label.full_name" bundle="${ rb }"/></th>
                    <th><fmt:message key="profile.label.phone" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.user_info.email" bundle="${ rb }"/></th>
                    <th><fmt:message key="profile.label.organization_name" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.payment_info.tariff_id" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.user_info.role" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.user_info.status" bundle="${ rb }"/></th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="user_info_tbody">
                <c:forEach items="${users}" var="currentUser" varStatus="status">
                    <tr >
                        <td id="userId${status.count}"><c:out value="${currentUser.id}"/></td>
                        <td id="fullName${status.count}"><c:out value="${currentUser.profile.fullName}"/></td>
                        <td id="phone${status.count}"><c:out value="${currentUser.profile.phone}"/></td>
                        <td id="email${status.count}"><c:out value="${currentUser.email}"/></td>
                        <td id="orgName${status.count}"><c:out value="${currentUser.profile.orgName}"/></td>
                        <td id="tariffId${status.count}"><c:out value="${currentUser.profile.tariffId}"/></td>
                        <td id="role${status.count}">
                            <c:if test="${currentUser.role eq 2}">admin</c:if>
                            <c:if test="${currentUser.role eq 1}">user</c:if></td>
                        <td id="userStatus${status.count}">
                            <c:if test="${currentUser.deleted eq false}"><fmt:message key="label.user_info.active" bundle="${ rb }"/></c:if>
                            <c:if test="${currentUser.deleted eq true}"><fmt:message key="label.user_info.blocked" bundle="${ rb }"/></c:if></td>
                        <td>
                                <input type="hidden" id="currentUserId${status.count}" value="${currentUser.id}"/>
                                <input type="hidden" id="currentUserStatus${status.count}" value="${currentUser.deleted}"/>
                                <c:if test="${currentUser.deleted eq true}">
                                    <label data-index="${status.count}" for="blockSubmit${status.count}" class="btn btn-xs iconsClass">
                                        <i data-toggle="tooltip" title="Lock" class="fa fa-lock fa-sm"></i></label>
                                    <input type="submit" name="command" id="blockSubmit${status.count}"
                                           value="${status.count}" class="hidden"/>
                                </c:if>
                                <c:if test="${currentUser.deleted eq false}">
                                    <label data-index="${status.count}" for="blockSubmit${status.count}" class="btn btn-xs">
                                        <i data-toggle="tooltip" title="UnLock" class="fa fa-unlock fa-sm iconsClass"></i></label>
                                    <input type="submit" name="command" id="blockSubmit${status.count}"
                                           value="${status.count}" class="hidden"/>
                                </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div class="clearfix">
                <div class="hint-text">Showing <b><label id="current_rows_label">${rows_on_page}</label></b> out of <b><label id="total_rows_label">${rows_count}</label></b> entries</div>
                <ul class="pagination" id="show_pages">
                    <li class="page-item active" data-page_value="1" id="1"><a href="#" class="page-link">1</a></li>
                    <input type="hidden" name="total_rows_count" id="total_rows_count" value="${rows_count}"/>
                    <input type="hidden" name="current_page" id="current_page" value="1"/>
                    <c:forEach var = "i" begin = "2" end = "${pages_count}">
                    <li class="page-item" data-page_value="${i}" id="${i}"><a href="#" >${i}</a></li>
                    </c:forEach>

                </ul>
            </div>
        </div>
    </div>
</div>

<%@ include file="../include/end-html.jsp" %>