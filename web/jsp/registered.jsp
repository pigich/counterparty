<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../include/begin-html.jsp" %>
<c:set var="page" scope="session" value="registered"/>
<fmt:setLocale value="${lang}" scope="session"/>

<style>
    .table-wrapper{
        background-color: rgba(255, 255, 255, 0.0);
        height: 100%;
    }
</style>
<div class="text-center ok-message text-message">
    <br>
    <c:out value="${sessionScope.ok_message}" />
    <c:remove var="ok_message" scope="session" />
</div>
<div class="text-center error-message text-message" id="error-message">
    <br>
    <c:out value="${sessionScope.error_message}" />
    <c:remove var="error_message" scope="session" />
</div>
<div class="table-wrapper">
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-8">
        <c:if test="${registration_page_status eq 'registration'}">
            <h2>
                <fmt:message key="registered.label.registration" bundle="${ rb }"/>
            </h2>
            <hr/>
            <fmt:message key="registered.label.hr" bundle="${ rb }"/> ${ pageContext.session.getAttribute("email")}.
            <hr/>
        </c:if>
        <c:if test="${registration_page_status eq 'recovering'}">
            <h2>
                <fmt:message key="registered.label.recovering" bundle="${ rb }"/>
            </h2>
            <hr/>
            <fmt:message key="registered.label.hr" bundle="${ rb }"/> ${ pageContext.session.getAttribute("email")}.
            <hr/>
        </c:if>

    </div>
    <div class="col-sm-3"></div>
</div>
</div>
<%@ include file="../include/end-html.jsp" %>