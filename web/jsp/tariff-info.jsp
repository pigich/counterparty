<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="page" scope="session" value="tariff_info"/>
<fmt:setLocale value="${lang}" scope="session"/>
<%@ include file="../include/begin-logged-in-admin.jsp" %>

<style type="text/css">
    .checkbox {
        padding-left: 20px; }
    .checkbox label {
        display: inline-block;
        position: relative;
        padding-left: 5px; }
    .checkbox label::before {
        content: "";
        display: inline-block;
        position: absolute;
        width: 17px;
        height: 17px;
        left: 0;
        margin-left: -20px;
        border: 1px solid #cccccc;
        border-radius: 3px;
        background-color: #fff;
        -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
        transition: border 0.15s ease-in-out, color 0.15s ease-in-out; }
    .checkbox label::after {
        display: inline-block;
        position: absolute;
        width: 16px;
        height: 16px;
        left: 0;
        top: 0;
        margin-left: -20px;
        padding-left: 3px;
        padding-top: 1px;
        font-size: 11px;
        color: #555555; }
    .checkbox input[type="checkbox"] {
        opacity: 0; }
    .checkbox input[type="checkbox"]:focus + label::before {
        outline: thin dotted;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px; }
    .checkbox input[type="checkbox"]:checked + label::after {
        font-family: 'FontAwesome';
        content: "\f00c"; }
    .checkbox input[type="checkbox"]:disabled + label {
        opacity: 0.65; }
    .checkbox input[type="checkbox"]:disabled + label::before {
        background-color: #eeeeee;
        cursor: not-allowed; }

    .checkbox-primary input[type="checkbox"]:checked + label::before {
        background-color: #428bca;
        border-color: #428bca; }
    .checkbox-primary input[type="checkbox"]:checked + label::after {
        color: #fff; }

    /* Modal styles */
    .modal .modal-dialog {
        max-width: 400px;
    }

    .modal .modal-header, .modal .modal-body, .modal .modal-footer {
        padding: 20px 30px;
    }

    .modal .modal-content {
        border-radius: 3px;
    }

    .modal .modal-footer {
        background: #ecf0f1;
        border-radius: 0 0 3px 3px;
    }

    .modal .modal-title {
        display: inline-block;
    }

    .modal .form-control {
        border-radius: 2px;
        box-shadow: none;
        border-color: #dddddd;
    }

    .modal textarea.form-control {
        resize: vertical;
    }

    .modal .btn {
        border-radius: 2px;
        min-width: 100px;
    }

    .iconsClass:hover {
        color: green;
        transition: 10ms;
        text-shadow: 2px 2px 15px #272634;
    }

    .modal form label {
        font-weight: normal;
    }
</style>

<div class="text-center ok-message text-message">
    <c:out value="${sessionScope.ok_message}" />
    <c:remove var="ok_message" scope="session" />
</div>
<div class="text-center error-message text-message">
    <c:out value="${sessionScope.error_message}" />
    <c:remove var="error_message" scope="session" />
</div>
<div class="jumbotron text-center">

    <div class="btn-group">
        <form action="do" method="post">
            <input type="hidden" id="currentLang" value="${lang}"/>
            <button type="submit" class="btn btn-success" name="command" value="to_user_info">
                <fmt:message key="button.users" bundle="${ rb }"/>
            </button>
            <button type="submit" class="btn btn-success" name="command" value="to_payments_info">
                <fmt:message key="button.payments" bundle="${ rb }"/>
            </button>
            <button type="submit" class="btn btn-success" name="command" value="to_tariff_info">
                <fmt:message key="button.tariffs" bundle="${ rb }"/>
            </button>
        </form>
    </div>
    <br>
    <br>

    <div class="container">
        <div class="table-wrapper">
            <div class="table-title" style="background: #03A9F4">
                <div class="row">
                    <div class="col-sm-6">
                        <h2><b><fmt:message key="label.tariff_info.header" bundle="${ rb }"/></b></h2>
                    </div>
                    <div class="col-sm-6">
                        <a href="#addTariffModal" class="btn btn-success add-new" data-toggle="modal">
                            <span><fmt:message key="button.tariff_info.add_new_tariff" bundle="${ rb }"/></span></a>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered table-condensed">
                <thead >
                <tr>
                    <th><fmt:message key="label.payment_info.tariff_id" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.tariff_info.name_en" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.tariff_info.name_ru" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.tariff_info.period" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.tariff_info.price" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.user_info.status" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.user_info.actions" bundle="${ rb }"/></th>
                </tr>
                </thead>
                <tbody id="tariff_info_tbody">
                <c:forEach items="${tariffs}" var="currentTariff" varStatus="status">
                    <tr>
                        <td id="id${status.count}"><c:out value="${currentTariff.id}"/></td>
                        <td id="tariffNameEn${status.count}"><c:out value="${currentTariff.tariffNameEn}"/></td>
                        <td id="tariffNameRu${status.count}"><c:out value="${currentTariff.tariffNameRu}"/></td>
                        <td id="period${status.count}"><c:out value="${currentTariff.period}"/></td>
                        <td id="price${status.count}"><label><c:out value="${currentTariff.price}"/></label></td>
                        <td id="archival${status.count}">
                            <c:if test="${currentTariff.archival eq false}"><fmt:message key="label.tariff_info.work" bundle="${ rb }"/></c:if>
                            <c:if test="${currentTariff.archival eq true}"><fmt:message key="label.tariff_info.archival" bundle="${ rb }"/></c:if>
                        </td>
                        <td>
                            <form action="do" method="post" style=" display: inline-block;">
                                <input type="hidden" name="current_tariff_id" id="tariffId${status.count}" value="${currentTariff.id}"/>
                                <a href="#editTariffModal" class="dropdown-item" data-toggle="modal"
                                   data-loop="${status.count}">
                                    <i data-toggle="tooltip" title="Edit"><span class="fa fa-pencil fa-sm iconsClass"
                                                                                style="color:orange"></span></i></a>
                            </form>
                            <c:if test="${currentTariff.archival eq false}">
                                <form action="do" method="post" style=" display: inline-block;">
                                    <input type="hidden" name="current_tariff_id" id="tariffId${status.count}" value="${currentTariff.id}"/>
                                    <label for="deleteSubmit${status.count}" class="btn btn-xs" >
                                        <i data-toggle="tooltip" title="Delete" class="fa fa-trash-o fa-sm iconsClass"
                                           style="color:red"></i></label>
                                    <input type="submit" name="command" id="deleteSubmit${status.count}"
                                           value="delete_tariff" class="hidden"/>
                                </form>
                            </c:if>

                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Edit Modal HTML -->
    <div id="addTariffModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="do" method="post" id="addTariffModalForm">
                    <input type="hidden" name="command" value="add_tariff">
                    <div class="modal-header">
                        <h4 class="modal-title"><fmt:message key="button.tariff_info.add_new_tariff"
                                                             bundle="${ rb }"/></h4>
                        ${sessionScope.errorBusyTariff}
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="tariff_id" value="0"/>
                            <label><fmt:message key="label.tariff_info.name_en" bundle="${ rb }"/></label>
                            <input type="text" name="tariff_name_en" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label><fmt:message key="label.tariff_info.name_ru" bundle="${ rb }"/></label>
                            <input type="text" name="tariff_name_ru" class="form-control" required>

                        </div>
                        <div class="form-group">
                            <label><fmt:message key="label.tariff_info.period" bundle="${ rb }"/></label>
                            <input type="number" name="period" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label><fmt:message key="label.tariff_info.price" bundle="${ rb }"/></label>
                            <input type="number" name="price" class="form-control" placeholder="0">
                        </div>
                        <label><fmt:message key="label.tariff_info.functional.header" bundle="${ rb }"/></label>
                        <div class="checkbox checkbox-primary" id="addTariffModalCheckBox">
                            <input type="hidden" name="ability_id_list" id="ability_id_list">
                            <c:forEach items="${abilities}" var="ability" varStatus="loop">
                                <div>
                                    <input type="checkbox" id="ability${loop.index}" name="addAbility"
                                           value="${ability.id}">
                                    <label class="custom-control-label" for="ability${loop.index}">
                                        <c:if test="${lang eq 'en_US'}">${ability.abilityNameEn}</c:if>
                                        <c:if test="${lang eq 'ru_RU'}">${ability.abilityNameRu}</c:if>
                                    </label>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal"
                               value="<fmt:message key="button.cancel" bundle="${ rb }"/>">
                        <input type="submit" class="btn btn-info" name="add_tariff"
                               value="<fmt:message key="button.save" bundle="${ rb }"/>">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Edit Tariff Modal HTML -->

    <div id="editTariffModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  action="do" method="post">
                    <input type="hidden" name="command" value="update_tariff">
                    <div class="modal-header">
                        <h4 class="modal-title"><fmt:message key="button.tariff_info.edit_tariff"
                                                             bundle="${ rb }"/></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label  >
                                <fmt:message key="label.payment_info.tariff_id" bundle="${ rb }"/>

                            </label>
                            <label id="edit_modal_id_label" ></label>
                            <%--                            <label id="edit_modal_id_label" ></label>--%>
                            <input type="hidden" id="modal_tariff_id" name="tariff_id" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label><fmt:message key="label.tariff_info.name_en" bundle="${ rb }"/></label>
                            <input type="text" id="modal_name_en" name="tariff_name_en"
                                   class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label><fmt:message key="label.tariff_info.name_ru" bundle="${ rb }"/></label>
                            <input type="text" id="modal_name_ru" name="tariff_name_ru"
                                   class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label><fmt:message key="label.tariff_info.period" bundle="${ rb }"/></label>
                            <input type="number" id="modal_period" name="period" class="form-control "
                                   required>
                        </div>
                        <div class="form-group">
                            <label><fmt:message key="label.tariff_info.price" bundle="${ rb }"/></label>
                            <input type="number" id="modal_price" name="price" class="form-control "
                                   placeholder="0">
                        </div>
                        <label><fmt:message key="label.tariff_info.functional.header" bundle="${ rb }"/></label>
                        <input type="hidden" id="edit_ability_list" name="ability_id_list">
                        <div class="checkbox checkbox-primary" id="editModalCheckBox">

                            <c:forEach items="${abilities}" var="ability" varStatus="loop">
                                <div>
                                    <input type="checkbox" id="editAbility${loop.index}" class="editAbility" name="editAbility"
                                           value="${ability.id}">
                                    <label class="custom-control-label" for="editAbility${loop.index}">
                                        <c:if test="${lang eq 'en_US'}">${ability.abilityNameEn}</c:if>
                                        <c:if test="${lang eq 'ru_RU'}">${ability.abilityNameRu}</c:if>
                                    </label>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal"
                               value="<fmt:message key="button.cancel" bundle="${ rb }"/>">
                        <input type="submit" class="btn btn-info" id="update_tariff" name="update_tariff"
                               value="<fmt:message key="button.save" bundle="${ rb }"/>">
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Functional HTML -->
    <div class="container">
        <div class="table-wrapper">
            <div class="table-title" style="background: #03A9F4">
                <div class="row">
                    <div class="col-sm-6">
                        <h2><b><fmt:message key="label.tariff_info.functional.header" bundle="${ rb }"/></b></h2>
                    </div>
                    <div class="col-sm-6">
                        <a href="#addAbilityModal" class="btn btn-success" data-toggle="modal">
                            <span><fmt:message key="label.tariff_info.functional.add_new" bundle="${ rb }"/></span></a>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr style="text-align:center">
                    <th><fmt:message key="label.tariff_info.ability_nameEn" bundle="${rb}"/></th>
                    <th><fmt:message key="label.tariff_info.ability_nameRu" bundle="${rb}"/></th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${abilities}" var="currentAbility" varStatus="loop">
                    <tr>
                        <td id="abilityNameEn${loop.count}">${currentAbility.abilityNameEn}</td>
                        <td id="abilityNameRu${loop.count}"><c:out value="${currentAbility.abilityNameRu}"/></td>
                        <td>
                            <form action="do" method="post">
                                <input type="hidden" name="current_ability_id" id="abilityId${loop.count}" value="${currentAbility.id}"/>

                                <a href="#editAbilityModal" data-loop="${loop.count}" class="dropdown-item" data-toggle="modal">
                                    <i data-toggle="tooltip" title="Edit"><span class="fa fa-pencil fa-sm iconsClass"
                                                                                style="color:orange"></span></i></a>
                                <label href="" for="deleteAbilitySubmit${loop.count}" class="btn btn-xs">
                                    <i data-toggle="tooltip" title="Delete" class="fa fa-trash-o fa-sm iconsClass"
                                       style="color:red"></i></label>
                                <input type="submit" name="command" id="deleteAbilitySubmit${loop.count}"
                                       value="delete_ability" class="hidden"/>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Add Ability Modal HTML -->
    <div id="addAbilityModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="do" method="post">
                    <input type="hidden" name="command" value="add_ability">
                    <div class="modal-header">
                        <input type="hidden"  name="ability_id" value="0"/>
                        <div class="modal-header">
                            <h4 class="modal-title"><fmt:message key="label.tariff_info.functional.adding_new" bundle="${ rb }"/></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label><fmt:message key="label.tariff_info.ability_nameEn" bundle="${ rb }"/></label>
                                <input type="text" name="ability_name_en"  class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label><fmt:message key="label.tariff_info.ability_nameRu" bundle="${ rb }"/></label>
                                <input type="text" name="ability_name_ru"  class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" name="add_ability"
                               value="<fmt:message key="button.add" bundle="${ rb }"/>">
                        <input type="button" class="btn btn-default" data-dismiss="modal"
                               value="<fmt:message key="button.cancel" bundle="${ rb }"/>">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Edit Ability Modal HTML -->
    <div id="editAbilityModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form  action="do" method="post">
                    <input type="hidden" name="command" value="update_ability">
                    <input type="hidden" id="modal_ability_id" name="ability_id"/>
                    <div class="modal-header">
                        <h4 class="modal-title"><fmt:message key="label.tariff_info.functional.edit" bundle="${ rb }"/></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label><fmt:message key="label.tariff_info.ability_nameEn" bundle="${ rb }"/></label>
                            <input type="text" name="ability_name_en" id="modal_ability_name_En"  class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label><fmt:message key="label.tariff_info.ability_nameRu" bundle="${ rb }"/></label>
                            <input type="text" name="ability_name_ru"  id="modal_ability_name_Ru" class="form-control" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal"
                               value="<fmt:message key="button.cancel" bundle="${ rb }"/>">
                        <input type="submit" class="btn btn-info" name="update_ability"
                               value="<fmt:message key="button.save" bundle="${ rb }"/>">
                    </div>
                </form>
            </div>
        </div>
    </div>

<%@ include file="../include/end-html.jsp" %>