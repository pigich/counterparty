<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<c:set var="page" scope="session" value="sign_up"/>
<fmt:setLocale value="${lang}" scope="session"/>
<%@ include file="../include/begin-html.jsp" %>

<div class="text-center ok-message text-message">
    <br>
    <c:out value="${sessionScope.ok_message}" />
    <c:remove var="ok_message" scope="session" />
</div>
<div class="text-center error-message text-message" id="error-message">
    <br>
    <c:out value="${sessionScope.error_message}" />
    <c:remove var="error_message" scope="session" />
</div>
<div class="jumbotron text-center">
      <p><fmt:message key="label.sign-up.legend" bundle="${ rb }"/></p>
    <form class="form-inline" action="do" method="post">
        <input type="hidden" name="command" value="sign_up">
        <div class="input-group">
            <input type="email" name="email" class="form-control" size="50"
                   placeholder="Email Address" required
                   onblur="validateEmail(this.value)" onchange="changeToDefault()" oninput="this.onchange()">
            <div class="input-group-btn">
                <button type="submit" class="btn btn-success submitButton">
                    <fmt:message key="button.sign_up" bundle="${ rb }"/></button>
            </div>
        </div>
    </form>
    <div class="text-center">
        <h5 class="resultOfValidation"></h5>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-xs-12">
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="text-center">
                    <br/>
                    <p style="color:red">${ pageContext.session.getAttribute("errorBusyEmail")}</p>
                    <br/>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12">
            </div>

        </div>
    </div>
</div>

</div>

<%@ include file="../include/end-html.jsp" %>