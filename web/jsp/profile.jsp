<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../include/begin-logged-in.jsp" %>
<fmt:setLocale value="${lang}" scope="session"/>
<c:set var="page" scope="session" value="profile"/>

<div class="text-center ok-message text-message">
    <br>
    <c:out value="${sessionScope.ok_message}" />
    <c:remove var="ok_message" scope="session" />
</div>
<div class="text-center error-message text-message" id="error-message">
    <br>
    <c:out value="${sessionScope.error_message}" />
    <c:remove var="error_message" scope="session" />
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h3><fmt:message key="profile.label.change_data" bundle="${ rb }"/></h3>
                </div>
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td colspan="1">
                            <form class="well form-horizontal" action="do?command=change_profile" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <label class=" control-label">
                                            <fmt:message key="profile.label.full_name" bundle="${ rb }"/></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i></span>
                                                <label for="full_name"></label>
                                                <input id="full_name" name="full_name" placeholder=""
                                                       class="form-control" value="${full_name}" type="text"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">
                                            <fmt:message key="profile.label.phone" bundle="${ rb }"/></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon">
                                                <i class="glyphicon glyphicon-earphone"></i></span>
                                                <label for="phone"></label>
                                                <input id="phone" name="phone" placeholder=""
                                                       class="form-control" value="${phone}" type="text"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label"><fmt:message key="profile.label.organization_unp"
                                                                                   bundle="${ rb }"/></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon">
                                                <i class="fa fa-id-card-o" aria-hidden="true"></i></span>
                                                <label for="organization_unp"></label>
                                                <input id="organization_unp" name="organization_unp" placeholder=""
                                                       class="form-control" value="${organization_unp}" type="text"
                                                       onblur="validateLength(12,12, this, 'неверная длинна УНП')"/>
                                                <%--                                                <span id="unp_help" style="color: #ff1105;"></span>>   --%>
                                                <%--                                                onblur="validateLength(12,12, this, document.getElementById('unp_help'))"/>--%>
                                                <%--                                                <span id="unp_help" style="color: #ff1105;"></span>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">
                                            <fmt:message key="profile.label.organization_name"
                                                         bundle="${ rb }"/></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon">
                                                <i class="fa fa-id-card-o" aria-hidden="true"></i></span>
                                                <label for="organization_name"></label>
                                                <input id="organization_name" name="organization_name" placeholder=""
                                                       class="form-control" value="${organization_name}" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-success">
                                            <fmt:message key="button.profile.change_data" bundle="${ rb }"/></button>
                                    </div>
                                </fieldset>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h3><fmt:message key="profile.button.change_pwd" bundle="${ rb }"/></h3>
                </div>
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td colspan="1">
                            <form class="well form-horizontal" action="do" method="post">
                                <input type="hidden" name="command" value="change_pwd">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="control-label">
                                            <fmt:message key="profile.label.old_pwd" bundle="${ rb }"/> <label
                                                style="color:red">*</label></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon"> <i
                                                    class="fa fa-lock"></i></span>
                                                <label for="old_pwd"></label>
                                                <input type="password" id="old_pwd" name="old_pwd"
                                                       class="form-control" required="required" value=""></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">
                                            <fmt:message key="profile.label.new_pwd" bundle="${ rb }"/><label
                                                style="color:red">*</label></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon"> <i
                                                    class="fa fa-lock"></i></span>
                                                <label for="new_pwd"></label>
                                                <input type="password" id="new_pwd" name="new_pwd"
                                                       class="form-control" required="required" value=""></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">
                                            <fmt:message key="profile.label.repeat_pwd" bundle="${ rb }"/><label
                                                style="color:red">*</label></label>
                                        <div class="inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon"> <i
                                                    class="fa fa-lock"></i></span>
                                                <label for="repeat_pwd"></label>
                                                <input type="password" id="repeat_pwd" name="repeat_pwd"
                                                       class="form-control" required="required" value=""></div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" name="change_pwd"
                                                class="btn btn-success">
                                            <fmt:message key="profile.button.change_pwd" bundle="${ rb }"/></button>
                                    </div>
                                </fieldset>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h3><fmt:message key="profile.label.choose_tariff" bundle="${ rb }"/></h3>
                </div>
                <label class="user_tariff">
                    <fmt:message key="profile.label.your_tariff" bundle="${ rb }"/>
                    <c:if test="${lang eq 'en_US'}">${current_tariff.tariffNameEn}</c:if>
                    <c:if test="${lang eq 'ru_RU'}">${current_tariff.tariffNameRu}</c:if>
                    <c:if test="${current_tariff.archival eq true}">
                </label>
                <label class="tariff_archive"> (<fmt:message key="profile.label.archive_tariff" bundle="${ rb }"/>)
                </label>
                </c:if>
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td colspan="1">
                            <form class="well form-horizontal" action="do?command=change_tariff" method="post">
                                <fieldset>
                                    <c:forEach items="${tariffs}" var="tariff" varStatus="loop">
                                        <c:if test="${tariff.id != current_tariff.id}">
                                        <c:if test="${tariff.archival eq false}">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="t${loop.index}" name="tariff_name"
                                                   class="custom-control-input" required="required"
                                                   value="<c:if test="${lang eq 'en_US'}">${tariff.tariffNameEn}</c:if><c:if test="${lang eq 'ru_RU'}">${tariff.tariffNameRu}</c:if>">
                                            <label class="custom-control-label" for="t${loop.index}">
  <%--@elvariable id="tariff" type="com.epam.counterparty.entity.<com.epam.counterparty.entity.Tariff</com.epam.counterparty.entity.Tariff>"--%>
                                                <c:if test="${lang eq 'en_US'}">${tariff.tariffNameEn}</c:if>
                                                <c:if test="${lang eq 'ru_RU'}">${tariff.tariffNameRu}</c:if>
                                            </label>
                                        </div>
                                        </c:if>
                                        </c:if>
                                    </c:forEach>
                                    <div>
                                        <button type="submit" class="btn btn-success">
                                            <fmt:message key="profile.button.change_tariff" bundle="${ rb }"/></button>
                                    </div>
                                </fieldset>
                            </form>
                            <c:if test="${current_tariff.archival eq false}">
                            <c:if test="${current_balance >= current_tariff.price}">
                            <form action="do" method="post" >
                                <input type="hidden" name="current_tariff_id" value="${current_tariff.id}">
                               <button type="submit"  class="btn btn-success" name="command" value="extend_tariff">
                                    <fmt:message key="button.extend" bundle="${ rb }"/>
                                </button>
                            </form>
                            </c:if>
                            </c:if>
                            <c:if test="${current_tariff.archival eq false}">
                                <c:if test="${current_balance < current_tariff.price}">
                                    <form action="do" method="post" >
                                        <input type="hidden" name="current_tariff_id" value="${current_tariff.id}">
                                        <button type="submit"  class="btn btn-success disabled" name="command" value="extend_tariff">
                                            <fmt:message key="button.extend" bundle="${ rb }"/>
                                        </button>
                                    </form>
                                </c:if>
                            </c:if>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h3><fmt:message key="profile.label.add_funds" bundle="${ rb }"/></h3>
                </div>
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td colspan="1">
                            <form class="well form-horizontal" action="do" method="post">
                                <input type="hidden" name="command" value="make_fee">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="control-label">
                                            <fmt:message key="profile.label.amount" bundle="${ rb }"/> <label
                                                style="color:red">*</label></label>
                                        <div class=" inputGroupContainer" align="center">
                                            <div class="input-group">
                                                <label for="fee_amount"></label>
                                                <input type="number" id="fee_amount" name="fee_amount"
                                                       class="form-control" required="required" value=""></div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" name="make_payment" class="btn btn-success">
                                            <fmt:message key="profile.button.pay" bundle="${ rb }"/></button>
                                    </div>
                                </fieldset>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<%@ include file="../include/end-html.jsp" %>