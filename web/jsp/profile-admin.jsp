<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../include/begin-logged-in-admin.jsp" %>
<fmt:setLocale value="${lang}" scope="session"/>
<c:set var="page" scope="session" value="profile_admin"/>
<%--<style>--%>
<%--    .table-wrapper{--%>
<%--        background-color: rgba(255, 255, 255, 0.0);--%>
<%--        height: 100%;--%>
<%--    }--%>
<%--</style>--%>


<div class="text-center ok-message text-message">
    <br>
    <c:out value="${sessionScope.ok_message}" />
    <c:remove var="ok_message" scope="session" />
</div>
<div class="text-center error-message text-message" id="error-message">
    <br>
    <c:out value="${sessionScope.error_message}" />
    <c:remove var="error_message" scope="session" />
</div>

<div class="jumbotron text-center">
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h3><fmt:message key="profile.label.change_data" bundle="${ rb }"/></h3>
                </div>
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td colspan="1">
                            <form class="well form-horizontal" action="do?command=change_profile" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <label class=" control-label">
                                            <fmt:message key="profile.label.full_name" bundle="${ rb }"/></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon">
                                                <i class="glyphicon glyphicon-user"></i></span>
                                                <input id="full_name" name="full_name" placeholder=""
                                                       class="form-control" value="${full_name}" type="text"></div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">
                                            <fmt:message key="profile.label.phone" bundle="${ rb }"/></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon">
                                                <i class="glyphicon glyphicon-earphone"></i></span>

                                                <input id="phone" name="phone" placeholder=""
                                                       class="form-control" value="${phone}" type="text"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label"><fmt:message key="profile.label.organization_unp"
                                                                                   bundle="${ rb }"/></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon">
                                                <i class="fa fa-id-card-o" aria-hidden="true"></i></span>
                                                <input id="organization_unp" name="organization_unp" placeholder=""
                                                       class="form-control" value="${organization_unp}" type="text"
                                                       onblur="validateLength(12,12, this, 'неверная длинна УНП')"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">
                                            <fmt:message key="profile.label.organization_name"
                                                         bundle="${ rb }"/></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon">
                                                <i class="fa fa-id-card-o" aria-hidden="true"></i></span>
                                                <input id="organization_name" name="organization_name" placeholder=""
                                                       class="form-control" value="${organization_name}" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-success">
                                            <fmt:message key="button.profile.change_data" bundle="${ rb }"/></button>
                                    </div>
                                </fieldset>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h3><fmt:message key="profile.button.change_pwd" bundle="${ rb }"/></h3>
                </div>
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <td colspan="1">
                            <form class="well form-horizontal" action="do" method="post">
                                <input type="hidden" name="command" value="change_pwd">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="control-label">
                                            <fmt:message key="profile.label.old_pwd" bundle="${ rb }"/> <label
                                                style="color:red">*</label></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon"> <i
                                                    class="fa fa-lock"></i></span>
                                                <input type="password" id="old_pwd" name="old_pwd"
                                                       class="form-control" required="true" value="" type="text"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">
                                            <fmt:message key="profile.label.new_pwd" bundle="${ rb }"/><label
                                                style="color:red">*</label></label>
                                        <div class=" inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon"> <i
                                                    class="fa fa-lock"></i></span>
                                                <input type="password" id="new_pwd" name="new_pwd"
                                                       class="form-control" required="true" value="" type="text"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">
                                            <fmt:message key="profile.label.repeat_pwd" bundle="${ rb }"/><label
                                                style="color:red">*</label></label>
                                        <div class="inputGroupContainer">
                                            <div class="input-group"><span class="input-group-addon"> <i
                                                    class="fa fa-lock"></i></span>
                                                <input type="password" id="repeat_pwd" name="repeat_pwd"
                                                       class="form-control" required="true" value="" type="text"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" name="change_pwd"
                                                class="btn btn-success">
                                            <fmt:message key="profile.button.change_pwd" bundle="${ rb }"/></button>
                                    </div>
                                </fieldset>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
</div>
<%@ include file="../include/end-html.jsp" %>