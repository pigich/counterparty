<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<c:set var="page" scope="session" value="admin_page"/>
<fmt:setLocale value="${lang}" scope="session"/>
<%@ include file="../include/begin-logged-in-admin.jsp" %>

<div class="text-center ok-message text-message">
    <br>
    <c:out value="${sessionScope.ok_message}" />
    <c:remove var="ok_message" scope="session" />
</div>
<div class="text-center error-message text-message" id="error-message">
    <br>
    <c:out value="${sessionScope.error_message}" />
    <c:remove var="error_message" scope="session" />
</div>
<div class="container">
    <div class="jumbotron text-center">
        <div class="btn-group">
            <form action="do" method="post" >
                <button type="submit"  class="btn btn-success" name="command" value="to_user_info">
                    <fmt:message key="button.users" bundle="${ rb }"/>
                </button>
                <button type="submit"  class="btn btn-success" name="command" value="to_payments_info">
                    <fmt:message key="button.payments" bundle="${ rb }"/>
                </button>
                <button type="submit"  class="btn btn-success" name="command" value="to_tariff_info">
                    <fmt:message key="button.tariffs" bundle="${ rb }"/>
                </button>
            </form>
        </div>
    </div>
</div>


<%@ include file="../include/end-html.jsp" %>

</body>
</html>