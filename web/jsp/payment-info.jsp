<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="page" scope="session" value="payment_info"/>
<fmt:setLocale value="${lang}" scope="session"/>
<%@ include file="../include/begin-logged-in-admin.jsp" %>

<div class="jumbotron text-center">
    <div class="btn-group">
        <form action="do" method="post">
            <input type="hidden" id="currentLang" value="${lang}"/>
            <button type="submit" class="btn btn-success" name="command" value="to_user_info">
                <fmt:message key="button.users" bundle="${ rb }"/>
            </button>
            <button type="submit" class="btn btn-success" name="command" value="to_payments_info">
                <fmt:message key="button.payments" bundle="${ rb }"/>
            </button>
            <button type="submit" class="btn btn-success" name="command" value="to_tariff_info">
                <fmt:message key="button.tariffs" bundle="${ rb }"/>
            </button>
        </form>
    </div>
    <br>
    <br>

    <div class="container">
        <div class="table-wrapper">
            <div class="table-title" style="background: #03A9F4">
                <div class="row">
                    <div class="col-sm-6">
                        <h2><b><fmt:message key="label.payment_info.header" bundle="${ rb }"/></b></h2>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered table-condensed" id="paymentInfoTable">
                <thead>
                <tr style="text-align:center">
                    <th><fmt:message key="label.payment_info.user_id" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.payment_info.tariff_id" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.payment_info.current_balance" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.payment_info.start_date" bundle="${ rb }"/></th>
                    <th><fmt:message key="label.payment_info.end_date" bundle="${ rb }"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${payments}" var="currentPayment" varStatus="status">
                    <tr >
                        <td id="userId${status.count}">${currentPayment.userId}</td>
                        <td id="tariffId${status.count}"><c:out value="${currentPayment.tariffId}"/></td>
                        <td id="currentBalance${status.count}"><label><c:out value="${currentPayment.currentBalance}"/></label></td>
                        <td id="startDate${status.count}"><c:out value="${currentPayment.getTimeInSimpleFormat(currentPayment.startDate)}"/></td>
                        <td id="endDate${status.count}"><label><c:out value="${currentPayment.getTimeInSimpleFormat(currentPayment.endDate)}"/></label></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <div class="container">
        <div class="table-wrapper">
            <div class="table-title" style="background: #03A9F4">
                <div class="row">
                    <div class="col-sm-6">
                        <h2><b><fmt:message key="label.fees_info.header" bundle="${ rb }"/></b></h2>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-bordered table-condensed" id="feeInfoTable">
                <thead>
                <tr style="text-align:center">
                    <th><fmt:message key="label.payment_info.fee_id" bundle="${rb}"/></th>
                    <th><fmt:message key="label.payment_info.user_id" bundle="${rb}"/></th>
                    <th><fmt:message key="profile.label.amount" bundle="${rb}"/></th>
                    <th><fmt:message key="label.fees_info.fee_date" bundle="${rb}"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${fees}" var="currentFee" varStatus="loop">
                    <tr>
                        <td id="feeId${loop.count}">${currentFee.id}</td>
                        <td id="userId${loop.count}">${currentFee.userId}</td>
                        <td id="amount${loop.count}"><c:out value="${currentFee.getAmount()}"/></td>
                        <td id="feesDate${loop.count}"><c:out value="${currentFee.getFeesDate()}"/></td>
<%--                        <td id="feesDate${loop.count}"><c:out value="${currentFee.getTimeInSimpleFormat()}"/></td>--%>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

<%@ include file="../include/end-html.jsp" %>