<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="properties/pageContent" var="rb"/>
<c:set var="page" scope="session" value="main"/>
<c:set var="tariffs_length" scope="page" value=" ${fn:length(working_tariffs)}"/>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:if test="${ not empty pageContext.session.getAttribute(\"role\")
and  pageContext.session.getAttribute(\"role\") eq 'USER' }">
    <%@ include file="../include/begin-logged-in.jsp" %>
</c:if>
<c:if test="${ not empty pageContext.session.getAttribute(\"role\")
and  pageContext.session.getAttribute(\"role\") eq 'ADMIN' }">
    <%@ include file="../include/begin-logged-in-admin.jsp" %>
</c:if>
<c:if test="${ empty pageContext.session.getAttribute(\"role\") }">
    <%@ include file="../include/begin-html.jsp" %>
</c:if>

<div class="text-center ok-message text-message">
    <br>
    <c:out value="${sessionScope.ok_message}" />
    <c:remove var="ok_message" scope="session" />
</div>
<div class="text-center error-message text-message">
    <br>
    <c:out value="${sessionScope.error_message}" />
    <c:remove var="error_message" scope="session" />
</div>
<div class="container">
    <div class="text-center">
        <h3><fmt:message key="label.legend" bundle="${ rb }"/></h3>
    </div>
</div>
<br>
<br>
<br>
<!-- Pricing HTML -->
<div class="container">
    <div class="text-center">
        <h2><fmt:message key="label.main_page.price" bundle="${ rb }"/></h2>
        <h4><fmt:message key="label.main_page.choose_tariff" bundle="${ rb }"/></h4>
    </div>
    <div class="row">
        <c:forEach items="${working_tariffs}" var="tariff" varStatus="loop">
            <div class="col-sm-${fn:replace(12/tariffs_length, ".0", "")} col-xs-12">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <h2>
                            <c:if test="${lang eq 'en_US'}">${tariff.tariffNameEn}</c:if>
                            <c:if test="${lang eq 'ru_RU'}">${tariff.tariffNameRu}</c:if></h2>
                    </div>
                    <div class="panel-body">
                        <c:forEach items="${tariff.abilityList}" var="ability" varStatus="loop">
                            <p><strong><i class="fa fa-check"></i></strong>
                                <c:if test="${lang eq 'en_US'}">${ability.abilityNameEn}</c:if>
                                <c:if test="${lang eq 'ru_RU'}">${ability.abilityNameRu}</c:if></p>
                        </c:forEach>
                    </div>
                    <div class="panel-footer">
                        <h3>${tariff.price}<fmt:message key="button.balance_rub" bundle="${ rb }"/> </h3>
                        <h4><fmt:message key="label.tariff.per_period" bundle="${ rb }"/> ${tariff.period}
                            <fmt:message key="label.tariff.per_period_two" bundle="${ rb }"/></h4>
                        <form action="do" method="post">
                            <button type="submit" class="btn btn-success" name="command" value="to_sign_up">
                                    <fmt:message key="button.sign_up" bundle="${ rb }"/>
                        </form>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>


<%@ include file="../include/end-html.jsp" %>