<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<fmt:setLocale value="${lang}" scope="session"/>
<fmt:setBundle basename="properties/pageContent" var="rb"/>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><fmt:message key="label.title" bundle="${ rb }"/></title>
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/font-awesome.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/Footer-Dark.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/styles.css"/>">
    <script type="text/javascript" src="${contextPath}/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="${contextPath}/js/bootstrap.js"></script>
    <script type="text/javascript" src="${contextPath}/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="${contextPath}/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="${contextPath}/js/service.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"> <fmt:message key="label.name" bundle="${ rb }"/></a>
        </div>
        <div class="collapse navbar-collapse" id="mainNavbar">
            <ul class="nav navbar-nav navbar-right ml-auto">
                <li><a href="do?command=to_balance" class="dropdown-item">
                    <fmt:message key="button.balance" bundle="${ rb }"/>&nbsp;<label style="color:white">${current_balance}<fmt:message key="button.balance_rub" bundle="${ rb }"/></label></a></li>
<%--                <li class="nav-item"><a href="do?command=to_messages" class="nav-link messages">--%>
<%--                    <i class="fa fa-envelope-o"></i><span class="badge">0</span></a></li>--%>
                <li class="nav-item dropdown">
                    <a href="" data-toggle="dropdown"
                       class="nav-link dropdown-toggle user-action">
                        <i class="fa fa-user-circle"></i> <b class="caret"></b></a>

                    <ul class="dropdown-menu">
                            <li><a href="do?command=to_user_page" class="dropdown-item"><i class="fa fa-home"></i>
                                <fmt:message key="button.cabinet" bundle="${ rb }"/></a></li>
                        <li><a href="do?command=to_profile" class="dropdown-item"><i class="fa fa-user-o"></i>
                            <fmt:message key="button.profile" bundle="${ rb }"/></a></li>
                        <li class="divider dropdown-divider"></li>
                        <li><a href="do?command=logout"><span class="glyphicon glyphicon-log-out"></span>
                            <fmt:message key="button.logout" bundle="${ rb }"/></a></li>
                    </ul>
                </li>
                <li><a href="do?command=lang&page=${page}">
                    <fmt:message key="button.lang" bundle="${ rb }"/></a></li>
            </ul>
        </div>
    </div>
</nav>


