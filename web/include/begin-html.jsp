<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:setBundle basename="properties/pageContent" var="rb"/>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><fmt:message key="label.title" bundle="${ rb }"/></title>
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/font-awesome.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/Footer-Dark.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/styles.css"/>">
    <script type="text/javascript" src="${contextPath}/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="${contextPath}/js/bootstrap.js"></script>
    <script type="text/javascript" src="${contextPath}/js/validator.js"></script>
    <script type="text/javascript" src="${contextPath}/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="${contextPath}/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="${contextPath}/js/service.js"></script>

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${contextPath}/"> <fmt:message key="label.name" bundle="${ rb }"/></a>
        </div>
        <div class="collapse navbar-collapse" id="mainNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="do?command=to_sign_up"><span class="glyphicon glyphicon-user"></span>
                    <fmt:message key="button.sign_up" bundle="${ rb }"/></a></li>
                <li><a href="#modalLogin" data-toggle="modal"><span class="glyphicon glyphicon-log-in"></span>
                    <fmt:message key="button.login" bundle="${ rb }"/></a></li>
                <li><a href="do?command=lang&page=${page}">
                    <fmt:message key="button.lang" bundle="${ rb }"/></a></li>
            </ul>
        </div>
    </div>
</nav>


<!-- Modal Login HTML -->
<div id="modalLogin" class="modal fade">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><fmt:message key="button.login" bundle="${ rb }"/></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <h5 class="resultOfValidation"></h5>
                </div>
                <form action="do" method="post" class="loginForm">
                    <input type="hidden" id="currentLang" value="${lang}"/>
                    <input type="hidden" name="command" value="login">
                    <div class="form-group">
                        <i class="fa fa-at"></i>
                        <input type="text" name="email" class="form-control" placeholder="Email" required
                               onblur="validateEmail(this.value)" onchange="changeToDefault()" oninput="this.onchange()">
                    </div>
                    <div class="form-group">
                        <i class="fa fa-lock"></i>
                        <input type="password" name="password" class="form-control" placeholder="Password" id="password_input" required>
                    </div>
                    <div class="form-group small clearfix">
                        <a href="#recoverPass" data-toggle="modal" data-dismiss="modal" class="forgot-link"><fmt:message key="login.forgot" bundle="${ rb }"/></a>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-block btn-lg submitButton"
                               value="<fmt:message key="button.login" bundle="${ rb }"/>">
                    </div>
                </form>
            </div>
            <div class="modal-footer"><fmt:message key="login.not_registered" bundle="${ rb }"/> &nbsp;<a
                    href="do?command=to_sign_up"><fmt:message key="button.sign_up" bundle="${ rb }"/></a></div>
        </div>
    </div>
</div>

<div id="recoverPass" class="modal fade">
    <div class="modal-dialog modal-login">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><fmt:message key="label.recovering" bundle="${ rb }"/></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <h5 class="resultOfValidation"></h5>
                </div>
                <p class="modal-title"><fmt:message key="label.enter_email" bundle="${ rb }"/></p>
                <form action="do" method="post">
                    <input type="hidden" name="command" value="recover_password">
                    <div class="form-group">
                        <i class="fa fa-at"></i>
                        <input type="text" name="email" class="form-control" placeholder="Email" required="required"
                               onblur="validateEmail(this.value)" onchange="changeToDefault()" oninput="this.onchange()">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-block btn-lg submitButton"
                               value="<fmt:message key="button.recover" bundle="${ rb }"/>">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- End Modal Login HTML -->