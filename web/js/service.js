var selected = [];
$(document).ready(function () {
    // Activate tooltip
    $('[data-toggle="tooltip"]').tooltip();

    $.fn.dataTable.ext.errMode = 'throw';
    //Datatable for fee info table in payments-info.jsp
    $(document).ready(function () {
        $('#paymentInfoTable').DataTable({
            "lengthMenu": [[2, 5, 10], [2, 5, 10]],
            "language": {
                "url": changeLanguage()
            }
        });
    });
    //Datatable for payment info table in payments-info.jsp
    $(document).ready(function () {
        var currentLang = $('#currentLang').val();
        $('#feeInfoTable').DataTable({
            "lengthMenu": [[2, 5, 10], [2, 5, 10]],
            "language": {
                "url": changeLanguage()
            },
            "columns": [
                { "type": "string" },
                { "type": "string" },
                { "type": "string" },
                { "type": "date" }
                ],
            "processing": true,
            "serverSide": true,
            ajax: {
                url: '/ado?command=find_fees_ajax',
                type: 'POST',
                data: {
                    "lang": currentLang
                }
            }
        });
    });
    $(document).click(function () {
        $('.text-message ').delay(100).animate({'opacity': '0'}, 500);
    });

    function changeLanguage() {
        var currentLang = $('#currentLang').val();
        if (currentLang === "ru_RU") {
            return "../include/Russian.json";
        } else {
            return "../include/English.json"
        }
    }

    function findUsers(tariffId, current_rows_count, current_page, searching_value) {
        $.ajax({
            method: "GET",
            url: '/ado?command=find_users_ajax',
            data: {
                current_tariff_id: tariffId,
                rows_per_page: current_rows_count,
                current_page: current_page,
                searching_value: searching_value
            },
            dataType: 'json',
            success: function (responseJson) {
                var $select = $('#user_info_tbody');
                $select.find('tr').remove();
                userInfoTableData(responseJson);
            }
        });
    }

    function searchByValueFunction() {
        var tariffId = $("#tableIdSearch").val();
        var current_rows_count = $("#showRowsOnPage").val();
        var current_page = $("#current_page").val();
        var searched_value = $("#search_input").val();
        findUsers(tariffId, current_rows_count, current_page, searched_value);
    }

    //Search action
    $('#search_button').on('click', function () {
        searchByValueFunction();
    });

    //Search action on Enter press
    $("#search_input").keypress(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            searchByValueFunction();
        }
    });

    //Tariff filter action
    $('#tableIdSearch').on('change', function () {
        var tariffId = $(this).val();
        var current_rows_count = $("#showRowsOnPage").val();
        $("#current_page").val("1");
        var current_page = $("#current_page").val();
        var searching_value = $("#search_input").val();
        console.log("tariffId = " + tariffId);
        findUsers(tariffId, current_rows_count, current_page, searching_value);
    });

    //Filter that shows rows on the page action
    $('#showRowsOnPage').on('change', function () {
        var current_rows_count = $(this).val();
        var tariffId = $("#tableIdSearch").val();
        $("#current_page").val("1");
        var current_page = $("#current_page").val();
        var searching_value = $("#search_input").val();
        findUsers(tariffId, current_rows_count, current_page, searching_value);
    });


    //Function shows amount of rows on the page, total amount of rows and amount of pages
    function showPages(responseJson) {
        var rowsCount = responseJson.Quantity.rowsCount;
        var pagesCount = responseJson.Quantity.pagesCount;
        var current_rows_count = countProperties(responseJson.listOfObjects);
        $("#current_rows_label").text(current_rows_count);
        $("#total_rows_label").text(rowsCount);
        $("#total_rows_count").text(rowsCount);
        var $select = $('#show_pages');
        $select.find('li').remove();
        for (var i = 1; i <= pagesCount; i++) {
            $select.append($('<li class=\"page-item \" data-page_value=\"' + i + '\" id=\"' + i + '\">')
                .append($('<a class=\"page-link\">').attr('href', '#')
                    .append(i)));
        }
        var currentPage = $("#current_page").val();
        $select.find('#' + currentPage + '').addClass("active");
    }

    //Pages change action
    $("#show_pages").on("click", "a", function () {
        var $this = $(this).parent();
        $this.addClass("active").siblings().removeClass("active");
        $("#current_page").val($this.data("page_value"));
        var current_rows_count = $("#showRowsOnPage").val();
        var tariffId = $("#tableIdSearch").val();
        var current_page = $("#current_page").val();
        var searching_value = $("#search_input").val();
        findUsers(tariffId, current_rows_count, current_page, searching_value);
        // console.log("chosen page is =" + $("#current_page").val() );
    });

    //function which counts how many rows are in the response
    function countProperties(obj) {
        var count = 0;
        $.each(obj, function () {
            count++;
        });
        return count;
    }

    var userInfoTableData = function (responseJson) {
        try {
            showPages(responseJson);
            $.each(responseJson.listOfObjects, function (index, object) {
                var currentLang = $('#currentLang').val();
                var userStatus = function (object) {
                    if (object.deleted) {
                        if (currentLang === "ru_RU") {
                            return "Заблокированный";
                        } else {
                            return "Blocked";
                        }

                    } else {
                        if (currentLang === "ru_RU") {
                            return "Активный";
                        } else {
                            return "Active";
                        }
                    }
                };
                var userRole = function (object) {
                    if (object.role === 1) {
                        return "user";
                    } else {
                        return "admin";
                    }
                };

                var userStatusTag = function (object) {
                    if (object.deleted) {
                        return "<input type=\"hidden\" id=\"currentUserId" + index + "\" value=\"" + object.id + "\"/>" +
                            "<input type=\"hidden\" id=\"currentUserStatus" + index + "\" value=\"" + object.deleted + "\"/>" +
                            "<label data-index=\"" + index + "\" for=\"blockSubmit" + index + "\" class=\"btn btn-xs testLabel\">" +
                            "<i data-toggle=\"tooltip\" title=\"Lock\" class=\"fa fa-lock fa-sm \"></i></label>" +
                            "<input type=\"submit\" name=\"command\" id=\"blockSubmit" + index + "\"" +
                            " value=\"" + index + "\" class=\"hidden\"/>";
                    } else return "<input type=\"hidden\"  id=\"currentUserId" + index + "\" value=\"" + object.id + "\"/>" +
                        "<input type=\"hidden\" id=\"currentUserStatus" + index + "\" value=\"" + object.deleted + "\"/>" +
                        "<label data-index=\"" + index + "\" for=\"blockSubmit" + index + "\" class=\"btn btn-xs testLabel\">" +
                        "<i data-toggle=\"tooltip\" title=\"UnLock\" class=\"fa fa-unlock fa-sm\"></i></label>" +
                        "<input type=\"submit\" name=\"command\" id=\"blockSubmit" + index + "\"" +
                        " value=\"" + index + "\" class=\"hidden\"/>";
                };
                $("#user_info_tbody").append(
                    "<tr >" +
                    "<td id=\"userId" + index + "\">" + object.id + "</td>" +
                    "<td id=\"fullName" + index + "\">" + object.profile.fullName + "</td>" +
                    "<td id=\"phone" + index + "\">" + object.profile.phone + "</td>" +
                    "<td id=\"email" + index + "\">" + object.email + "</td>" +
                    "<td id=\"orgName" + index + "\">" + object.profile.orgName + "</td>" +
                    "<td id=\"tariffId" + index + "\">" + object.profile.tariffId + "</td>" +
                    "<td id=\"role" + index + "\">" + userRole(object) + "</td>" +
                    "<td id=\"userStatus" + index + "\">" + userStatus(object) + "</label></td>" +
                    "<td>" + userStatusTag(object) + "</td>" +
                    "</tr>");
            });
        } catch (err) {
            console.log("responseJson " + responseJson);
            var message = responseJson;
            var $select = $('#errorInfoClass');
            $select.find('label').remove();
            $("#errorInfoClass").append(
                "<label  class=\"btn btn-xs testLabel\" id=\'errorLabel\' >" + message + "</label>");
            $('#myModal').modal('show');
            message = null;
        }
    };

    //Block user action
    $("#user_info_tbody").on("click", '[name=command]', function (e) {
        var searching_value = $("#search_input").val();
        var current_rows_count = $("#showRowsOnPage").val();
        var current_tariff = $("#tableIdSearch").val();
        var index = $(this).val();
        var current_user_id = $('#currentUserId' + index).val();
        var current_user_status = $('#currentUserStatus' + index).val();
        var current_page = $("#current_page").val();
        $.ajax({
            method: "GET",
            url: '/ado?command=block_user_ajax',
            data: {
                rows_per_page: current_rows_count,
                current_tariff_id: current_tariff,
                current_user_status: current_user_status,
                current_user_id: current_user_id,
                current_page: current_page,
                searching_value: searching_value
            },
            dataType: 'json',
            success: function (responseJson) {
                var $select = $('#user_info_tbody');
                $select.find('tr').remove();
                userInfoTableData(responseJson);
            }
        });
    });

    $('#editTariffModal').on('shown.bs.modal', function (e) {
        selected = [];
        $.each($('.editAbility'), function () {
            $(this).prop('checked', false);
        });
        var loop = $(e.relatedTarget).data('loop');
        var tariffId = $('#tariffId' + loop).val();
        var tariffNameEn = $('#tariffNameEn' + loop).text();
        var tariffNameRu = $('#tariffNameRu' + loop).text();
        var tariffPeriod = $('#period' + loop).text();
        var tariffPrice = $('#price' + loop).text();
        $("#edit_modal_id_label").text('');
        document.getElementById("edit_modal_id_label").append(" " + tariffId);
        document.getElementById("modal_tariff_id").value = tariffId;
        document.getElementById("modal_name_en").value = tariffNameEn;
        document.getElementById("modal_name_ru").value = tariffNameRu;
        document.getElementById("modal_period").value = tariffPeriod;
        document.getElementById("modal_price").value = tariffPrice;
        $.get('/ado?command=find_ability_ajax', {tariff_id: tariffId}, function (response) {
            if (response != null) {
                $.each(JSON.parse(response), function (i, object) {
                    $.each($('.editAbility'), function () {
                        if (object.id == $(this).val()) {
                            $(this).prop('checked', true);
                            selected.push(object.id);
                        }
                    });
                });
                document.getElementById("edit_ability_list").value = selected;
            }
        });


    });
    $('#editModalCheckBox').change(function() {
        var checkboxes = document.getElementsByName('editAbility');
        selected = [];
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                selected.push(checkboxes[i].value);
            }
        }
        document.getElementById("edit_ability_list").value = selected;
    });

    $('#editAbilityModal').on('shown.bs.modal', function (e) {
        var loop = $(e.relatedTarget).data('loop');
        var abilityId = $('#abilityId' + loop).val();
        var ability_name_en = $('#abilityNameEn' + loop).text();
        var ability_name_ru = $('#abilityNameRu' + loop).text();
        document.getElementById("modal_ability_id").value = abilityId;
        document.getElementById("modal_ability_name_En").value = ability_name_en;
        document.getElementById("modal_ability_name_Ru").value = ability_name_ru;
    });

    $('#addTariffModal').on('shown.bs.modal', function (e) {
        selected = [];
        var loop = $(e.relatedTarget).data('loop');
        var tariffId = $('#tariffId' + loop).val();
        var tariff_name_en = $('#tariffNameEn' + loop).text();
        var tariffNameRu = $('#tariffNameRu' + loop).text();
        var tariffPeriod = $('#period' + loop).text();
        var tariffPrice = $('#price' + loop).text();
        document.getElementById("modal_tariff_id").value = tariffId;
        document.getElementById("modal_name_en").value = tariff_name_en;
        document.getElementById("modal_name_ru").value = tariffNameRu;
        document.getElementById("modal_period").value = tariffPeriod;
        document.getElementById("modal_price").value = tariffPrice;
    });

    $('#addTariffModalCheckBox').change(function() {
        var selected = [];
        var checkboxes = document.getElementsByName('addAbility');
        // var checkboxes = document.getElementsByName('ability_id');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                selected.push(checkboxes[i].value);
            }
        }
        document.getElementById("ability_id_list").value = selected;
        // console.log(ability_id_list.value);
    });

    // This function prevent multy click on submit button
    jQuery.fn.preventDoubleSubmission = function () {
        $(this).on('submit', function (e) {
            var $form = $(this);
            if ($form.data('submitted') === true) {
                e.preventDefault();
            } else {
                $form.data('submitted', true);
            }
        });
        return this;
    };
    $('form').preventDoubleSubmission();
});

