
//проверка на число

//        Проверяет мин. и макс. длину текста вводимой в поле формы
function validateLength(minLength, maxLength, inputField, helpText) {
    if (inputField.value.length < minLength || inputField.value.length > maxLength) {
        if (helpText != null)
            helpText.innerHTML = "заполните поле " + minLength + " - " + maxLength + " символами";
        return false;
    }
    else {
        if (helpText != null)
            helpText.innerHTML = "";
        return true;
    }
}
function validateLengthNumber(minLength, maxLength, inputField, helpText) {
    if (inputField.value.length == 0) {
        if (helpText != null)
            helpText.innerHTML = "заполните поле";
        return false;
    } else {
        if (isNaN(inputField.value)) { // введено не число
            helpText.innerHTML = "введите число";
        }
        else if (inputField.value.length < minLength || inputField.value.length > maxLength) {
            if (helpText != null)
                helpText.innerHTML = "макс кол-во символов " + maxLength;
            return false;
        } else if (helpText != null)
            helpText.innerHTML = "";
        return false;
    }
}

function validateNonEmpty(inputField, clientName_help) {
    if (inputField.value.length == 0) {
        if (clientName_help != null)
            clientName_help.innerHTML = "заполните поле";
        return false;
    }
    else {
        if (clientName_help != null)
            clientName_help.innerHTML = "";
        return false;
    }
}

//        Проверяет мин. и макс. длину текста вводимой в поле формы
function validateLength(minLength, maxLength, inputField, helpText) {
    if (inputField.value.length < minLength || inputField.value.length > maxLength) {
        if (helpText != null)
            helpText.innerHTML = "заполните поле " + minLength + " - " + maxLength + " символами";
        return false;
    }
    else {
        if (helpText != null)
            helpText.innerHTML = "";
        return true;
    }
}
//Проверяет введена ли информация в поле формы
// function validateFocus(inputField) {
//     alert(inputField);
// }
//        function confirmBut(form) {
//            if (validateLength(1, 32, form["clientSurname"], form["clientSurname_help"]) &&
//                    validateNonEmpty(form["clientName"], form["clientName_help"])) {
//                form.submit();
//            }
//            else {
//                alert("Форма полностью не заполненна");
//            }
//        }
