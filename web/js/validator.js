var EMAIL_REGEXP = /^([A-z0-9_.-]{1,45})@([A-z0-9_.-]{1,45})\.([A-z]{2,6})$/;
var EMAIL_FORMAT_VALIDATION_RU = "Неверный формат электронной почты";
var EMAIL_FORMAT_VALIDATION_EN = "Not valid email format";

function validateEmail(str) {
    var currentLang = $('#currentLang').val();
    var emailStatus;
    if (currentLang === "ru_RU") {
        emailStatus = EMAIL_FORMAT_VALIDATION_RU;
    } else {
        emailStatus = EMAIL_FORMAT_VALIDATION_EN;
    }
    var $result = $(".resultOfValidation");
    if (!EMAIL_REGEXP.test(str)) {
        $result.text(emailStatus);
        $result.css("color", "red");
        $('.submitButton').prop('disabled', true);
    }
}

function changeToDefault() {
    $('.submitButton').prop('disabled', false);
    var $result = $(".resultOfValidation");
    $result.text("");
}
