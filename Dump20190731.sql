CREATE DATABASE  IF NOT EXISTS `counterparty` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `counterparty`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: counterparty
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abilities`
--

DROP TABLE IF EXISTS `abilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `abilities` (
  `ability_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ability_name_en` varchar(100) NOT NULL DEFAULT '',
  `ability_name_ru` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`ability_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abilities`
--

LOCK TABLES `abilities` WRITE;
/*!40000 ALTER TABLE `abilities` DISABLE KEYS */;
INSERT INTO `abilities` VALUES (1,'Test ability','Тестовая функция'),(4,'Budget debt information','Информация о задолженности перед бюджетом'),(5,'Information on legal proceedings','Информация о приказных производствах'),(7,'Bunkruptcy information','Информация о банкротстве'),(8,'Monitoring with notification','Мониторинг с уведомлением');
/*!40000 ALTER TABLE `abilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ability_tariff`
--

DROP TABLE IF EXISTS `ability_tariff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ability_tariff` (
  `ability_id` bigint(20) unsigned NOT NULL,
  `tariff_id` bigint(20) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ability_tariff`
--

LOCK TABLES `ability_tariff` WRITE;
/*!40000 ALTER TABLE `ability_tariff` DISABLE KEYS */;
INSERT INTO `ability_tariff` VALUES (1,52),(4,58),(5,58),(7,58),(1,1),(1,19),(4,19),(5,19),(1,29),(4,29),(5,29),(7,29),(8,29),(5,40),(8,40);
/*!40000 ALTER TABLE `ability_tariff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees`
--

DROP TABLE IF EXISTS `fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `fees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `amount` decimal(10,0) unsigned NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees`
--

LOCK TABLES `fees` WRITE;
/*!40000 ALTER TABLE `fees` DISABLE KEYS */;
INSERT INTO `fees` VALUES (44,48,30,'2019-06-12 05:19:55'),(45,50,30,'2019-06-12 14:33:07'),(46,48,30,'2019-06-15 16:04:02'),(47,48,5,'2019-06-18 10:47:46'),(56,48,5,'2019-06-24 05:24:52'),(57,48,5,'2019-07-03 12:36:23'),(58,59,5,'2019-07-05 09:54:26'),(59,59,5,'2019-07-05 09:55:24'),(60,48,1,'2019-07-06 07:05:18'),(61,48,1,'2019-07-06 12:21:00'),(62,48,1,'2019-07-06 12:21:04'),(63,48,1,'2019-07-07 16:45:16'),(64,55,100,'2019-07-08 14:41:25');
/*!40000 ALTER TABLE `fees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `payments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `tariff_id` bigint(20) unsigned DEFAULT '1',
  `current_balance` decimal(10,0) unsigned NOT NULL DEFAULT '0',
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT ((now() + interval 1 day)),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (25,48,40,44,'2019-07-07 16:45:27','2019-07-09 16:45:27'),(27,52,19,30,'2019-06-12 14:35:00','2019-06-17 14:35:00'),(28,59,1,0,'2019-07-04 08:22:25','2019-07-05 08:22:25'),(29,55,40,98,'2019-07-08 14:41:25','2019-07-10 14:41:25');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `profile` (
  `user_id` bigint(20) unsigned NOT NULL,
  `full_name` varchar(100) DEFAULT '',
  `phone` varchar(13) DEFAULT '',
  `organization_unp` int(11) unsigned DEFAULT '0',
  `organization_name` varchar(255) DEFAULT '',
  `tariff_id` bigint(20) unsigned DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (48,'Иван Иванович','80339874582',123123112,'Coca-cola Company',40),(50,'Паша','80295489745',123875411,'EPAM',19),(52,'Ozzy','80256324158',133232314,'Google',29),(53,'Zakk Wylde','80671459735',101546673,'SONY',29),(54,'Tom Hanks','80987422287',103216642,'Dell',29),(55,'Jim Carry','80327412387',101541671,'IBM',29),(56,'Анатолий','80446547814',133232314,'Администрация призидента РБ',29),(59,'Angelina Jolie','80293299886',123546764,'EPAM',1);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role` (
  `role_id` tinyint(3) unsigned NOT NULL,
  `role` varchar(5) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'user'),(2,'admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tariff`
--

DROP TABLE IF EXISTS `tariff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tariff` (
  `tariff_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tariff_name_en` varchar(100) NOT NULL DEFAULT '',
  `tariff_name_ru` varchar(100) NOT NULL DEFAULT '',
  `archival` tinyint(1) DEFAULT '0',
  `price` decimal(10,0) unsigned DEFAULT '0',
  `period` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`tariff_id`),
  UNIQUE KEY `planId` (`tariff_id`),
  UNIQUE KEY `plan_name_UNIQUE` (`tariff_name_en`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tariff`
--

LOCK TABLES `tariff` WRITE;
/*!40000 ALTER TABLE `tariff` DISABLE KEYS */;
INSERT INTO `tariff` VALUES (1,'Demo','Демо',0,0,1),(19,'Base','Базовый',0,30,30),(29,'Pro','Про',0,100,30),(40,'test','тест',0,2,2),(52,'Old_tariff','Старый тариф',1,123,123);
/*!40000 ALTER TABLE `tariff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `role_id` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (48,'123@mail.ru','a03ab19b866fc585b5cb1812a2f63ca861e7e7643ee5d43fd7106b623725fd67',1,0),(50,'qwe@mail.ru','67b176705b46206614219f47a05aee7ae6a3edbe850bbbe214c536b989aea4d2',2,0),(52,'342@tut.by','67b176705b46206614219f47a05aee7ae6a3edbe850bbbe214c536b989aea4d2',1,1),(53,'asd@i.ua','67b176705b46206614219f47a05aee7ae6a3edbe850bbbe214c536b989aea4d2',1,1),(54,'ggg@mail.ua','67b176705b46206614219f47a05aee7ae6a3edbe850bbbe214c536b989aea4d2',1,0),(55,'vasya@gmail.com','67b176705b46206614219f47a05aee7ae6a3edbe850bbbe214c536b989aea4d2',1,1),(56,'olya@rb.gov','67b176705b46206614219f47a05aee7ae6a3edbe850bbbe214c536b989aea4d2',1,1),(59,'pigich09@gmail.com','a04b760503526abc0244e7596ffa203fa4479a112a15d82254a1354250c66b86',1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-31 13:20:48
